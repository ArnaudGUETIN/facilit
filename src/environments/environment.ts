// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
export const environment = {
  production: false,
  endpoint: 'https://www.facilit-dev.com/platform-facilit-1.0/',
  dateShortLabels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
  lineChartDefaultOption:{
    tooltips: {
      enabled: true,
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: true,
        points: false,
      }],
      yAxes: [{
        display: true,
      }]
    },
    elements: { point: { radius: 1 } },
    legend: {
      display: false
    }
  }
};
