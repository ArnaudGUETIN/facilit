import {Component, NgZone} from '@angular/core';
import {Collaborateur} from "../../Model/Collaborateur";
import {Entreprise} from "../../Model/Entreprise";
import {Router} from "@angular/router";
import {Platform} from "@angular/cdk/platform";
import {Location} from "@angular/common";
const roleAdmin = 'ADMIN';
const roleCollaborateur = 'COLLABORATEUR';
const roleEntreprise = 'ENTREPRISE';
const roleConsultant = 'CONSULTANT';
const rolePartenaire = 'PARTENAIRE';
@Component({
  templateUrl: '404.component.html'
})
export class P404Component {
  role = localStorage.getItem('appRole');
  private collaborateur: Collaborateur;
  private entreprise: Entreprise;
  private zone: NgZone
  constructor(private location: Location) { }

  goToDashBorad() {
   this.goToHome();
  }

  goToHome() {

    this.location.back();
 /*   this.zone.run(() => {
    if (this.role == roleAdmin) {
      this.router.navigate(['/admin']) ; //this.router.navigate(['admin']);
    } else if (this.role == roleCollaborateur) {
      this.collaborateur = JSON.parse(localStorage.getItem('collaborateur'));
      this.router.navigate(['/collaborateur']) ;
    } else if (this.role == roleEntreprise) {
      this.entreprise = JSON.parse(localStorage.getItem('entreprise'));
      this.router.navigate(['/entreprise/'+this.entreprise.entrepriseId]) ;

    }
    });*/
  }
}
