import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AdminRoutingModule} from './admin-routing.module';
import {AdmindashbordComponent} from './admindashbord/admindashbord.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSortModule} from '@angular/material/sort';
import {MatTabsModule} from '@angular/material/tabs';
import {UserComponent} from './user/user.component';
import {DragDropModule} from 'primeng/dragdrop';
import {TableModule} from 'primeng/table';
import {UserEditComponent} from './user-edit/user-edit.component';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {DialogModule} from 'primeng/dialog';
import {CardModule} from 'primeng/card';
import {FlexLayoutModule} from '@angular/flex-layout';
import {DateAdapter, MatNativeDateModule} from "@angular/material/core";
import {DateFormat} from "../../shared/shared/DateFormat";
import {MatTooltipModule} from "@angular/material/tooltip";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {ToastModule} from "primeng/toast";
import {SharedModule} from "../../shared/shared/shared.module";

@NgModule({
  declarations: [
    AdmindashbordComponent,
    UserComponent,
    UserEditComponent
  ],
    imports: [
        CommonModule,

        FormsModule,
        ReactiveFormsModule,
        ChartsModule,
        DragDropModule,
        MatDialogModule,
        BsDropdownModule,
        TableModule,
        MatTableModule,
        MatSelectModule,
        MatChipsModule,
        MatIconModule,
        MatTabsModule,
        MatFormFieldModule,
        MatInputModule,
        MatPaginatorModule,
        MatSortModule,
        AdminRoutingModule,
        DialogModule,
        CardModule,
        MatNativeDateModule,
        FlexLayoutModule,
        MatTooltipModule,
        ConfirmDialogModule,
        ToastModule,
        SharedModule
    ],
  providers:[{ provide: DateAdapter, useClass: DateFormat },]
})
export class AdminModule {
  constructor(private dateAdapter: DateAdapter<Date>) {
    dateAdapter.setLocale("en-in"); // DD/MM/YYYY
  }
}
