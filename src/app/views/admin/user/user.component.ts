import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from '../../../Model/User';
import {UserService} from '../../../Services/user.service';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {UserEditModalComponent} from '../../../shared/dialogModal/user-edit-modal/user-edit-modal.component';
import {Router} from '@angular/router';
import {ConfirmModalComponent} from '../../../shared/dialogModal/confirm-modal/confirm-modal.component';
import {ConfirmationService, MessageService} from "primeng/api";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  availableUsers: User[];

  selectedUsers: User[];

  selectedAdminUsers: User[];
  selectedCollabUsers: User[];
  selectedEntUsers: User[];

  draggedUser: User;
  displayedColumns: string[] = ['nom', 'prenom', 'dateCreation', 'username', 'role','dateDerniereConnexion', 'star'];
  dataSource: MatTableDataSource<User> = new MatTableDataSource();

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialog: MatDialog, private userService: UserService, private router: Router
              ,private messageService: MessageService,private confirmationService: ConfirmationService) {
  }

  ngOnInit() {
    this.selectedUsers = [];
    this.selectedAdminUsers = [];
    this.selectedCollabUsers = [];
    this.selectedEntUsers = [];
    this.getAllUsers();
    //  this.getAllAdminUsers();
    //   this.getAllCollabUsers();
    //  this.getAllEntUsers();
  }

  dragStart(user: User) {
    this.draggedUser = user;
  }

  drop() {
    if (this.draggedUser) {
      let draggedUserIndex = this.findIndex(this.draggedUser);
      this.selectedUsers = [...this.selectedUsers, this.draggedUser];
      //this.availableUsers = this.availableUsers.filter((val,i) => i!=draggedUserIndex);
      this.draggedUser = null;
    }
  }

  dropAdmin() {
    if (this.draggedUser) {
      let draggedUserIndex = this.findIndex(this.draggedUser);
      this.selectedAdminUsers = [...this.selectedAdminUsers, this.draggedUser];
      //this.availableUsers = this.availableUsers.filter((val,i) => i!=draggedUserIndex);
      this.draggedUser = null;
    }
  }

  dropCollab() {
    if (this.draggedUser) {
      let draggedUserIndex = this.findIndex(this.draggedUser);
      this.selectedCollabUsers = [...this.selectedCollabUsers, this.draggedUser];
      //this.availableUsers = this.availableUsers.filter((val,i) => i!=draggedUserIndex);
      this.draggedUser = null;
    }
  }

  dropEnt() {
    if (this.draggedUser) {
      let draggedUserIndex = this.findIndex(this.draggedUser);
      this.selectedEntUsers = [...this.selectedEntUsers, this.draggedUser];
      //this.availableUsers = this.availableUsers.filter((val,i) => i!=draggedUserIndex);
      this.draggedUser = null;
    }
  }

  dragEnd(event) {
    this.draggedUser = null;
  }

  findIndex(user: User) {
    let index = -1;
    for (let i = 0; i < this.availableUsers.length; i++) {
      if (user.id === this.availableUsers[i].id) {
        index = i;
        break;
      }
    }
    return index;
  }

  openAddDialog(isEdit: boolean) {

    if (isEdit) {

    } else {
      const dialogRef = this.dialog.open(UserEditModalComponent, {
        width: '700px',
        data: {user: null, isModify: false}
      });

      dialogRef.afterClosed().subscribe(result => {
        let user = JSON.parse(result.user);
        if (user && user.id && user.id != 0) {
          console.log(user);
          this.availableUsers.push(user);
          this.availableUsers = [...this.availableUsers];
          this.dataSource = new MatTableDataSource(this.availableUsers);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          console.log(this.availableUsers);
        }

      });
    }
  }

  private getAllAdminUsers() {
    this.userService.getAllRoleUsers(User.ROLE_ADMIN)
      .subscribe(users => {
        console.log(users);
        this.selectedAdminUsers = users._embedded.appUsers;
        this.availableUsers = users._embedded.appUsers;
      });
  }

  private getAllCollabUsers() {
    this.userService.getAllRoleUsers(User.ROLE_COLLABORATEUR)
      .subscribe(users => {
        console.log(users);
        this.selectedCollabUsers = users._embedded.appUsers;
        this.availableUsers = users._embedded.appUsers;
      });
  }

  private getAllEntUsers() {
    this.userService.getAllRoleUsers(User.ROLE_ENTREPRISE)
      .subscribe(users => {
        console.log(users);
        this.selectedEntUsers = users._embedded.appUsers;
        this.availableUsers = users._embedded.appUsers;
      });
  }

  openModifyDialog(user: any) {
    console.log(user);

    const dialogRef = this.dialog.open(UserEditModalComponent, {
      width: '700px',
      data: {user: user, isModify: true}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      if (result) {
        let user = JSON.parse(result.user);
        if (result.isModifyPerformed) {
          console.log(user);
          const elementsIndex = this.availableUsers.findIndex(element => element.id == user.id);
          let newArray = [...this.availableUsers];
          newArray[elementsIndex] = user;
          this.dataSource = new MatTableDataSource(newArray);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        }

      }
    });

  }

  displayUser(user: any) {
    this.router.navigate(['/admin/userEdit', {userId: user.id}]);
  }

  beSureToDelete(userId: number) {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '500px',
      data: {message: 'Voulez-vous supprimer cet utilisateur?', object: 'utilisateur'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.removeUser(userId);
      }
    });
  }

  private getAllUsers() {
    this.userService.getAllUsers()
      .subscribe(users => {
        this.availableUsers = users;
        this.dataSource = new MatTableDataSource(this.availableUsers);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }

  private removeUser(userId: number) {
    this.userService.deleteUser(userId)
      .subscribe(data => {
        console.log(data);
        this.getAllUsers();
      });
  }

  resetPassword(email:string) {
    this.userService.resetPassword(email).subscribe(data => {
      this.messageService.add({severity: 'success', summary: 'Success', detail: `'un email a été envoyé à l'adresse '${email}`});
    })
  }

  confirmReset(email:string) {
    this.confirmationService.confirm({
      message: 'Êtes vous sûr de vouloir réinitialiser le mot de passe?',
      header: 'Confirmation',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel:'Oui',
      rejectLabel:'Non',
      accept: () => {
        this.resetPassword(email);
      },
      reject: () => {
        this.messageService.add({severity: 'info', summary: 'Info', detail: `Abandon de l'opération`});
      }
    });
  }
}
