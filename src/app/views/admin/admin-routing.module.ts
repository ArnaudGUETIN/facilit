import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdmindashbordComponent} from './admindashbord/admindashbord.component';
import {UserComponent} from './user/user.component';
import {UserEditComponent} from './user-edit/user-edit.component';
import {ParametrageComponent} from "../../shared/component/parametrage/parametrage.component";

const routes: Routes = [
  {
    path: '',
    component: AdmindashbordComponent,
    data: {
      title: 'Tableau de bord'
    }
  }, {
    path: 'users',
    component: UserComponent,
    data: {
      title: 'Tableau de bord'
    }
  }
  , {
    path: 'userEdit',
    component: UserEditComponent,
    data: {
      title: 'Tableau de bord'
    }
  }, {
    path: 'parametrage',
    component: ParametrageComponent,
    data: {
      title: 'Tableau de bord'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
