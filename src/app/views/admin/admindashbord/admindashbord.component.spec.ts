import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AdmindashbordComponent} from './admindashbord.component';
import {HttpClientModule} from "@angular/common/http";
import {MatDialogModule} from "@angular/material/dialog";
import { RouterModule } from '@angular/router';
import {MessageService} from "primeng/api";

describe('AdmindashbordComponent', () => {
  let component: AdmindashbordComponent;
  let fixture: ComponentFixture<AdmindashbordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AdmindashbordComponent],
      imports:[HttpClientModule,MatDialogModule,RouterModule.forRoot([])],
      providers:[MessageService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmindashbordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
