import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {EntrepriseserviceService} from '../../../Services/entrepriseservice.service';
import {Entreprise} from '../../../Model/Entreprise';
import {MatDialog} from '@angular/material/dialog';
import {EntrepriseEditModalComponent} from '../../../shared/dialogModal/entreprise-edit-modal/entreprise-edit-modal.component';
import {ConfirmModalComponent} from '../../../shared/dialogModal/confirm-modal/confirm-modal.component';
import {Router} from '@angular/router';
import {Camping} from '../../../Model/Camping';
import {MessageService} from "primeng/api";

export interface PeriodicElement {
  nom: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, nom: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, nom: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, nom: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, nom: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, nom: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, nom: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, nom: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, nom: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, nom: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, nom: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

@Component({
  selector: 'app-admindashbord',
  templateUrl: './admindashbord.component.html',
  styleUrls: ['./admindashbord.component.scss']
})
export class AdmindashbordComponent implements OnInit {

  entrepriseList: any ;
  displayedColumns: string[] = ['code', 'nomCommercial','entrepriseType', 'emailContact', 'type', 'siret', 'telephone', 'star'];
  dataSource: MatTableDataSource<Entreprise> = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayEntrepriseTypeSelection: any;
  showLoader=false;

  constructor(private entrepriseService: EntrepriseserviceService, public dialog: MatDialog, public router: Router,private messageService:MessageService) {
  }


  ngOnInit(): void {
    this.showLoader = true;
    setTimeout(()=>{
      localStorage.removeItem("entreprise");
      this.entrepriseService.CURRENT_ENTREPRISE.next(null);
      this.getAllEntreprise();
      this.showLoader = false;
    },1500)

  }

  getAllEntreprise() {
    this.entrepriseService.getAllEntreprises()
      .subscribe(data => {
        this.entrepriseList = data;
        this.dataSource = new MatTableDataSource(this.entrepriseList);
        setTimeout(() => {
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        },1000);

      });
  }

  displayEntreprise(row: any) {
    this.entrepriseService.CURRENT_ENTREPRISE.next(row);
    localStorage.setItem("entreprise",JSON.stringify(row));
    this.router.navigate(['entreprise/' + row.entrepriseId,{selectedIndex: 0}]);

  }

  openAddDialog(isEdit: boolean) {

    if (!isEdit) {
      this.displayEntrepriseTypeSelection = true;
    } else {
      const dialogRef = this.dialog.open(EntrepriseEditModalComponent, {
        width: '700px',
        data: {entreprise: null, isModify: false}
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          let entreprise = JSON.parse(result.entreprise);
          if (entreprise && entreprise.entrepriseId && entreprise.entrepriseId != 0) {
            console.log(entreprise);
            this.entrepriseList.push(entreprise);
            this.entrepriseList = [...this.entrepriseList];
            this.dataSource = new MatTableDataSource(this.entrepriseList);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
            console.log(this.entrepriseList);
          }
        }


      });
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openModifyDialog(entreprise: any) {
    // console.log(entreprise);
    //
    // const dialogRef = this.dialog.open(EntrepriseEditModalComponent, {
    //   width: '700px',
    //   data: {entreprise: entreprise, isModify: true}
    // });
    //
    // dialogRef.afterClosed().subscribe(result => {
    //   console.log(result);
    //   if (result) {
    //     let entreprise = JSON.parse(result.entreprise);
    //     if (result.isModifyPerformed) {
    //       console.log(entreprise);
    //       const elementsIndex = this.entrepriseList.findIndex(element => element.entrepriseId == entreprise.entrepriseId);
    //       let newArray = [...this.entrepriseList];
    //       newArray[elementsIndex] = entreprise;
    //       this.dataSource = new MatTableDataSource(newArray);
    //       this.dataSource.sort = this.sort;
    //       this.dataSource.paginator = this.paginator;
    //     }
    //
    //   }
    // });
    this.router.navigate(['entreprise/' + entreprise.entrepriseId + '/editEntreprise', {
      entrepriseId: entreprise.entrepriseId,
      entrepriseType: entreprise.entrepriseType
    }]);
  }

  beSureToDelete(entrepriseId: number) {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '500px',
      data: {message: 'Voulez-vous supprimez cette entreprise?', object: 'entreprise'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.deleteEntreprise(entrepriseId);
      }
    });
  }

  deleteEntreprise(entrepriseId: number) {
    this.entrepriseService.deleteEntreprise(entrepriseId)
      .subscribe(data => {
        const elementsIndex = this.entrepriseList.findIndex(element => element.entrepriseId == entrepriseId);
        this.entrepriseList.splice(elementsIndex, 1);
        console.log(this.entrepriseList);
        this.dataSource = new MatTableDataSource(this.entrepriseList);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }

  hideTypeSelectionDialog() {
    this.displayEntrepriseTypeSelection = false;
  }

  createCamping() {
    let newEntreprise: Camping = new Camping();
    this.entrepriseService.addCamping(newEntreprise)
      .subscribe(data => {
        let camping: any = data;
        this.router.navigate(['entreprise/' + camping.entrepriseId + '/editEntreprise', {
          entrepriseId: camping.entrepriseId,
          entrepriseType:'CAMPING',
          isCreation:true
        }]);
      });
  }

  createEntreprise() {
    let newEntreprise: Entreprise = new Entreprise();
    this.entrepriseService.addEntreprise(newEntreprise)
      .subscribe(data => {
        let entreprise: any = data;
        this.router.navigate(['entreprise/' + entreprise.entrepriseId + '/editEntreprise', {
          entrepriseId: entreprise.entrepriseId,
          entrepriseType:'ENTREPRISE',
          isCreation:true
        }]);
      });
  }

  notAvailable() {
    this.messageService.add({severity:'info',summary:'Oops',detail:'Cette fonctionalité n\'est pas encore disponible'});
  }
}

