import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {UserService} from '../../../Services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CollaborateurserviceService} from '../../../Services/collaborateurservice.service';
import {User} from '../../../Model/User';
import {Entreprise} from '../../../Model/Entreprise';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {EntrepriseserviceService} from '../../../Services/entrepriseservice.service';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {AffectationEntrepriseUserComponent} from '../../../shared/dialogModal/affectation-entreprise-user/affectation-entreprise-user.component';
import {ConfirmModalComponent} from '../../../shared/dialogModal/confirm-modal/confirm-modal.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {
  roleList: any = [];
  changed: boolean;
  user: any = new User();
  userId: number;
  userForm: any;
  payLoad = '';
  rolesIds: string[] = [];
  roles: any[] = [];
  ref: DynamicDialogRef;
  hasManagerRole: boolean = false;
  roleControl = new FormControl([]);
  entrepriseList: Entreprise[] = [];
  displayedColumns: string[] = ['code', 'nomCommercial', 'emailContact', 'type', 'siret', 'telephone', 'star'];
  dataSource: MatTableDataSource<Entreprise> = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(public dialogService: DialogService, private fb: FormBuilder, private userService: UserService, private activateRoute: ActivatedRoute, private collaborateurService: CollaborateurserviceService, private router: Router, private entrepriseService: EntrepriseserviceService, public dialog: MatDialog,) {
    this.activateRoute.params.subscribe(data => {
      // this.user = data.user;
      this.userId = data.userId;
    });
  }

  ngOnInit(): void {
    this.userForm = this.fb.group({
      nom: [this.user.nom, Validators.required],
      prenom: [this.user.prenom, Validators.required],
      email: [this.user.email, Validators.required],
      username: [this.user.username, Validators.required],
    });
    this.getUser(this.userId);
    this.getManagerEntreprises(this.userId);

  }

  getUser(userId: number) {
    this.userService.getUser(userId)
      .subscribe(data => {
        this.user = data;
        this.getRoles();
      });
  }

  affectEntrepriseToUserDialog() {
    let ids =[];
  for(let ent of this.entrepriseList){
    ids.push(ent.entrepriseId);
  }
    this.ref = this.dialogService.open(AffectationEntrepriseUserComponent, {
      data: {
        userId: this.userId,
        ids : ids

      },
      header: 'Selection de l\' entreprise',
      width: '70%',
      contentStyle: {'max-height': '500px', 'overflow': 'auto'},
      baseZIndex: 10000
    });

    this.ref.onClose.subscribe((affectation: any) => {
      this.getManagerEntreprises(this.userId);
      console.log('it\'s ok');
    });

  }
  getManagerEntreprises(managerId: number) {
    this.entrepriseService.getManagerEntreprises(managerId)
      .subscribe(data => {
        this.entrepriseList =[];
        if(data._embedded.entreprises){
          this.entrepriseList = data._embedded.entreprises;
        }
        if(data._embedded.campings){
          this.entrepriseList =[...this.entrepriseList,...data._embedded.campings] ;
        }

        console.log(this.entrepriseList);
        this.dataSource = new MatTableDataSource(this.entrepriseList);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }
  getRoles() {
    this.hasManagerRole = false;
    this.userService.getAllRoles()
      .subscribe(data => {
        this.roleList = data._embedded.appRoles;
        this.roleList.forEach(r => {
          let s: string = ('' + r.id + '');

          if (this.user.rolesIds.indexOf(s) != -1) {
            if (r.roleName === 'COLLAB_FACILIT') {
              this.hasManagerRole = true;
            }
            this.roles.push(r);
          }
        });
      });
  }

  saveUpdates() {
    this.userService.updateUser(this.user)
      .subscribe(data => {
        console.log(data);
        this.goToUserList();

      });
  }

  onSubmit() {
    this.payLoad = JSON.stringify(this.userForm.getRawValue());
    console.log(this.payLoad);
    var itemsProcessed = 0;

    this.roles.forEach(r => {
      this.user.rolesIds.push(r.id);
      itemsProcessed++;
      if (itemsProcessed === this.roles.length) {
        this.saveUpdates();
      }
    });

  }

  goToUserList() {
    this.hasManagerRole = false;
    this.router.navigate(['/admin/users']);
  }

  onRoleRemoved(role: string) {
    const roles = this.roleControl.value as string[];
    this.removeFirst(roles, role);
    this.roleControl.setValue(roles); // To trigger change detection
  }

  private removeFirst<T>(array: T[], toRemove: T): void {
    const index = array.indexOf(toRemove);
    if (index !== -1) {
      array.splice(index, 1);
    }
  }

  beSureToDelete(entrepriseId: number) {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '500px',
      data: {message: 'Voulez-vous détâcher cette entreprise?', object: 'entreprise'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.detachEntreprise(entrepriseId);
      }
    });
  }

  detachEntreprise(entrepriseId: number) {
    this.entrepriseService.detachMannagerToEntreprise(entrepriseId, this.userId)
      .subscribe(data => {
        this.user = data;
        console.log(this.user);
        this.getManagerEntreprises(this.userId);
      });
  }
}
