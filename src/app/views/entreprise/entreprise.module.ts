import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EntrepriseRoutingModule} from './entreprise-routing.module';
import {EntreprisedashboardComponent} from './entreprisedashboard/entreprisedashboard.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ChartsModule} from 'ng2-charts';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {EntrepriseprofilComponent} from './entrepriseprofil/entrepriseprofil.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatSortModule} from '@angular/material/sort';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {PortalModule} from '@angular/cdk/portal';
import {OverlayModule} from '@angular/cdk/overlay';
import {MatTreeModule} from '@angular/material/tree';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSliderModule} from '@angular/material/slider';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSelectModule} from '@angular/material/select';
import {DateAdapter, MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatRadioModule} from '@angular/material/radio';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDividerModule} from '@angular/material/divider';
import {MatStepperModule} from '@angular/material/stepper';
import {MatChipsModule} from '@angular/material/chips';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatCardModule} from '@angular/material/card';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatButtonModule} from '@angular/material/button';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {CdkTreeModule} from '@angular/cdk/tree';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {A11yModule} from '@angular/cdk/a11y';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {ProgressBarModule} from 'primeng/progressbar';
import {TimelineModule} from 'primeng/timeline';
import {CardModule} from 'primeng/card';
import {ToastModule} from 'primeng/toast';
import {SharedModule} from '../../shared/shared/shared.module';
import {EntrepriseEditComponent} from './entreprise-edit/entreprise-edit.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {DialogModule} from "primeng/dialog";
import {DropdownModule} from "primeng/dropdown";
import {DateFormat} from "../../shared/shared/DateFormat";
import { ParametrageComponent } from './parametrage/parametrage.component';
import {DividerModule} from "primeng/divider";
import {PanelModule} from "primeng/panel";
import {MenuModule} from "primeng/menu";
import {ListboxModule} from "primeng/listbox";
import {RatingModule} from "primeng/rating";
import {AppModule} from "../../app.module";


@NgModule({
  declarations: [
    EntreprisedashboardComponent,
    EntrepriseprofilComponent,
    EntrepriseEditComponent,
    ParametrageComponent
  ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ChartsModule,
        MatDialogModule,
        BsDropdownModule,
        A11yModule,
        MatMomentDateModule,
        ClipboardModule,
        CdkStepperModule,
        CdkTableModule,
        CdkTreeModule,
        DragDropModule,
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatChipsModule,
        MatStepperModule,
        MatDatepickerModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatGridListModule,
        MatIconModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatPaginatorModule,
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSidenavModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatSortModule,
        MatTableModule,
        MatTabsModule,
        MatToolbarModule,
        MatTooltipModule,
        MatTreeModule,
        OverlayModule,
        PortalModule,
        ScrollingModule,
        DynamicDialogModule,
        TableModule,
        ButtonModule,
        ProgressBarModule,
        TimelineModule,
        CardModule,
        ToastModule,
        SharedModule,
        FlexLayoutModule,
        ButtonsModule.forRoot(),
        EntrepriseRoutingModule,
        DialogModule,
        MatNativeDateModule,
        DropdownModule,
        DividerModule,
        PanelModule,
        MenuModule,
        ListboxModule,
        RatingModule,
    ],
  providers:[{ provide: DateAdapter, useClass: DateFormat },]
})
export class EntrepriseModule {
  constructor(private dateAdapter: DateAdapter<Date>) {
    dateAdapter.setLocale("en-in"); // DD/MM/YYYY
  }
}
