import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Entreprise} from '../../../Model/Entreprise';

@Component({
  selector: 'app-entrepriseprofil',
  templateUrl: './entrepriseprofil.component.html',
  styleUrls: ['./entrepriseprofil.component.scss']
})
export class EntrepriseprofilComponent implements OnInit {
  entreprise: any = new Entreprise();

  constructor(public actvRoute: ActivatedRoute) {
    this.actvRoute.params.subscribe(filterEntreprise => {
      this.entreprise = filterEntreprise;
      console.log(filterEntreprise);
    });
  }

  ngOnInit(): void {
  }

}
