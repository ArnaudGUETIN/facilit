import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EntrepriseprofilComponent} from './entrepriseprofil.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ActivatedRoute, RouterModule} from "@angular/router";

describe('EntrepriseprofilComponent', () => {
  let component: EntrepriseprofilComponent;
  let fixture: ComponentFixture<EntrepriseprofilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EntrepriseprofilComponent],
      imports:[HttpClientModule,ReactiveFormsModule,FormsModule,RouterModule.forRoot([])],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntrepriseprofilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
