import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EntreprisedashboardComponent} from './entreprisedashboard/entreprisedashboard.component';
import {EntrepriseprofilComponent} from './entrepriseprofil/entrepriseprofil.component';
import {TaskViewComponent} from '../collaborateur/task-view/task-view.component';
import {TaskEditComponent} from '../collaborateur/task-edit/task-edit.component';
import {EntrepriseEditComponent} from './entreprise-edit/entreprise-edit.component';
import {ParametrageComponent} from "./parametrage/parametrage.component";
import {IndicateurComponent} from "../../shared/component/indicateur/indicateur.component";

const routes: Routes = [
  {
    path: ':id',
    component: EntreprisedashboardComponent,
    data: {
      title: 'Tableau de bord'
    }
  },
  {
    path: ':id/taskView',
    component: TaskViewComponent,
    data: {
      title: 'tâche'
    }
  },
  {
    path: ':id/taskEdit',
    component: TaskEditComponent,
    data: {
      title: 'tâche'
    }
  },
  {
    path: ':id/profil',
    component: EntrepriseprofilComponent,
    data: {
      title: 'Tableau de bord'
    }
  },
  {
    path: ':id/editEntreprise',
    component: EntrepriseEditComponent,
    data: {
      title: 'Edition entreprise'
    }
  },
  {
    path: ':id/indicateurs',
    component: IndicateurComponent,
    data: {
      title: 'Indicateurs graphiques'
    }
  },
  {
    path: ':id/parametrage',
    component: ParametrageComponent,
    data: {
      title: 'Edition parametrage'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EntrepriseRoutingModule {
}
