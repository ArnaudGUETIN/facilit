import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EntreprisedashboardComponent} from './entreprisedashboard.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {HttpClientModule} from "@angular/common/http";
import { RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DialogService} from "primeng/dynamicdialog";
import {MessageService} from "primeng/api";

describe('EntreprisedashboardComponent', () => {
  let component: EntreprisedashboardComponent;
  let fixture: ComponentFixture<EntreprisedashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EntreprisedashboardComponent],
      imports: [ReactiveFormsModule,FormsModule,HttpClientModule,RouterModule.forRoot([]),MatDialogModule],
      providers:[DialogService,{
        provide: MatDialogRef,
        useValue: {}
      },{
        provide: MAT_DIALOG_DATA,
        useValue: {}
      },MessageService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntreprisedashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
