import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {CollaborateurserviceService} from '../../../Services/collaborateurservice.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Entreprise} from '../../../Model/Entreprise';
import {MatDialog} from '@angular/material/dialog';
import {CollaborateurEditModalComponent} from '../../../shared/dialogModal/collaborateur-edit-modal/collaborateur-edit-modal.component';
import {TaskService} from '../../../Services/task.service';
import {TaskEditModalComponent} from '../../../shared/dialogModal/task-edit-modal/task-edit-modal.component';
import {DepartementServiceService} from '../../../Services/departement-service.service';
import {ServiceEditModalComponent} from '../../../shared/dialogModal/service-edit-modal/service-edit-modal.component';
import {Task} from '../../../Model/Task';
import {Service} from '../../../Model/Service';
import {MatTabChangeEvent} from '@angular/material/tabs';
import {ConfirmModalComponent} from '../../../shared/dialogModal/confirm-modal/confirm-modal.component';
import {Message, MessageService, PrimeIcons} from 'primeng/api';
import {EntrepriseserviceService} from '../../../Services/entrepriseservice.service';
import {Collaborateur} from "../../../Model/Collaborateur";
import {FormBuilder, Validators} from "@angular/forms";
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import {any} from "codelyzer/util/function";
import {environment} from "../../../../environments/environment";
export interface PeriodicElement {
  nom: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, nom: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, nom: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, nom: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, nom: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, nom: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, nom: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, nom: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, nom: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, nom: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, nom: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

/**
 * @title Table with sorting
 */
@Component({
  selector: 'app-entreprisedashboard',
  templateUrl: './entreprisedashboard.component.html',
  styleUrls: ['./entreprisedashboard.component.scss']
})
export class EntreprisedashboardComponent implements OnInit, AfterViewInit {


lineChartCA : ChartDataModel = {
  libelle:'',
  rate:'25%',
  diff:'+0',
  value:0,
  data:[],
  labels:[],
  options:environment.lineChartDefaultOption,
  colors:[{
    backgroundColor: 'transparent',
    borderColor: 'rgba(255,255,255,.55)',
    borderWidth: 3
  }],
  legend:true,
  type:'line'
};
lineChartNbBooking : ChartDataModel = {
  libelle:'',
  rate:'15%',
  diff:'+41',
  value:0,
  data:[],
  labels:[],
  options:environment.lineChartDefaultOption,
  colors:[{
    backgroundColor: 'transparent',
    borderColor: 'rgba(255,255,255,.55)',
    borderWidth: 3
  }],
  legend:true,
  type:'line'
};



  // lineChart4
  public lineChart4Data: Array<any> = [
    {
      data: [4, 18, 9, 17, 34, 22, 11, 3, 15, 12, 18, 9],
      label: 'Series A'
    }, {
      data: [2, 11, 7, 20, 22, 34, 11, 3, 15, 12, 18, 9],
      label: 'Series B'
    }
  ];
  public lineChart4Labels: Array<any> = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public lineChart4Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: false,
        points: false,
      }],
      yAxes: [{
        display: false,
      }]
    },
    elements: { point: { radius: 0 } },
    legend: {
      display: false
    }
  };
  public lineChart4Colours: Array<any> = [
    {
      backgroundColor: 'transparent',
      borderColor: 'rgba(255,255,255,.55)',
      borderWidth: 2
    }
  ];
  public lineChart4Legend = false;
  public lineChart4Type = 'line';


  // barChart2
  public barChart2Data: Array<any> = [
    {
      data: [4, 18, 9, 17, 34, 22, 11, 3, 15, 12, 18, 9],
      label: '2020',
      barPercentage: 0.6
    },
    {
      data: [14, 8, 9, 7, 24, 18, 13, 23, 11, 19, 2, 7],
      label: '2021',
      barPercentage: 0.6
    }
  ];
  public barChart2Labels: Array<any> = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public barChart2Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: false,
      }],
      yAxes: [{
        display: false,
        ticks: {
          beginAtZero: true,
        }
      }]
    },
    legend: {
      display: false
    }
  };
  public barChart2Colours: Array<any> = [
    {
      backgroundColor: 'rgba(0,0,0,.2)',
      borderWidth: 0
    }
  ];
  public barChart2Legend = false;
  public barChart2Type = 'bar';










  fonctionList: any = [
    {label: 'EMPLOYE', code: 0},
    {label: 'CHEF DE SERVICE', code: 1},
    {label: 'DIRIGEANT', code: 2},
  ];
  collaborateurList: any = [];
  taskList: any = [];
  serviceList: any = [];
  entreprise: any = new Entreprise();
  entrepriseId: any;
  radarChartData: any = [
    {data: [65, 59, 90, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 96, 27, 100], label: 'Series B'}
  ];
  radarChartType = 'radar';
  // Radar
  radarChartLabels: string[] = ['Finance & gestion', 'Marketing', 'RH', 'Planning', 'Audit', 'Cycling', 'Running'];

  // Pie
  pieChartLabels: string[] = ['Terminé', 'En cours', 'Affectées'];
  pieChartData: number[] = [300, 500, 100];
  pieChartType = 'pie';
  displayedColumns: string[] = ['matricule','nom', 'prenom', 'email', 'telephone','numeroSecu', 'fonction', 'profil', 'star'];
  taskDisplayedColumns: string[] = ['numero','designation', 'commentaire', 'instruction', 'dateDebut', 'dateFin','nature','type', 'star'];
  serviceDisplayedColumns: string[] = ['code', 'designation', 'responsable', 'star'];
  dataSource: MatTableDataSource<Entreprise> = new MatTableDataSource();
  taskDataSource: MatTableDataSource<Task> = new MatTableDataSource();
  serviceDataSource: MatTableDataSource<Service> = new MatTableDataSource();
  events1: any[];
  showLoader: any;
  events2: any[];
  msgs: Message[] = [];
  @ViewChild('sort') sort: MatSort;
  @ViewChild('paginator') paginator: MatPaginator;
  @ViewChild('sortTask') sortTask: MatSort;
  @ViewChild('paginatorTask') paginatorTask: MatPaginator;
  @ViewChild('sortService') sortService: MatSort;
  @ViewChild('paginatorService') paginatorService: MatPaginator;
  @ViewChild('group') group: ElementRef;

  collaborateurForm: any;
  serviceForm: any;
  service: Service = new Service();
  payLoad = '';
  collaborateur: any = {};
  displayCollaborateurEditModal: boolean = false;

  constructor(public dialog: MatDialog, private collaborateurService: CollaborateurserviceService, public actvRoute: ActivatedRoute, public router: Router, public taskService: TaskService, public departmentService: DepartementServiceService, private entrepriseService: EntrepriseserviceService,
              private messageService: MessageService,private fb: FormBuilder, private departementService: DepartementServiceService) {
    this.actvRoute.params.subscribe(filterEntreprise => {
      this.entrepriseId = filterEntreprise.id;

      console.log(filterEntreprise);
    });
    this.actvRoute.queryParams.subscribe(filterEntreprise => {
      if(filterEntreprise.selectedIndex){
        this.selectedIndex = filterEntreprise.selectedIndex;

      }
    })
  }

  ngAfterViewInit() {


  }

  applyCollabFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  applyTaskFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.taskDataSource.filter = filterValue.trim().toLowerCase();
  }

  applyServiceFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.serviceDataSource.filter = filterValue.trim().toLowerCase();
  }

  getOneEntreprise(entrepriseId: number) {
    this.entrepriseService.getOneEntrepriseById(entrepriseId)
      .subscribe(data => {
        this.entreprise = data;
        this.entrepriseService.CURRENT_ENTREPRISE.next(this.entreprise);
      });
  }
  initCollabForm(){
    this.collaborateurForm = this.fb.group({
      email: ['', Validators.required],
      matricule: [''],
      dateNaissance: [this.collaborateur.dateNaissance],
      lieuNaissance: [''],
      fonction: [''],
      block: [''],
      rue: [''],
      ville: [''],
      codePostal: [''],
      service: [''],
      nom: ['',Validators.required],
      prenom: ['', Validators.required],
      niveauHierarchique: [''],
      profil: [''],
      telephone: ['',Validators.required],
      numeroSecuriteSociale: ['',Validators.required],
      serviceIds: [this.collaborateur.serviceIds],

    });
  }
  ngOnInit(): void {
    this.getOneEntreprise(this.entrepriseId);
    let entrepriseId = this.entrepriseId;
    if (!entrepriseId) {
      entrepriseId = Number(localStorage.getItem('entrepriseId'));
    }
    this.getAllCollaborateurs(entrepriseId);
    this.getAllTasks(entrepriseId);
    this.getAllService(entrepriseId);
    this.initCollabForm();
    this.getRespobsables();
    this.events1 = [
      {
        status: '1er checkpoint annuel',
        date: '15/10/2020 10:30',
        icon: PrimeIcons.SHOPPING_CART,
        color: '#9C27B0',
        image: 'game-controller.jpg'
      },
      {
        status: '2ème checkpoint annuel',
        date: '15/10/2020 14:00',
        icon: PrimeIcons.COG,
        color: '#673AB7'
      },
      {
        status: '3ème checkpoint annuel',
        date: '15/10/2020 16:15',
        icon: PrimeIcons.ENVELOPE,
        color: '#FF9800'
      },
      {
        status: '4ème checkpoint annuel',
        date: '16/10/2020 10:00',
        icon: PrimeIcons.CHECK,
        color: '#607D8B'
      }
    ];
    this.events2 = ['2020', '2021', '2022', '2023'];

    this.serviceForm = this.fb.group({
      code: ['', Validators.required],
      designation: ['', Validators.required],
      responsable: ['', Validators.required],

    });
    this.entrepriseService.getChartData().subscribe(data=>{
      let payloadCAN = [];
      let payloadCAN1 = [];
      let payloadBookingN = [];
      let payloadBookingN1 = [];
      for(let i=0; i<12; i++){
        payloadCAN.push(data[0].ProductList[0].BookingWindowMonthData[i].CaN);
        payloadCAN1.push(data[0].ProductList[0].BookingWindowMonthData[i].CaN1);
        payloadBookingN.push(data[0].ProductList[0].BookingWindowMonthData[i].NbBookingN);
        payloadBookingN1.push(data[0].ProductList[0].BookingWindowMonthData[i].NbBookingN1);
      }
      this.lineChartCA.libelle = "CHIFFRE D'AFFAIRES TTC";
      this.lineChartCA.value = "1.890,65 €";
      this.lineChartCA.data =[{data:payloadCAN,label:new Date().getFullYear()},{data:payloadCAN1,label:new Date().getFullYear()-1,borderColor: 'rgba(255, 99, 132, 1)',backgroundColor: 'transparent'}];
      this.lineChartCA.labels =environment.dateShortLabels;

      this.lineChartNbBooking.libelle = "NB de réservation";
      this.lineChartNbBooking.value = "1.474 ";
      this.lineChartNbBooking.data = [{data:payloadBookingN,label:new Date().getFullYear()},{data:payloadBookingN1,label:new Date().getFullYear()-1,borderColor: 'rgba(25, 99, 132, 1)',backgroundColor: 'transparent'}];
      this.lineChartNbBooking.labels =environment.dateShortLabels;
    })
  }

  // events
  displaytypeTask: boolean =false;
  selectedIndex: any = 0;
  displayService: boolean = false;
  responsableList: any = [];


  onChange(event: MatTabChangeEvent) {
    const tab = event.tab.textLabel;
    if (tab === 'Liste des collaborateurs') {
      this.getAllCollaborateurs(this.entreprise.entrepriseId);
    }
    if (tab === 'Liste des tâches') {
      this.getAllTasks(this.entreprise.entrepriseId);
    }
    if (tab === 'Liste des services') {
      this.getAllService(this.entreprise.entrepriseId);
    }

    this.group.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
  }
  getAllCollaborateurs(entrepriseId: number) {
    this.collaborateurService.getAllEntrepriseCollaborateurs(entrepriseId, 0, -1)
      .subscribe(data => {
        console.log(data);
        this.collaborateurList = data;
        this.dataSource = new MatTableDataSource(this.collaborateurList);
        setTimeout(() => {
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        },1000);
      });
  }

  getAllTasks(entrepriseId: number) {
    this.taskService.getAllEntrepriseTasks(entrepriseId)
      .subscribe(data => {
        if (data){
          this.taskList = data._embedded.tasks;
          this.taskDataSource = new MatTableDataSource(this.taskList);
          this.taskDataSource.sort = this.sortTask;
          this.taskDataSource.paginator = this.paginatorTask;
        }

      });
  }

  getAllService(entrepriseId: number) {
    this.departmentService.getAllEntrepriseServices(entrepriseId)
      .subscribe(data => {
        console.log(data);
        if (data) {
          this.serviceList = data._embedded.services;
          this.serviceDataSource = new MatTableDataSource(this.serviceList);
          setTimeout(() => {
            this.serviceDataSource.sort = this.sortService;
            this.serviceDataSource.paginator = this.paginatorService;
          },1000);
        }

      });
  }

  openAddCollabDialog(isEdit: boolean) {



    this.displayCollaborateurEditModal = true;
    this.collaborateur = new Collaborateur();
  }

  openAddTaskDialog(isEdit: boolean) {

    if (isEdit) {

    } else {
      const dialogRef = this.dialog.open(TaskEditModalComponent, {
        width: '500px',
        data: {entrepriseId: this.entreprise.entrepriseId, task: null, isModify: false}
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          let task = JSON.parse(result.task);
          if (task && task.taskId && task.taskId != 0) {
            console.log(task);
            this.taskList.push(task);
            this.taskList = [...this.taskList];
            this.taskDataSource = new MatTableDataSource(this.taskList);
            this.taskDataSource.sort = this.sort;
            this.taskDataSource.paginator = this.paginator;
            this.messageService.add({severity: 'success', summary: 'Success', detail: 'Ajout effectué '});
            console.log(this.taskList);
          }
        }
      });
    }
  }

  openModifyDialog(collaborateur: any) {
    console.log(collaborateur);
    this.collaborateur = collaborateur;
    this.collaborateurForm.get('dateNaissance').setValue(new Date(collaborateur.dateNaissance));
    this.displayCollaborateurEditModal = true;
    this.getAllService(this.entrepriseId);


  }

  openModifyTaskDialog(task: any) {
    console.log(task);

    const dialogRef = this.dialog.open(TaskEditModalComponent, {
      width: '500px',
      data: {entrepriseId: this.entreprise.entrepriseId, task: task, isModify: true}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);

      if (result && result.isModifyPerformed) {
        let task = JSON.parse(result.task);
        console.log(task);
        const elementsIndex = this.taskList.findIndex(element => element.taskId == task.taskId);
        let newArray = [...this.taskList];
        newArray[elementsIndex] = task;
        this.taskList[elementsIndex] = task;
        this.taskDataSource = new MatTableDataSource(newArray);
        this.taskDataSource.sort = this.sortTask;
        this.taskDataSource.paginator = this.paginatorTask;
        this.messageService.add({severity: 'info', summary: 'Info', detail: 'Modification effectuée'});
      }

    });

  }

  openModifyServiceDialog(service: any) {
    this.displayService = true;
    this.service = service;

  }

  openAddServiceEditDialog(isEdit: boolean) {

   this.displayService = true;


  }

  onSubmitService(){

    if (this.serviceForm.invalid) {
      return;
    }
    this.payLoad = JSON.stringify(this.serviceForm.getRawValue());
    console.log(this.payLoad);
    console.log(this.entrepriseId);
    if (this.service.serviceId==0) {
      this.departementService.addService(this.entrepriseId, this.service)
        .subscribe((data:any) => {
          this.service = data;
          this.getAllService(this.entrepriseId);
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Ajout effectué '});
          this.displayService = false;
          this.serviceForm.reset();
        });
    } else {

      this.service.entrepriseId = this.entrepriseId;
      this.departementService.updateService(this.service)
        .subscribe((data:any) => {
          this.service = data;
          this.getAllService(this.entrepriseId);
          this.serviceDataSource = new MatTableDataSource(this.serviceList);
          this.serviceDataSource.sort = this.sortService;
          this.serviceDataSource.paginator = this.paginatorService;
          this.messageService.add({severity: 'info', summary: 'Info', detail: 'Modification effectuée'});
          this.serviceForm.reset();
        });
      this.displayService = false;

    }

  }
  displayCollaborateur(row: any) {
    this.router.navigate(['collaborateur', row]);

  }

  viewTask(task: any) {
    this.router.navigate(['entreprise/' + this.entrepriseId + '/taskEdit', {
      taskId: task.taskId,
      entrepriseId: this.entrepriseId,
      task: task
    }]);
  }

  newTask(taskType:number) {
    let taskToSave = new Task();
    taskToSave.typeTache = taskType;
    this.taskService.addTask(this.entrepriseId, taskToSave)
      .subscribe(data => {
        let task: any = data;
        console.log(data);
        this.router.navigate(['entreprise/' + this.entrepriseId + '/taskEdit', {
          taskId: task.taskId,
          entrepriseId: this.entrepriseId,
          task: task
        }]);
      });

  }

  beSureToDelete(collaborateurId: number) {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '500px',
      data: {message: 'Voulez-vous supprimez ce collaborateur?', object: 'collaborateur'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.deleteCollaborateur(collaborateurId);
      }
    });
  }

  beSureToDeleteTask(taskId: number) {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '500px',
      data: {message: 'Voulez-vous supprimez cette tâche?', object: 'tâche'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.deleteTask(taskId);
      }
    });
  }

  beSureToDeleteService(service: any) {
    const dialogRef = this.dialog.open(ConfirmModalComponent, {
      width: '500px',
      data: {message: 'Voulez-vous supprimez ce service?', object: 'service'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1) {
        this.deleteService(service);
      }
    });
  }

  deleteCollaborateur(collaborateurId: number) {
    this.collaborateurService.deleteCollaborateur(collaborateurId)
      .subscribe(data => {
        const elementsIndex = this.collaborateurList.findIndex(element => element.collaborateurId == collaborateurId);

        this.collaborateurList.splice(elementsIndex, 1);

        console.log(this.collaborateurList);
        this.dataSource = new MatTableDataSource(this.collaborateurList);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }

  deleteTask(taskId: number) {
    this.taskService.deleteTask(taskId)
      .subscribe(data => {
        const elementsIndex = this.taskList.findIndex(element => element.taskId == taskId);

        this.taskList.splice(elementsIndex, 1);

        console.log(this.taskList);
        this.taskDataSource = new MatTableDataSource(this.taskList);
        this.taskDataSource.sort = this.sortTask;
        this.taskDataSource.paginator = this.paginatorTask;
      });
  }

  deleteService(service: any) {
    this.departmentService.deleteService(service)
      .subscribe(data => {
        const elementsIndex = this.serviceList.findIndex(element => element.serviceId == service.serviceId);

        this.serviceList.splice(elementsIndex, 1);

        console.log(this.serviceList);
        this.taskDataSource = new MatTableDataSource(this.serviceList);
        this.serviceDataSource.sort = this.sortService;
        this.serviceDataSource.paginator = this.paginatorService;
      });
  }


  onSubmitCollaborateur() {
    console.group(this.collaborateurForm);
    if (this.collaborateurForm.invalid) {
      return;
    }
    this.showLoader = true;
    this.payLoad = JSON.stringify(this.collaborateurForm.getRawValue());
    console.group(this.payLoad);
    if (!this.collaborateur || this.collaborateur.collaborateurId == 0 ) {
      this.collaborateurService.addCollaborateur(this.entrepriseId, JSON.parse(this.payLoad))
        .subscribe(collaborateur => {
          this.getAllCollaborateurs(this.entrepriseId);
          this.showLoader = false;
          this.displayCollaborateurEditModal = false;
          this.collaborateurForm.reset();
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Collaborateur ajouté avec succès'});
        });
    } else {

        this.collaborateurService.updateCollaborateur(this.collaborateur)
          .subscribe(data => {
            this.getAllCollaborateurs(this.entrepriseId);
            this.showLoader = false;
            this.displayCollaborateurEditModal = false;
            this.messageService.add({severity: 'success', summary: 'Success', detail: 'Collaborateur modifié avec succès'});
            this.collaborateurForm.reset();
          });


    }


  }

  editTask(task: any) {
    this.router.navigate(['entreprise/' + this.entrepriseId + '/taskEdit', {
      taskId: task.taskId,
      entrepriseId: this.entrepriseId,
      task: task
    }]);
  }
  getRespobsables() {
    this.collaborateurService.getAllResponsable(0, this.entrepriseId)
      .subscribe(data => {
        console.log(data);
        this.responsableList = data._embedded.collaborateurs;
      });
  }
  goToIndicators() {
    this.router.navigate(['entreprise/' + this.entreprise.entrepriseId + '/indicateurs', {
      entrepriseId: this.entreprise.entrepriseId,
      entrepriseType: this.entreprise.entrepriseType
    }]);
  }
}

export interface ChartDataModel {
  libelle:any,
  value:any,
  rate:any,
  diff:any,
  data?: Array<any>,
  labels?: Array<any>,
  options?: any,
  colors?: Array<any>,
  legend?: boolean,
  type?: string

}
