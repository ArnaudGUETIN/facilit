import { Component, OnInit } from '@angular/core';
import {MenuItem} from "primeng/api";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {InstallationService} from "../../../Services/installation.service";
interface City {
  name: string,
  code: string
}
@Component({
  selector: 'app-parametrage',
  templateUrl: './parametrage.component.html',
  styleUrls: ['./parametrage.component.scss']
})
export class ParametrageComponent implements OnInit {
  items: MenuItem[];
  cities: City[];
  installations: [];

  selectedCity: City;
  displayInstallationModal = false;
  installationForm:FormGroup;
  showLoader: any =false;
  entrepriseId = 0;
  constructor(private formBuilder:FormBuilder,private installationService:InstallationService) { }

  ngOnInit(): void {
    this.entrepriseId = +localStorage.getItem('entrepriseId');
    this.getInstallations();
    this.items = [
      {
        label: 'Actions',
        items: [{
          label: 'Ajouter',
          icon: 'pi pi-plus',
          command: () => {
           this.displayInstallationModal = true;
          }
        },
          {
            label: 'Delete',
            icon: 'pi pi-times',
            command: () => {

            }
          }
        ]},
    ];
    this.installationForm = this.formBuilder.group({
      type:[Validators.required],
      designation:['',Validators.required],
      nombre:[1,Validators.required],
      fournisseur:['',Validators.required],
    })
    this.installationForm.get('type').valueChanges.subscribe(value=>{
      if(value==2){
        this.installationForm.addControl('numero',new FormControl('', Validators.required));
        this.installationForm.addControl('valeur',new FormControl(0, Validators.required));
        this.installationForm.addControl('capacite',new FormControl('', Validators.required));
        this.installationForm.addControl('nature',new FormControl('', Validators.required));
      }else {
        this.installationForm.removeControl('numero');
        this.installationForm.removeControl('valeur');
        this.installationForm.removeControl('capacite');
        this.installationForm.removeControl('nature');
      }
    })
  }

  getInstallations(){
    if(this.entrepriseId!=0){
      this.installationService.getAllEntrepriseInstallations(this.entrepriseId)
        .subscribe(data=>{
          this.installations = data;
        })
    }

  }
  saveInstallation() {
    this.showLoader = true;
    let payload:any = {
      type:this.installationForm.get('type')?.value,
      designation:this.installationForm.get('designation')?.value,
      nombre:this.installationForm.get('nombre')?.value,
      fournisseur:this.installationForm.get('fournisseur')?.value,
      entrepriseId:this.entrepriseId,
    }
    if(this.installationForm.get('type').value==2){
      payload.numero = this.installationForm.get('numero')?.value;
      payload.valeur = this.installationForm.get('valeur')?.value;
      payload.capacite = this.installationForm.get('capacite')?.value;
      payload.nature = this.installationForm.get('nature')?.value;
      this.installationService.addCompteur(payload)
        .subscribe(data=>{
          console.log(data);
          this.showLoader = false;
          this.displayInstallationModal = false;
          this.getInstallations();
        })
    }else {
      this.installationService.addInstallation(payload)
        .subscribe(data=>{
          console.log(data);
          this.showLoader = false;
          this.displayInstallationModal = false;
          this.getInstallations();
        })
    }

  }
}
