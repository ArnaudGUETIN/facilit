import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MenuItem, MessageService} from 'primeng/api';
import {Entreprise} from '../../../Model/Entreprise';
import {EntrepriseserviceService} from '../../../Services/entrepriseservice.service';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {Observable} from 'rxjs';
import {HttpEventType, HttpResponse} from '@angular/common/http';
import {UploadFilesService} from '../../../Services/upload-files.service';
import {emailExistValidator} from "../../../shared/dialogModal/collaborateur-edit-modal/collaborateur-edit-modal.component";
import {UserService} from "../../../Services/user.service";
interface Nature {
  name: string,
  code: number
}
@Component({
  selector: 'app-entreprise-edit',
  templateUrl: './entreprise-edit.component.html',
  styleUrls: ['./entreprise-edit.component.scss']
})
export class EntrepriseEditComponent implements OnInit, OnDestroy {
  emplacementSwtich = false;
  natures: Nature[];
  entrepriseForm: any;
  ribForm: any;
  emplacementForm: any;
  entreprise: any = new Entreprise();
  entrepriseId: number = 0;
  entrepriseType: string = '';
  isCreation = false;
  private formReady: boolean;
  isActive: boolean = true;
  image: string | SafeUrl =
    'assets/img/avatars/office-building.png';
  selectedFiles?: FileList;
  currentFile?: File;
  progress = 0;
  showProgress = false;
  message = '';
  items: MenuItem[];
  fileInfos?: Observable<any>;
  private rib: any={};
  private emplacementsLocatif: any=[];
  private emplacementsLibre: any=[];
  private emplacementSelectEdit: any={};
  displayEmplacementEditModal: boolean=false;
  displayRibEditModal: boolean=false;
  alreadyExist: boolean = false;
  showLoader: any;


  constructor(private uploadService: UploadFilesService, private sanitizer: DomSanitizer, private activateRoute: ActivatedRoute,
              private router: Router, private fb: FormBuilder, private messageService: MessageService
              , private entrepriseService: EntrepriseserviceService,private userService:UserService) {
    console.log(this.activateRoute)
    this.activateRoute.params.subscribe(data => {
      this.entrepriseId = data.entrepriseId;
      this.entrepriseType = data.entrepriseType;
      this.isCreation = data.isCreation;
      if(!this.entrepriseId){
        this.activateRoute.queryParams.subscribe(param=>{
          this.entrepriseId = param.entrepriseId;
        })
      }
    });
    this.natures = [
      {name: 'Mobile home', code: 1},
      {name: 'Chalêt', code: 2},
      {name: 'Chambre', code: 3}
    ];
  }


  ngOnInit(): void {
    if (this.entrepriseType === 'CAMPING') {
      this.getCamping();
    } else {
      this.getEntreprise();
    }
    this.initEmplacementFormGroup();
    this.items = [
      {
        label: 'Actions',
        items: [{
          label: 'Ajouter',
          icon: 'pi pi-plus',
          command: () => {
            this.displayEmplacement({type:2}) ;
          }
        }
        ]},
    ];
  }

  ngOnDestroy() {
    console.log(this.entrepriseForm.invalid)
    if(this.entrepriseForm.invalid && this.isCreation){
      this.entrepriseService.deleteEntreprise(this.entrepriseId)
        .subscribe(data=>{
          this.messageService.add({severity: 'info', summary: 'Information', detail: 'l\' entreprise que vous venez de créer a été suprimer'});
        })
    }

  }
  getCamping() {
    this.entrepriseService.getOneCampingById(this.entrepriseId)
      .subscribe(data => {
        this.entreprise = data;
        this.uploadService.getFiles('entreprise', this.entrepriseId, 'ent-' + this.entrepriseId);
        this.initCampingFormGroup();
        this.getEmplacements();
        this.entrepriseService.getEntrepriseRib(this.entrepriseId)
          .subscribe(ribData=>{
            if(ribData){
              this.rib = ribData;
            }
            console.log(this.rib);
          })
      });
  }

  getEntreprise() {
    this.entrepriseService.getOneEntrepriseById(this.entrepriseId)
      .subscribe(data => {
        this.entreprise = data;
       // this.uploadService.getFiles('entreprise', this.entrepriseId, 'ent-' + this.entrepriseId);
        this.entrepriseService.getEntrepriseRib(this.entrepriseId)
          .subscribe(ribData=>{
            if(ribData){
              this.rib = ribData;
            }
            console.log(this.rib);
          })
        this.initEntrepriseFormGroup();
      });
  }
  onSubmitEmplacement(){
    console.log(this.emplacementForm);
    if (this.emplacementForm.invalid) {
      this.messageService.add({severity: 'error', summary: 'Problème de saisie', detail: "Vérifiez votre saisie"});
      return;
    }
    if(this.emplacementSelectEdit.emplacementId && this.emplacementSelectEdit.emplacementId!=0){
      this.entrepriseService.updateCampingEmpl(this.emplacementSelectEdit)
        .subscribe(data=>{
          this.getEmplacements();
          this.displayEmplacementEditModal = false;
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Emplacement modifié avec succès'});
        })
    }else{
      this.emplacementSelectEdit.campingId = this.entrepriseId;
      this.entrepriseService.addCampingEmpl(this.emplacementSelectEdit)
        .subscribe(data=>{
          this.getEmplacements();
          this.displayEmplacementEditModal = false;
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Emplacement ajouté avec succès'});
        })
    }

  }
  async onSubmit() {
    this.showLoader = true;
    let payload = this.entrepriseForm.value;
    payload.entrepriseId = this.entreprise.entrepriseId;

    setTimeout(()=>{
      console.log(this.entrepriseForm);
      if (this.entrepriseForm.invalid) {
        this.showLoader = false;
        this.messageService.add({severity: 'error', summary: 'Problème de saisie', detail: "Vérifiez votre saisie"});
        return;
      }
      if (payload.entrepriseType === 'ENTREPRISE') {
        this.entrepriseService.updateEntreprise(payload)
          .subscribe(data => {
            this.showLoader = false;
            this.router.navigate(['entreprise/' + this.entreprise.entrepriseId]);
          });
      } else if (payload.entrepriseType === 'CAMPING') {
        this.entrepriseService.updateCamping(payload)
          .subscribe(data => {
            this.showLoader = false;
            this.router.navigate(['entreprise/' + this.entreprise.entrepriseId]);
          });
      }
    },1000)


  }

  selectedFile() {
    let input = document.getElementById('input-img');
    //input.type = 'file';
    //input.accept="image/*";
    input.click();
  }

  upload() {

    this.showProgress = true;
    this.progress = 0;

    if (this.selectedFiles) {
      const file: File | null = this.selectedFiles.item(0);

      if (file) {
        this.currentFile = file;

        this.uploadService.upload(this.currentFile, 'entreprise', this.entrepriseId).subscribe(
          (event: any) => {
            if (event.type === HttpEventType.UploadProgress) {
              this.progress = Math.round(100 * event.loaded / event.total);
              console.log(this.progress);
            } else if (event instanceof HttpResponse) {
              this.message = event.body;
              this.messageService.add({severity: 'success', summary: 'Success', detail: this.message});
              this.getEntreprise();
             // this.uploadService.getFiles('entreprise', this.entrepriseId, 'ent-' + this.entrepriseId);
            }
            this.showProgress = false;
          },
          (err: any) => {
            console.log(err);
            this.progress = 0;

            if (err.error && err.error.message) {
              this.message = err.error.message;
              this.messageService.add({severity: 'error', summary: 'Error', detail: this.message});
            } else {
              this.message = 'Could not upload the file!';
              this.messageService.add({severity: 'error', summary: 'Error', detail: this.message});
            }

            this.currentFile = undefined;
          });
      }

      this.selectedFiles = undefined;
    }
  }

  updateImage(ev) {
    console.log(ev);
    this.selectedFiles = ev.target.files;
    this.image = this.sanitizer.bypassSecurityTrustUrl(
      window.URL.createObjectURL(ev.target.files[0])
    );
  }

  private initEntrepriseFormGroup() {
    this.entrepriseForm = this.fb.group({
      emailContact: [this.entreprise.emailContact, [Validators.email,Validators.required]],
      code: [this.entreprise.code, Validators.required],
      nomCommercial: [this.entreprise.nomCommercial, Validators.required],
      siren: [this.entreprise.siren, Validators.required],
      nic: [this.entreprise.nic, Validators.required],
      rcs: [this.entreprise.rcs, Validators.required],
      codeAPE: [this.entreprise.codeAPE, Validators.required],
      type: [this.entreprise.type],
      statutJuridique: [this.entreprise.statutJuridique],
      telephone: [this.entreprise.telephone, Validators.required],
      telephone2: [this.entreprise.telephone2],
      ville: [this.entreprise.ville, Validators.required],
      ville2: [this.entreprise.ville2],
      codePostal: [this.entreprise.codePostal, Validators.required],
      codePostal2: [this.entreprise.codePostal2],
      entrepriseType: [this.entreprise.entrepriseType, Validators.required],
      block: [this.entreprise.block, Validators.required],
      block2: [this.entreprise.block2],
      rue: [this.entreprise.rue, Validators.required],
      rue2: [this.entreprise.rue2],
      pays: [this.entreprise.pays],
      pays2: [this.entreprise.pays2],
      numeroTva: [this.entreprise.numeroTva],
      raisonSociale: [this.entreprise.raisonSociale],
      website: [this.entreprise.website],

    },{updateOn: 'change'});
    const emailControl = <FormControl>this.entrepriseForm.get('emailContact');
    emailControl.valueChanges.subscribe(value=>{
      this.checkIfEmailExist(value, emailControl);

    })
    this.initRibFormGroup();
    this.formReady = true;
  }

   checkIfEmailExist(value, emailControl: FormControl) {
    if (value !== this.entreprise.emailContact) {
      this.userService.userEmailAlreadyExist(value)
        .subscribe(data => {
          this.alreadyExist = data;
          console.log(this.alreadyExist)
          emailControl.clearValidators();
          emailControl.setValidators([Validators.email, emailExistValidator(this.alreadyExist), Validators.required]);
        })
    }
  }

  private initEmplacementFormGroup() {
    this.emplacementForm = this.fb.group({
      code: ['',[Validators.required]],
      designation: ['',[Validators.required]],
      type: [this.emplacementSelectEdit.type, Validators.required],
      capacite: ['',[Validators.required]],
      superficieMoyenne: ['',[Validators.required]],
    })
    this.emplacementForm.get('type').valueChanges.subscribe(value=>{
      if(value==1){
        this.emplacementSwtich = true;
        this.emplacementForm.addControl('nature',new FormControl('',[Validators.required]));
        this.emplacementForm.addControl('nombreChambre',new FormControl('',[Validators.required]));
        this.emplacementForm.addControl('nombreSDB',new FormControl('',[Validators.required]));
        this.emplacementForm.addControl('nombreTotalEmp',new FormControl('',[Validators.required]));
      }else {
        this.emplacementSwtich = false;
        this.emplacementForm.removeControl('nature');
        this.emplacementForm.removeControl('nombreChambre');
        this.emplacementForm.removeControl('nombreSDB');
        this.emplacementForm.removeControl('nombreTotalEmp');
      }
    })


  }
  private initRibFormGroup() {
    this.ribForm = this.fb.group({
      banque: ['', Validators.required],
      bic: ['', Validators.required],
      cle: ['', Validators.required],
      domiciliation: [''],
      guichet: ['', Validators.required],
      iban: ['', Validators.required],
      libelle: ['', Validators.required],
      numeroDeCompte: ['', Validators.required],
      ville: [this.entreprise.ville, Validators.required],
      codePostal: [this.entreprise.codePostal, Validators.required],
      block: [this.entreprise.block, Validators.required],
      rue: [this.entreprise.rue, Validators.required],
      pays: [this.entreprise.pays],
    })
  }

  private initCampingFormGroup() {
    this.entrepriseForm = this.fb.group({
      emailContact: [this.entreprise.emailContact, [Validators.email,Validators.required]],
      code: [this.entreprise.code, Validators.required],
      nomCommercial: [this.entreprise.nomCommercial, Validators.required],
      siren: [this.entreprise.siren, Validators.required],
      nic: [this.entreprise.nic, Validators.required],
      rcs: [this.entreprise.rcs, Validators.required],
      codeAPE: [this.entreprise.codeAPE, Validators.required],
      statutJuridique: [this.entreprise.statutJuridique],
      type: [this.entreprise.type],
      telephone: [this.entreprise.telephone, Validators.required],
      telephone2: [this.entreprise.telephone2],
      ville: [this.entreprise.ville, Validators.required],
      ville2: [this.entreprise.ville2],
      codePostal: [this.entreprise.codePostal, Validators.required],
      codePostal2: [this.entreprise.codePostal2],
      entrepriseType: [this.entreprise.entrepriseType, Validators.required],
      block: [this.entreprise.block, Validators.required],
      block2: [this.entreprise.block2],
      rue: [this.entreprise.rue, Validators.required],
      rue2: [this.entreprise.rue2],
      pays: [this.entreprise.pays],
      pays2: [this.entreprise.pays2],
      numeroTva: [this.entreprise.numeroTva],
      raisonSociale: [this.entreprise.raisonSociale],
      website: [this.entreprise.website],
      classement: [this.entreprise.classement, Validators.required],
      dateDernierClassement: [this.entreprise.dateDernierClassement, Validators.required],
      dateProchainClassement: [this.entreprise.dateProchainClassement, Validators.required],
      nombreEmplacement: [this.entreprise.nombreEmplacement, Validators.required],
      dateCreation: [this.entreprise.dateCreation, Validators.required],
      superficie: [this.entreprise.superficie, Validators.required],
      //gouvernance: [this.entreprise.gouvernance, Validators.required],
      situationGeographique: [this.entreprise.situationGeographique, Validators.required],
      proprieteFonciere: [this.entreprise.proprieteFonciere, Validators.required],
      proprieteFondDeCommerce: [this.entreprise.proprieteFondDeCommerce],
      delegationServicePublic: [this.entreprise.delegationServicePublic],
    },{updateOn: 'submit'});
    const emailCampingControl = <FormControl>this.entrepriseForm.get('emailContact');
    emailCampingControl.valueChanges.subscribe(value=>{
      this.checkIfEmailExist(value, emailCampingControl);

    })
    this.initRibFormGroup();
    this.formReady = true;
  }

  getEmplacements(){
    this.entrepriseService.getCampingEmpl(this.entrepriseId,1)
      .subscribe(ribData=>{

        this.emplacementsLocatif = ribData;
        console.log(this.emplacementsLocatif);
      })
    this.entrepriseService.getCampingEmpl(this.entrepriseId,2)
      .subscribe(ribData=>{

        this.emplacementsLibre = ribData;
        console.log(this.emplacementsLocatif);
      })
  }

  displayEmplacement(emplacement: any) {
    console.log(emplacement);
    this.displayEmplacementEditModal = true;
    this.emplacementSelectEdit = emplacement;


  }

  displayRib() {
   this.displayRibEditModal = true;
   if(!this.rib){
     this.rib = {};
   }
  }

  onSubmitRib() {
    console.log(this.ribForm);
    if (this.ribForm.invalid) {
      this.messageService.add({severity: 'error', summary: 'Problème de saisie', detail: "Vérifiez votre saisie"});
      return;
    }
    this.rib.entrepriseId = this.entrepriseId;
    this.entrepriseService.updateEntrepriseRib(this.rib)
      .subscribe(data=>{
        this.rib = data;
        this.displayRibEditModal = false;
        this.messageService.add({severity: 'success', summary: 'Success', detail: 'Rib modifié avec succès'});
      })
  }
}
