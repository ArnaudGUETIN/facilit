import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EntrepriseEditComponent} from './entreprise-edit.component';
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {DialogService} from "primeng/dynamicdialog";
import {MessageService} from "primeng/api";

describe('EntrepriseEditComponent', () => {
  let component: EntrepriseEditComponent;
  let fixture: ComponentFixture<EntrepriseEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EntrepriseEditComponent],
      imports: [ReactiveFormsModule,FormsModule,HttpClientModule,RouterModule.forRoot([])],
      providers:[MessageService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntrepriseEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
