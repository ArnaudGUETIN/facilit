import {Component, OnInit} from '@angular/core';
import {LoginServiceService} from '../../Services/login-service.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {AdminserviceService} from '../../Services/adminservice.service';
import {EntrepriseserviceService} from '../../Services/entrepriseservice.service';
import {CollaborateurserviceService} from '../../Services/collaborateurservice.service';
import {Admin} from '../../Model/Admin';
import {Entreprise} from '../../Model/Entreprise';
import {Collaborateur} from '../../Model/Collaborateur';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MessageService} from "primeng/api";

const helper = new JwtHelperService();
const roleAdmin = 'ADMIN';
const roleCollaborateur = 'COLLABORATEUR';
const roleEntreprise = 'ENTREPRISE';
const roleCollabFacilit = 'COLLAB_FACILIT';
const roleConsultant = 'CONSULTANT';
const rolePartenaire = 'PARTENAIRE';
@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  styles: [`
    .align-radio {
      display: flex;
      align-items: flex-start;

    }

    .align-radio p-radiobutton {
      margin-right: 4px;
    }
    input:invalid {
      border-color: #E21776 !important;
    }
    .app-body{
      background-image: url('.../../assets/img/avatars/back.png');
      background-repeat: no-repeat;
      background-size: cover;
    }
  `

  ]
})
export class LoginComponent implements OnInit {
  user = {username: '', password: ''};
  password = '';
  username = '';
  displayBasic: boolean;
  public token = localStorage.getItem('token');
  public authority: string;
  badCredentials: Boolean = false;
  selectedrole: any = null;
  tokenPayload: any;
  roles: any[] = [];
  entrepriseList: any = [];
  loginform:FormGroup;

  selectedEnt: any;
  cities = [
    {name: 'New York', code: 'NY'},
    {name: 'Rome', code: 'RM'},
    {name: 'London', code: 'LDN'},
    {name: 'Istanbul', code: 'IST'},
    {name: 'Paris', code: 'PRS'}
  ];
  showLoader: boolean = false;
  constructor(private loginService: LoginServiceService,
              private adminService: AdminserviceService,
              private entrepriseService: EntrepriseserviceService,
              private collaborateurService: CollaborateurserviceService,
              private messageService: MessageService,
              public actvRoute: ActivatedRoute,
              private formBuilder:FormBuilder,
              public router: Router) {
    this.actvRoute.params.subscribe(data => {
      this.username = data.username;
    });
  }

  ngOnInit() {
    if(!this.loginService.isTokenExpired()){
      this.showLoader = true;
      const token = localStorage.getItem('token');
      this.tokenPayload = helper.decodeToken(token);
      if(this.tokenPayload.roles?.length == 1){
        this.authority = this.tokenPayload.roles[0].authority;
        this.forwardWithOnlyOneAuthority(this.tokenPayload);
      }

      this.showLoader = false;

    }
    this.loginform = this.formBuilder.group({
      username:['',[Validators.required,Validators.email]],
      password:['',Validators.required],
      remindMe:[true],
    })
  }


  onLogin() {
    if(this.loginform.invalid){
      return;
    }
    this.showLoader = true;
    this.user.username = this.loginform.get('username').value;
    this.user.password = this.loginform.get('password').value;
    console.log(this.user);
    if (this.user.username != '' && this.user.password != '') {

      this.loginService.attempAuthentification(this.user)
        .subscribe(data => {
            let jwt = data.headers.get('Authorization');
            console.log(data);
            this.loginService.saveToken(jwt);
            const token = localStorage.getItem('token');
            this.tokenPayload = helper.decodeToken(token);
            if (this.tokenPayload.roles.length <= 1) {
              this.authority = this.tokenPayload.roles[0].authority;
              // if (this.authority == "ADMIN")  localStorage.setItem('userType',this.authority);
              this.forwardWithOnlyOneAuthority(this.tokenPayload);
            } else if (this.tokenPayload.roles.length > 1) {
              let key = 1;
              this.tokenPayload.roles.forEach(r => {

                this.roles.push({name: r.authority, key: key});
                key++;
                if (key == this.tokenPayload.roles.length) {
                  this.displayBasic = true;
                }

                if (r.authority === 'COLLAB_FACILIT') {
                  this.entrepriseService.getManagerEntreprisesByUsername(this.tokenPayload.sub)
                    .subscribe(data => {
                      console.log(data);
                      this.entrepriseList = data._embedded.entreprises;
                      this.showLoader = false;
                    });
                }

              });
            }

          },
          err => {
            // this.spinner.hide();
            this.badCredentials = true;
            this.showLoader = false;
            console.log(err);
          });

    }
    else {
      this.badCredentials = true;
      this.showLoader = false;
    }

  }

  goToDashboard() {
    if (this.selectedrole.name === 'ADMIN') {
      this.adminLogin(this.tokenPayload);
    } else if (this.selectedrole.name === 'COLLAB_FACILIT') {
      this.entrepriseService.getOneEntreprise(this.selectedEnt.emailContact)
        .subscribe(data => {
          let entreprise: Entreprise = new Entreprise();
          entreprise = data.body;
          console.log(entreprise);
          localStorage.setItem('entreprise', JSON.stringify(entreprise));
          localStorage.setItem('entrepriseId', entreprise.entrepriseId.toString());
          localStorage.setItem('appRole', 'COLLAB_FACILIT');
          this.entrepriseService.CURRENT_ENTREPRISE.next(entreprise);
          this.goToEntrepriseHome(entreprise.entrepriseId);
          this.showLoader = false;
        });
    } else if (this.selectedrole.name === 'ENTREPRISE') {
      this.entrepriseService.getOneEntreprise(this.tokenPayload.sub)
        .subscribe(data => {
          let entreprise = data.body;
          if(entreprise){
            localStorage.setItem('entreprise', JSON.stringify(entreprise));
            localStorage.setItem('entrepriseId', entreprise.entrepriseId.toString());
            localStorage.setItem('appRole', 'ENTREPRISE');
            this.entrepriseService.CURRENT_ENTREPRISE.next(entreprise);
            this.goToEntrepriseHome(entreprise.entrepriseId);
          }else {
            this.messageService.add({severity: 'error', summary: 'Erreur', detail: 'Aucune entreprise n\' est lié à ce utilisateur '});
          }

        });
    }

  }

  private forwardWithOnlyOneAuthority(tokenPayload) {
    setTimeout(() => {
      if (this.authority == roleAdmin) {
        this.adminLogin(tokenPayload);

      } else if (this.authority == roleEntreprise) {
        this.entrepriseService.getOneEntreprise(tokenPayload.sub)
          .subscribe(data => {
            let entreprise: any;
            entreprise = data.body;
            console.log(entreprise);
            localStorage.setItem('entreprise', JSON.stringify(entreprise));
            localStorage.setItem('pictureUrl', JSON.stringify(entreprise.pictureUrl));
            localStorage.setItem('entrepriseId', entreprise.entrepriseId.toString());
            localStorage.setItem('appRole', roleEntreprise);
            this.entrepriseService.CURRENT_ENTREPRISE.next(entreprise);
            this.goToEntrepriseHome(entreprise.entrepriseId);
            this.showLoader = false;
          });
      } else if (this.authority == roleCollaborateur) {
        this.collaborateurService.getOneCollab(tokenPayload.sub)
          .subscribe(data => {
            let collaborateur: any;
            collaborateur = data.body;
            console.log(collaborateur);
            localStorage.setItem('collaborateur', JSON.stringify(collaborateur));
            localStorage.setItem('pictureUrl', JSON.stringify(collaborateur.pictureUrl));
            localStorage.setItem('collaborateurId', collaborateur.collaborateurId.toString());
            localStorage.setItem('appRole', roleCollaborateur);
            this.collaborateurService.CURRENT_COLLABORATEUR.next(collaborateur);
            // this.loginService.IS_LIVREUR.next(true);
            // this.loading.dismiss();
            this.showLoader = false;
            this.goToCollaborateurHome();
          });
      }
      else {
        this.loginService.getUserDetails(tokenPayload.sub)
          .subscribe(data => {

          });
      }

    }, 1000);
  }

  private adminLogin(tokenPayload) {
    this.adminService.getAdmin(tokenPayload.sub)
      .subscribe(data => {
        let admin: Admin = new Admin();
        admin = data.body;
        localStorage.setItem('admin', JSON.stringify(admin));
        localStorage.setItem('adminId', admin.id.toString());
        localStorage.setItem('appRole', roleAdmin);
        // this.clientService.CURRENT_CLIENT.next(client);
        // this.serviceProvider.completeAddrCurrentUser(client.adresse.nom)
        // this.loginService.IS_ADMIN.next(true);
        // this.loading.dismiss();
        this.showLoader = false;
        this.goToAdminHome();

      });
  }

  private goToAdminHome() {
    this.router.navigate(['admin']);
  }

  private goToEntrepriseHome(entrepriseId: number) {
    this.router.navigate(['entreprise/' + entrepriseId]);
  }

  private goToCollaborateurHome() {
    console.log('ici');
    this.router.navigate(['collaborateur']);
  }
}
