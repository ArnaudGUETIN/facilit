import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Task} from "../../../Model/Task";
import {TaskService} from "../../../Services/task.service";
import {ConfirmationService, MessageService, PrimeNGConfig} from "primeng/api";
import {TypeTache} from "../task-edit/task-edit.component";
import {CollaborateurserviceService} from "../../../Services/collaborateurservice.service";

@Component({
  selector: 'app-task-to-do',
  templateUrl: './task-to-do.component.html',
  styleUrls: ['./task-to-do.component.scss']
})
export class TaskToDoComponent implements OnInit {
  taskId:any;
  task: any;
  etapeList: any = [];
  relevesList: any = [];
  collaborateurList: any = [];
  releveDialog: boolean;

  releve: any;

  selectedReleves: any;

  submitted: boolean;
  acteurs: any;
  constructor( private messageService: MessageService, private activateRoute:ActivatedRoute
               ,private taskService:TaskService,private primengConfig: PrimeNGConfig
    , private confirmationService: ConfirmationService,private collaborateurService:CollaborateurserviceService) {
    this.activateRoute.params.subscribe(data=>{
      this.taskId = data.taskId;
      console.log(this.taskId);
    })
  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.getTask(this.taskId);
    this.getCollaborateur();

  }
getCollaborateur(){
  let collaborateur = CollaborateurserviceService.getCollaborateur();
  console.log(collaborateur);

  this.collaborateurList.push(collaborateur);
  console.log(this.collaborateurList);
}
  getTask(taskId:number){
    this.taskService.getOneTask(taskId)
      .subscribe(data=>{
        this.task = data;
        if(TypeTache.CheckList){
          this.getTaskEtapes(this.task.taskId);
        }else if(TypeTache.RELEVE){
          this.getReleves();
        }

        console.log(this.task)
      })
  }
  getTaskEtapes(taskId:number){
    this.taskService.getAllTaskEtapes(taskId)
      .subscribe(data=>{
        this.etapeList = data;
        console.log(this.etapeList)
      })
  }

  updateSteps(){
    let stepsUpdate: any = []
    for(let step of this.etapeList){
      let newVar = {stepId:step.etapeId,value:step.checked};
      stepsUpdate.push(JSON.stringify(newVar));
    }
    this.taskService.updateSteps(stepsUpdate)
      .subscribe(data=>{
        console.log(data);
      })
  }
  getReleves(): any {
    this.taskService.getAllTaskReleves(this.taskId)
      .subscribe(data => {
        console.log(data);
        this.relevesList = data;
        return this.relevesList;
      }, error => {

      });

  }
  openNew() {
    this.releve = {};
    this.releve.releveId=0;
    this.submitted = false;
    this.releveDialog = true;
  }

  deleteSelectedReleves() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected products?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.relevesList = this.relevesList.filter(val => !this.selectedReleves.includes(val));
        this.selectedReleves = null;
        this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
      }
    });
  }

  editProduct(product: any) {
    console.log(product)
    this.releve = {...product};
    this.releveDialog = true;
  }

  deleteReleve(releve: any) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete releve?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.relevesList = this.relevesList.filter(val => val.id !== releve.releveId);
        this.taskService.deleteReleve(this.releve.releveId)
          .subscribe(data=>{
            this.releve = {};
            this.messageService.add({severity:'success', summary: 'Successful', detail: 'Releve Deleted', life: 3000});
          })

      }
    });
  }

  hideDialog() {
    this.releveDialog = false;
    this.submitted = false;
  }

  saveReleve() {
    this.submitted = true;

    console.log(this.releve)
    if (this.releve && this.releve.releveId!=0) {
      this.taskService.updateReleve(this.releve)
        .subscribe(data=>{
          this.releve = data;
          this.getReleves();
          this.messageService.add({severity:'success', summary: 'Successful', detail: 'Releve Updated', life: 3000});
        })
    }else {
      this.taskService.addReleve(this.taskId,this.releve)
        .subscribe(data=>{
          this.releve = data;
          this.relevesList.push(this.releve);
          this.relevesList = [...this.relevesList];
          this.messageService.add({severity:'success', summary: 'Successful', detail: 'Releve Created', life: 3000});
        })
    }



    this.releveDialog = false;
    this.releve = {};
    //}
  }

  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.relevesList.length; i++) {
      if (this.relevesList[i].releveId === id) {
        index = i;
        break;
      }
    }

    return index;
  }


}
