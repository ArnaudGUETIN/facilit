import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskToDoComponent } from './task-to-do.component';
import {ConfirmationService, MessageService} from "primeng/api";
import {ActivatedRoute, RouterModule} from "@angular/router";
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {DialogService} from "primeng/dynamicdialog";

describe('TaskToDoComponent', () => {
  let component: TaskToDoComponent;
  let fixture: ComponentFixture<TaskToDoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskToDoComponent ],
      imports: [ReactiveFormsModule,FormsModule,HttpClientModule,RouterModule.forRoot([])],

      providers:[MessageService,ConfirmationService]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskToDoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
