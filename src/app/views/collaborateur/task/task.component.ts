import {Component, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {Router} from '@angular/router';
import {TaskService} from "../../../Services/task.service";
import {CollaborateurserviceService} from "../../../Services/collaborateurservice.service";
import {Task} from "../../../Model/Task";
import {MessageService} from "primeng/api";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  stackedData: any;
  value: number = 0;
  stackedOptions: any;
  todo = [
    'Get to work',
    'Pick up groceries',
    'Go home',
    'Fall asleep'
  ];
  done = [
    'Get up',
    'Brush teeth',
    'Take a shower',
    'Check e-mail',
    'Walk dog'
  ];
  taskList: any;
  selectedTasks: any;

  constructor(public router: Router,private taskService:TaskService,private collaborateurService:CollaborateurserviceService,private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.getTasks();

    this.getTypeReleveChartData();
    this.loadStatus();
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

  viewTask(task: Task) {
    this.router.navigate(['collaborateur/taskToDo',{taskId:task.taskId}]);
  }

  EditTask(task: any) {
    this.router.navigate(['collaborateur/taskEdit']);
  }

  getTasks(){
    this.taskService.getAllCollaborateurTasks(CollaborateurserviceService.getCollaborateurId())
      .subscribe(data=>{
        this.taskList = data;
        console.log(data);
      })
  }

  loadCharts(data){
    this.stackedData = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [{
        type: 'bar',
        label: data[0].label,
        backgroundColor: '#42A5F5',
        data: data[0].data
      }, {
        type: 'bar',
        label: data[1].label,
        backgroundColor: '#66BB6A',
        data: data[1].data
      }, {
        type: 'bar',
        label: data[2].label,
        backgroundColor: '#FFA726',
        data: data[2].data
      }]
    };

    this.stackedOptions = {
      tooltips: {
        mode: 'index',
        intersect: false
      },
      responsive: true,
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true
        }]
      }
    };
  }
  getTypeReleveChartData(){
    this.taskService.getTypeReleveChartData(CollaborateurserviceService.getCollaborateurId())
      .subscribe(data=>{
        console.log(data);
        if(data){
          this.loadCharts(data);
        }

      })
  }

   loadStatus() {
     let interval = setInterval(() => {
       this.value = this.value + Math.floor(Math.random() * 10) + 1;
       if (this.value >= 100) {
         this.value = 100;
         this.messageService.add({severity: 'info', summary: 'Success', detail: 'Process Completed'});
         clearInterval(interval);
       }
     }, 2000);
  }
}
