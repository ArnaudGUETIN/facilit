import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CollaborateurdashboardComponent} from './collaborateurdashboard.component';
import {ActivatedRoute, RouterModule} from "@angular/router";

describe('CollaborateurdashboardComponent', () => {
  let component: CollaborateurdashboardComponent;
  let fixture: ComponentFixture<CollaborateurdashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CollaborateurdashboardComponent],
      imports:[RouterModule.forRoot([])]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborateurdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
