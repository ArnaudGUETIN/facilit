import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Entreprise} from '../../../Model/Entreprise';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {Task} from '../../../Model/Task';
import {TaskService} from '../../../Services/task.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CollaborateurserviceService} from '../../../Services/collaborateurservice.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE, ThemePalette} from '@angular/material/core';

// tslint:disable-next-line:no-duplicate-imports
import * as _moment from 'moment';
import {defaultFormat as _rollupMoment} from 'moment';
import {AffectationCollaborateurTaskModalComponent} from '../../../shared/dialogModal/affectation-collaborateur-task-modal/affectation-collaborateur-task-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {DialogService, DynamicDialogRef} from 'primeng/dynamicdialog';
import {ConfirmationService, MenuItem, MessageService} from 'primeng/api';
import {Etape} from '../../../Model/Etape';
import {Compteur} from '../../../Model/Compteur';
import {DepartementServiceService} from "../../../Services/departement-service.service";
import {Service} from "../../../Model/Service";
import {DatePipe} from "@angular/common";
import {InstallationService} from "../../../Services/installation.service";



// See the Moment.js docs for the meaning of these formats:
// https://momentjs.com/docs/#/displaying/format/

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.scss'],
  providers: [

  ],
})
export class TaskEditComponent implements OnInit, AfterViewInit, OnDestroy {

  public disabled = false;
  public showSpinners = true;
  public showSeconds = false;
  public touchUi = false;
  public enableMeridian = false;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;
  public color: ThemePalette = 'primary';
  stackedData: any;
  stackedOptions: any;
  public static nbreEtapes = -1;
  defaultTime  = new Date();
  dateDebut = new FormControl();
  dateFin = new FormControl();
  collaborateurList: any = [];
  etapeList: any = [];
  relevesList: any = [];
  responsableList: any = [];
  etape: Etape = new Etape();
  newCheck: Etape = new Etape();
  displayedColumns: string[] = ['nom', 'prenom', 'email', 'fonction', 'profil', 'star'];
  dataSource: MatTableDataSource<Entreprise> = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  task: Task = new Task();
  taskId: number = 0;
  entrepriseId = 0;
  ref: DynamicDialogRef;
  public polarAreaChartData: number[] = [70, 50, 30, 80, 45];
  public polarAreaChartLabels: string[] = ['Etape 1', 'Etape 2', 'Etape 3', 'Etape 4', 'Etape 5'];
  public polarAreaLegend = true;
  public polarAreaChartType = 'polarArea';
  steps: MenuItem[];
  activeIndex: number = 0;
  etapeListReady: boolean = false;
  taskForm: any;
  compteurForm: any;
  payLoad = '';
  displayBasic: boolean;
  showLoader: boolean;
  displayReleves: boolean;
  showCompteur: any;
  disableType: boolean;
  typeReleveSelection: any;
  typeAnalyseSelection: any;
  showCheckList: any;
  showSteps: any;
  displayNewCheck: boolean = false;
  isUpdate: boolean = false;
  serviceList: Service[]=[];
  releveDialog: boolean;
  isToSave = false;
  products: any;
  annuelData:any = [];
  releve: any;
  items: MenuItem[];
  itemsCompteur: MenuItem[];
  itemstask: MenuItem[];
  selectedReleves: any;

  submitted: boolean;
  acteurs: any;
  displaySaisieDialog = false;
  saisieForm: FormGroup;
  saisieCurrentDate = new Date();
  saisieList:any;
  compteurList: any;
  taskCompteurList: any;
  constructor(public dialogService: DialogService, public dialog: MatDialog, private taskService: TaskService, private activateRoute: ActivatedRoute
              , private collaborateurService: CollaborateurserviceService, private router: Router, private fb: FormBuilder
              , private messageService: MessageService, private  departmentService:DepartementServiceService
              , private confirmationService: ConfirmationService,private installationService:InstallationService) {
    this.activateRoute.params.subscribe(data => {
      console.log(data.task);
      this.taskId = data.taskId;
      this.entrepriseId = data.entrepriseId;
    });

    this.defaultTime.setTime(10);
    this.defaultTime.setMinutes(0);
  }

  ngAfterViewInit() {

    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnDestroy(): void {

  }
  getAnnualCompteurReports(){
    this.taskService.getAnnualCompteurReport(this.taskId)
      .subscribe(data=>{
        this.annuelData = data;
        this.loadCharts();
      })
  }

  ngOnInit(): void {
    this.taskForm = this.fb.group({
      numero: [this.task.numero, Validators.required],
      designation: [this.task.designation, Validators.required],
      commentaire: [this.task.commentaire],
      instruction: [this.task.instruction],
      dateDebut: [this.task.dateDebut, Validators.required],
      dateFin: [this.task.dateFin, Validators.required],
      responsable: [this.task.responsableId, Validators.required],
      rappelTime: [this.task.rappelTime, Validators.required],
      nature: [this.task.nature, Validators.required],
      categorie: [this.task.categorie, Validators.required],
      periodicite: [this.task.periodicite, Validators.required],
      type: [this.task.typeTache, Validators.required],
      statut: [this.task.statut],
      serviceId: [this.task.serviceId,Validators.required],
    });
    this.taskForm.valueChanges.subscribe(data=>{
     if(data){
       if(!this.taskForm.pristine){
         this.isToSave = true;
       }
     }

    })
    this.compteurForm = this.fb.group({
      numero: [this.task.compteurMV?.numero, Validators.required],
      valeur: [this.task.compteurMV?.valeur, Validators.required],
      fournisseur: [this.task.compteurMV?.fournisseur, Validators.required],
    });
    this.getTask(this.taskId);
    this.getDepartements(this.entrepriseId);
    this.getTaskCollaborateur(this.taskId);
    this.getTaskEtapes(this.taskId);
    this.getRespobsables();
    this.getAllSaisie();
    this.getReleves();
    this.getAllEntrepriseAndTaskCompteurs();
    this.items = [
      {
        label: 'Actions',
        items: [{
          label: 'Ajouter',
          icon: 'pi pi-plus',
          command: () => {
            this.displaySaisieDialog = true;
          }
        }
        ]},
    ];
    this.itemsCompteur = [
      {
        label: 'Actions',
        items: [{
          label: 'Ajouter',
          disabled:this.task.compteurMV?false:true,
          icon: 'pi pi-plus',
          command: () => {
            if(this.task.compteurMV){
              this.messageService.add({severity: 'info', summary: 'Action imposible', detail: 'Un compteur est déjà affecté à cette tâche '});
            }else {
              this.affectCompteur();
            }

          }
        },
          {
          label: 'Voir relevés',
          icon: 'pi pi-eye',
          command: () => {

             this.showReleves();


          }
        }
        ,{
          label: 'Suprimer',
          icon: 'pi pi-exclamation-triangle',
          command: () => {

             // this.del();


          }
        }
        ]},
    ];
    this.itemstask = [
      {
        label: 'Actions',
        items: [{
          label: 'Lancer la tâche',
          icon: 'pi pi-plus',
          command: () => {
            this.affectCompteur();
          }
        }
        ]},
    ];
    this.initSaisieForm();
    this.getAnnualCompteurReports();
  }

   initSaisieForm() {
    this.saisieForm = this.fb.group({
      libelle: ['', [Validators.required]],
      startTime: ['', [Validators.required]],
      endTime: ['', [Validators.required]],
      collaborateurId: ['', [Validators.required]],
    })
  }

  getTask(taskId: number) {
    if (taskId != 0) {
      this.taskService.getOneTask(taskId)
        .subscribe(data => {
          if(data){
            this.task = data;
            let datePipe = new DatePipe('fr');
            this.taskForm.get('dateDebut').setValue(new Date(this.task.dateDebut));
            this.taskForm.get('dateFin').setValue(new Date(this.task.dateFin));
            this.displayPanels();
          }

        }, error => {

        });
    }
  }

  getTaskCollaborateur(taskId: number) {
    if (taskId != 0) {
      this.collaborateurService.getAllTaskCollaborateurs(taskId)
        .subscribe(data => {
          if(data){
            this.collaborateurList = data._embedded.collaborateurs;
            this.dataSource = new MatTableDataSource(this.collaborateurList);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
          }
        }, error => {

        });
    }
  }

  getTaskEtapes(taskId: number): any{
    if (taskId != 0) {
      this.taskService.getAllTaskEtapes(taskId)
        .subscribe(data => {
          if(data){
            this.etapeList = data;
            if (this.etapeList) {
              this.etape = this.etapeList[0];
            }
            let array = [];
            this.etapeList.forEach(e => {
              array.push({
                label: 'Etape ' + e.ordre,
                command: (event: any) => {
                  this.activeIndex = (e.ordre - 1);
                  this.etape = e;
                }
              });

            });

            this.steps = [...array];
            this.etapeListReady = true;
            // this.etape.designation = "Etape "+(this.items.length+1);
            TaskEditComponent.nbreEtapes = this.steps.length - 1;
          }

          return this.etapeList;
        }, error => {

        });
    }
  }

  affectCollabToTaskDialog() {

    this.ref = this.dialogService.open(AffectationCollaborateurTaskModalComponent, {
      data: {
        entrepriseId: this.entrepriseId,
        taskId: this.taskId
      },
      header: 'Selection de collaborateur',
      width: '73%',
      contentStyle: {'max-height': '500px', 'overflow': 'auto'},
      baseZIndex: 10000
    });

    this.ref.onClose.subscribe((result: any) => {

      if(result.isModify){
        this.getTaskCollaborateur(this.taskId);
        this.messageService.add({severity: 'success', summary: 'Success', detail: 'Affectation effectué avec succès '});
      }

    });
  }

  public chartClicked(e: any): void {
    //  console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }

  addStep() {
    let etape: Etape = new Etape();
    etape.ordre = (this.etapeList.length + 1);
    etape.designation = 'Etape ' + (this.etapeList.length + 1);
    this.taskService.addEtape(this.taskId, etape)
      .subscribe(data => {
        if(data){
          let step: any = data;
          this.etapeList.push(step);
          this.etapeList = [...this.etapeList];
          this.steps.push({
            label: step.designation,
            command: (event: any) => {
              this.etape = step;
              this.activeIndex = step.ordre;

            }
          });
          this.steps = [...this.steps];
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Etape ajoutée avec succès '});
        }

      }, error => {
        this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur s\'est produite lors de cette oprération'});
      });

  }

  addCheck(isUpdate: boolean, etape?: Etape) {
    this.displayNewCheck = true;
    this.isUpdate = isUpdate;
    if (isUpdate) {
      this.newCheck = new Etape();
      this.newCheck.ordre = etape.ordre;
      this.newCheck.designation = etape.designation;
      this.newCheck.description = etape.description;
      this.newCheck.mandatory = etape.mandatory;
      this.newCheck.checked = etape.checked;
      this.newCheck.statut = etape.statut;
      this.newCheck.etapeId = etape.etapeId;
    } else {
      this.newCheck = new Etape();
      this.newCheck.ordre = (this.etapeList.length + 1);
      this.newCheck.designation = 'Check ' + (this.etapeList.length + 1);
    }

  }

  updateCheck(check: Etape, updateAll: boolean) {
    this.taskService.updateEtape(check)
      .subscribe(data => {
        if(data){
          if (updateAll) {
            this.etapeList = [];
          }
          this.getTaskEtapes(this.taskId);
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Modification effectué'});
          this.displayNewCheck = false;
        }

      }, error => {
        this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur s\'est produite lors de cette oprération'});
        this.displayNewCheck = false;
      });
  }

  saveCheck(etape: Etape) {

    this.taskService.addEtape(this.taskId, etape)
      .subscribe(data => {
        if(data){
          let step: any = data;
          this.etapeList.push(step);
          this.etapeList = [...this.etapeList];
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Check ajoutée avec succès '});
          this.displayNewCheck = false;
        }
      }, error => {
        this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur s\'est produite lors de cette oprération'});
        this.displayNewCheck = false;
      });
  }

  removeLastStep() {
    let etapeId = 0;
    let position = this.etapeList.length - 1;
    console.log(position);
    if (position >= 0) {
      let etape = this.etapeList[(position)];
      this.taskService.deleteEtape(etape.etapeId)
        .subscribe(data => {
          if(data){
            const elementsIndex = this.etapeList.findIndex(element => element.etapeId == etape.etapeId);
            this.messageService.add({severity: 'success', summary: 'Success', detail: 'Etape supprimée avec succès '});
            this.activeIndex = 0;
            this.steps = [];
            this.getTaskEtapes(this.taskId);
          }

        }, error => {
          this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur s\'est produite lors de cette oprération'});
        });
    }

  }

  removeLastCheck() {
    let etapeId = 0;
    let position = this.etapeList.length - 1;

    if (position >= 0) {
      let etape = this.etapeList[(position)];
      this.taskService.deleteEtape(etape.etapeId)
        .subscribe(data => {
          if(data){
            const elementsIndex = this.etapeList.findIndex(element => element.etapeId == etape.etapeId);
            this.messageService.add({severity: 'success', summary: 'Success', detail: 'Etape supprimée avec succès '});
            this.activeIndex = 0;
            this.etapeList = [];
            this.getTaskEtapes(this.taskId);
          }

        }, error => {
          this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur s\'est produite lors de cette oprération'});
        });
    }

  }

  viewTask(task: any) {
    this.router.navigate(['entreprise/' + this.entrepriseId + '/taskView', {
      taskId: task.taskId,
      entrepriseId: this.entrepriseId
    }]);
    //this.router.navigate(['collaborateur/taskView', task]);
  }

  updateStep(step: any) {
    this.etapeListReady = false;
    this.taskService.updateEtape(step)
      .subscribe(data => {
        if(data){
          let etape: any = data;
          this.activeIndex = 0;
          this.steps = [];
          this.getTaskEtapes(this.taskId);
        }

      }, error => {
        this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur s\'est produite lors de cette oprération'});
      });
  }

  getRespobsables() {
    this.collaborateurService.getAllResponsable(0, this.entrepriseId)
      .subscribe(data => {
        if(data){
          this.responsableList = data._embedded.collaborateurs;
        }

      });
  }

  onSubmit() {
    if ( this.taskForm.invalid) {
      this.messageService.add({severity: 'error', summary: 'Problème de saisie', detail: "Vérifiez votre saisie"});
      return;
    }
    this.showLoader =true;
    this.payLoad = JSON.stringify(this.taskForm.getRawValue());
    this.task.dateDebut = this.taskForm.get('dateDebut').value;
    this.task.dateFin = this.taskForm.get('dateFin').value;
    if (this.task.typeTache == 1) {
      this.taskService.updateTask(this.task, true)
        .subscribe(data => {
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Modification effectué avec succès '});
          this.isToSave = false;
          this.showLoader =false;
        }, error => {
          this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur s\'est produite lors de cette oprération'});
          this.showLoader =false;
        });
    } else {
      this.taskService.updateTask(this.task, false)
        .subscribe(data => {
          this.messageService.add({severity: 'success', summary: 'Success', detail: 'Modification effectué avec succès '});
          this.isToSave = false;
          this.showLoader =false;
        }, error => {
          this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur s\'est produite lors de cette oprération'});
          this.showLoader =false;
        });

    }


  }

  affectCompteur() {


    this.displayBasic = true;
    this.getEntrepriseCompteurs();

  }

  displayPanels() {

    let typeTache = this.task.typeTache;
    if (typeTache == TypeTache.RELEVE) {
      this.showCompteur = this.task.compteurMV != null;
      this.disableType = this.hasReleve(this.task);
    } else if (typeTache == TypeTache.ANALYSE) {
      this.typeAnalyseSelection = true;
    } else if (typeTache == TypeTache.CheckList) {
      this.showCheckList = true;
      this.disableType = this.hasStep(this.task);
    } else if (typeTache == TypeTache.ETAPE) {
      this.showSteps = true;
      this.disableType = this.hasStep(this.task);
    }


  }

  showReleves() {
    this.getReleves();
    this.displayReleves = true;
  }

  selectCompteur(compteurId: number) {
    this.installationService.addCompteurToTask(compteurId,this.taskId)
      .subscribe(data=>{
        this.displayBasic = false;
        this.getAllEntrepriseAndTaskCompteurs();
      })

  }

  hasReleve(task: Task): boolean {
    let releves = this.getReleves();
    return (releves && releves.length > 0) || (releves);

  }
  hasStep(task: Task): boolean {
    let steps = this.etapeList;
    return (steps && steps.length > 0) || (steps);

  }
  hideTypeSelectionDialog() {
    this.typeAnalyseSelection = false;
    this.typeReleveSelection = false;
  }

   getReleves(): any {
    this.taskService.getAllTaskReleves(this.taskId)
      .subscribe(data => {
        if(data){
          this.relevesList = data;
        }
        return this.relevesList;
      }, error => {

      });

  }
  getDepartements(entrepriseId:number){
    this.departmentService.getAllEntrepriseServices(entrepriseId)
      .subscribe(data => {

        if (data) {
          this.serviceList = data._embedded.services;
        }

      });
  }


  openNew() {
    this.releve = {};
    this.releve.id = 0;
    this.submitted = false;
    this.releveDialog = true;
  }

  deleteSelectedReleves() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected products?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.relevesList = this.relevesList.filter(val => !this.selectedReleves.includes(val));
        this.selectedReleves = null;
        this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
      }
    });
  }

  editProduct(product: any) {
    console.log(product)
    this.releve = {...product};
    this.releve.dateReleve = new Date(product.dateReleve);
    this.releveDialog = true;
  }

  deleteReleve(releve: any) {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete releve?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.relevesList = this.relevesList.filter(val => val.id !== releve.releveId);
        this.taskService.deleteReleve(this.releve.releveId)
          .subscribe(data=>{
            this.releve = {};
            this.messageService.add({severity:'success', summary: 'Successful', detail: 'Releve Deleted', life: 3000});
          })

      }
    });
  }

  hideDialog() {
    this.releveDialog = false;
    this.submitted = false;
  }

  saveReleve() {
    this.submitted = true;
    console.log(this.releve);
    if (this.releve && this.releve.id!=0) {
      this.taskService.updateReleve(this.releve)
        .subscribe(data=>{
          this.releve = data;
          this.getReleves();
          this.getAnnualCompteurReports();
          this.messageService.add({severity:'success', summary: 'Successful', detail: 'Releve Updated', life: 3000});
        })
    }else {
      this.taskService.addReleve(this.taskId,this.releve)
        .subscribe(data=>{
          this.releve = data;
          this.getAnnualCompteurReports();
          this.relevesList.push(this.releve);
          this.relevesList = [...this.relevesList];
          this.messageService.add({severity:'success', summary: 'Successful', detail: 'Releve Created', life: 3000});
        })
    }
      this.releveDialog = false;
      this.displayReleves = false;
      this.releve = {};
    //}
  }

  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.relevesList.length; i++) {
      if (this.relevesList[i].releveId === id) {
        index = i;
        break;
      }
    }

    return index;
  }

  addSaisie() {
    let saisie = {
      libelle:this.saisieForm.get('libelle')?.value,
      date:this.saisieCurrentDate,
      startTime:this.saisieForm.get('startTime')?.value,
      endTime:this.saisieForm.get('endTime')?.value,
      collaborateurId:this.saisieForm.get('collaborateurId')?.value,
      taskId:this.taskId,
    }
    console.log(this.saisieForm);
    console.log(saisie);
   this.taskService.addSaisie(saisie)
     .subscribe(data=>{
       this.messageService.add({severity:'success',summary:'Saisie temps',detail:'Saisie effectuée'});
       this.initSaisieForm();
       this.getAllSaisie();
       this.displaySaisieDialog = false;
     })
  }

  getAllSaisie(){
     this.taskService.getByLocalDateAndTaskTaskId(this.taskId,this.saisieCurrentDate)
       .subscribe(data=>{
         this.saisieList =data;
       })
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  getEntrepriseCompteurs(){
    this.installationService.getAllEntrepriseCompteur(this.entrepriseId)
      .subscribe(data=>{
        console.log(data);
        this.compteurList = data;
      })
  }
  getAllEntrepriseAndTaskCompteurs(){
    this.installationService.getAllEntrepriseAndTaskCompteurs(this.entrepriseId,this.taskId)
      .subscribe(data=>{
        console.log(data);
        this.taskCompteurList = data;
      })
  }

  loadCharts(){
    let nYear = this.annuelData.map(a => a.nyearValue);
    let n_1Year = this.annuelData.map(a => a.n_1yearValue);
    console.log(nYear)

    this.stackedData = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December'],
      datasets: [{
        type: 'bar',
        label: 'N',
        backgroundColor: '#FFA726',
        data: nYear
      }, {
        type: 'bar',
        label: 'N-1',
        backgroundColor: '#66BB6A',
        data: n_1Year
      }]
    };

    this.stackedOptions = {
      tooltips: {
        mode: 'index',
        intersect: false
      },
      responsive: true,
      scales: {
        xAxes: [{
          stacked: true,
        }],
        yAxes: [{
          stacked: true
        }]
      }
    };
  }
}
export enum TypeTache {
  ETAPE = 0,
  RELEVE = 1,
  CheckList = 2,
  RATIO_COMMERCE = 3,
  ANALYSE = 4,
}
