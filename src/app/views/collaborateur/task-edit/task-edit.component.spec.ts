import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TaskEditComponent} from './task-edit.component';
import {DialogService} from "primeng/dynamicdialog";
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {DialogModule} from "primeng/dialog";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {ConfirmationService, MessageService} from 'primeng/api';

describe('TaskEditComponent', () => {
  let component: TaskEditComponent;
  let fixture: ComponentFixture<TaskEditComponent>;

  beforeEach(async () => {
    // await TestBed.configureTestingModule({
    //   declarations: [TaskEditComponent],
    //   imports: [ReactiveFormsModule,FormsModule,HttpClientModule,RouterModule.forRoot([]),MatDialogModule],
    //   providers:[DialogService,{
    //     provide: MatDialogRef,
    //     useValue: {}
    //   },{
    //     provide: MAT_DIALOG_DATA,
    //     useValue: {}
    //   },MessageService,ConfirmationService]
    // })
    //   .compileComponents();
  });

  beforeEach(() => {
    // fixture = TestBed.createComponent(TaskEditComponent);
    // component = fixture.componentInstance;
    // fixture.detectChanges();
  });

  it('should create', () => {
   // expect(component).toBeTruthy();
  });
});
