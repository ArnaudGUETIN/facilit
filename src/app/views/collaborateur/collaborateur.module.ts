import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {CollaborateurRoutingModule} from './collaborateur-routing.module';
import {CollaborateurdashboardComponent} from './collaborateurdashboard/collaborateurdashboard.component';
import {ChartsModule} from 'ng2-charts';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {FormsModule} from '@angular/forms';
import {ButtonsModule} from 'ngx-bootstrap/buttons';
import {TaskComponent} from './task/task.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {ProgressBarModule} from 'primeng/progressbar';
import {MatSelectModule} from '@angular/material/select';
import {StepsModule} from 'primeng/steps';
import {InputSwitchModule} from 'primeng/inputswitch';
import {DividerModule} from 'primeng/divider';
import {CalendarModule} from 'primeng/calendar';
import {ChipsModule} from 'primeng/chips';
import {DropdownModule} from 'primeng/dropdown';
import {InputMaskModule} from 'primeng/inputmask';
import {InputNumberModule} from 'primeng/inputnumber';
import {CascadeSelectModule} from 'primeng/cascadeselect';
import {InputTextModule} from 'primeng/inputtext';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToolbarModule} from "primeng/toolbar";
import {TableModule} from "primeng/table";
import {BadgeModule} from "primeng/badge";
import { TaskToDoComponent } from './task-to-do/task-to-do.component';
import {AvatarModule} from "primeng/avatar";
import {FieldsetModule} from "primeng/fieldset";
import {CheckboxModule} from "primeng/checkbox";
import {ToastModule} from "primeng/toast";
import {DialogModule} from "primeng/dialog";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {FileUploadModule} from "primeng/fileupload";
import {ChartModule} from "primeng/chart";
import {DateAdapter, MatNativeDateModule} from "@angular/material/core";
import {DateFormat} from "../../shared/shared/DateFormat";
import {NgxMatDatetimePickerModule, NgxMatTimepickerModule} from "@angular-material-components/datetime-picker";
import { AlerteComponent } from './alerte/alerte.component';
import {CardModule} from "primeng/card";
import {FlexModule} from "@angular/flex-layout";
import { AlerteDetailComponent } from './alerte-detail/alerte-detail.component';


@NgModule({
  declarations: [
    CollaborateurdashboardComponent,
    TaskComponent,
    TaskToDoComponent,
    AlerteComponent,
    AlerteDetailComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    CollaborateurRoutingModule,
    ChartsModule,
    DragDropModule,
    MatDialogModule,
    MatTabsModule,
    MatDatepickerModule,
    MatTableModule,
    MatFormFieldModule,
    MatSelectModule,
    StepsModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    ProgressBarModule,
    BsDropdownModule,
    InputSwitchModule,
    CalendarModule,
    ChipsModule,
    DropdownModule,
    InputMaskModule,
    InputNumberModule,
    CascadeSelectModule,
    InputTextModule,
    DividerModule,
    ButtonsModule.forRoot(),
    ToolbarModule,
    TableModule,
    BadgeModule,
    AvatarModule,
    FieldsetModule,
    CheckboxModule,
    ToastModule,
    DialogModule,
    ConfirmDialogModule,
    MatNativeDateModule,
    FileUploadModule,
    ChartModule,
    CardModule,
    FlexModule
  ],
  providers: [{provide: DateAdapter, useClass: DateFormat},]
})
export class CollaborateurModule {
  constructor(private dateAdapter: DateAdapter<Date>) {
    dateAdapter.setLocale("en-in"); // DD/MM/YYYY
  }
}
