import {ComponentFixture, TestBed} from '@angular/core/testing';

import {TaskViewComponent} from './task-view.component';
import {HttpClientModule} from "@angular/common/http";
import { RouterModule } from '@angular/router';

describe('TaskViewComponent', () => {
  let component: TaskViewComponent;
  let fixture: ComponentFixture<TaskViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TaskViewComponent],
      imports: [HttpClientModule,RouterModule.forRoot([])]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
