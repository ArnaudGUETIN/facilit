import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {TaskService} from '../../../Services/task.service';
import {Task} from '../../../Model/Task';
import {ActivatedRoute, Router} from '@angular/router';
import {MatTableDataSource} from '@angular/material/table';
import {Entreprise} from '../../../Model/Entreprise';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {CollaborateurserviceService} from '../../../Services/collaborateurservice.service';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.scss']
})
export class TaskViewComponent implements OnInit, AfterViewInit {
  collaborateurList: any = [];
  displayedColumns: string[] = ['nom', 'prenom', 'email', 'fonction', 'profil', 'star'];
  dataSource: MatTableDataSource<Entreprise> = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  task: Task = new Task();
  taskId: number = 0;
  entrepriseId: number = 0;
  public polarAreaChartData: number[] = [70, 50, 30, 80, 45];
  public polarAreaChartLabels: string[] = ['Etape 1', 'Etape 2', 'Etape 3', 'Etape 4', 'Etape 5'];
  public polarAreaLegend = true;
  public polarAreaChartType = 'polarArea';

  constructor(private taskService: TaskService, private activateRoute: ActivatedRoute, private collaborateurService: CollaborateurserviceService, private router: Router) {
    this.activateRoute.params.subscribe(data => {
      this.taskId = data.taskId;
      this.entrepriseId = data.entrepriseId;
    });


  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.getTask(this.taskId);
    this.getTaskCollaborateur(this.taskId);
  }

  getTask(taskId: number) {
    this.taskService.getOneTask(taskId)
      .subscribe(data => {
        this.task = data;
      });
  }

  getTaskCollaborateur(taskId: number) {
    this.collaborateurService.getAllTaskCollaborateurs(taskId)
      .subscribe(data => {
        this.collaborateurList = data._embedded.collaborateurs;
        this.dataSource = new MatTableDataSource(this.collaborateurList);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });
  }

  editTask(task: any) {
    this.router.navigate(['entreprise/' + this.entrepriseId + '/taskEdit', {
      taskId: task.taskId,
      entrepriseId: this.entrepriseId,
      task: task
    }]);
  }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }


}
