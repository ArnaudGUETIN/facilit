import { Component, OnInit } from '@angular/core';
import {AlerteService} from "../../../Services/alerte.service";

@Component({
  selector: 'app-alerte',
  templateUrl: './alerte.component.html',
  styleUrls: ['./alerte.component.scss']
})
export class AlerteComponent implements OnInit {
  alerteList=[];
  constructor(private alerteService:AlerteService) { }

  ngOnInit(): void {
    this.alerteService.getAllUserAlertes(localStorage.getItem('username'))
      .subscribe(data=>{
        console.log(data);
        this.alerteList = data
      })
  }

}
