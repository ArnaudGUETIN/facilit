import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CollaborateurdashboardComponent} from './collaborateurdashboard/collaborateurdashboard.component';
import {TaskComponent} from './task/task.component';
import {TaskViewComponent} from './task-view/task-view.component';
import {TaskEditComponent} from './task-edit/task-edit.component';
import {TaskToDoComponent} from "./task-to-do/task-to-do.component";
import {AlerteComponent} from "./alerte/alerte.component";
import {AlerteDetailComponent} from "./alerte-detail/alerte-detail.component";

const routes: Routes = [
  {
    path: '',
    component: CollaborateurdashboardComponent,
    data: {
      title: 'Tableau de bord'
    }
  },
  {
    path: 'tasks',
    component: TaskComponent,
    data: {
      title: 'tâches'
    }
  },
  {
    path: 'taskView',
    component: TaskViewComponent,
    data: {
      title: 'tâche'
    }
  },
  {
    path: 'taskEdit',
    component: TaskEditComponent,
    data: {
      title: 'tâche'
    }
  },
  {
    path: 'taskToDo',
    component: TaskToDoComponent,
    data: {
      title: 'tâche'
    }
  },
  {
    path: 'alertes',
    component: AlerteComponent,
    data: {
      title: 'Alertes'
    }
  },
  {
    path: 'alertes/:id',
    component: AlerteDetailComponent,
    data: {
      title: 'Alerte Detail'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CollaborateurRoutingModule {
}
