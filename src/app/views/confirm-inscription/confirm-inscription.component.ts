import {AfterContentChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../Services/user.service';
import {AbstractControl, FormBuilder, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {PassWordErrorsStateMatcher} from './PassWordErrorsStateMatcher';
import {MessageService} from 'primeng/api';

const helper = new JwtHelperService();

@Component({
  selector: 'app-confirm-inscription',
  templateUrl: './confirm-inscription.component.html',
  styleUrls: ['./confirm-inscription.component.scss']
})
export class ConfirmInscriptionComponent implements OnInit {
  confirmPassword: any;
  password: any;
  hide = true;
  token: string = '';
  changePasswordGroup: any;
  matcher = new PassWordErrorsStateMatcher();

  constructor(public actvRoute: ActivatedRoute, private userService: UserService, private fb: FormBuilder,
              private messageService: MessageService, private router: Router) {
    this.actvRoute.queryParams.subscribe(filterEntreprise => {


      this.token = filterEntreprise.token;

    });
  }

  checkPasswords: ValidatorFn = (group: AbstractControl): ValidationErrors | null => {
    let pass = group.get('password').value;
    let confirmPass = group.get('confirmPassword').value;
    return pass === confirmPass ? null : {notSame: true};
  };

  ngOnInit(): void {
    this.changePasswordGroup = this.fb.group({
      password: [this.password, Validators.required],
      confirmPassword: [this.confirmPassword, Validators.required],

    }, {validators: this.checkPasswords});
  }

  validatePassword() {
    if(this.token){
      this.userService.changeUserPassword(this.password, this.token)
        .subscribe(data => {
          if (data) {
            this.messageService.add({severity: 'success', summary: 'Success', detail: 'changement effectué '});
            this.router.navigate(['login', { username: helper.decodeToken(this.token).sub }]);
          } else {
            this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur est survenu lors de cette opération'});
          }
        }, error => {
          this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur est survenu lors de cette opération'});
        });
    }else {
      this.changePasswordGroup.setErrors(null)
      this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur est survenu lors de cette opération'});
    }

  }
}
