import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ConfirmInscriptionComponent} from './confirm-inscription.component';
import {ActivatedRoute, RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {DialogService} from "primeng/dynamicdialog";
import {MessageService} from "primeng/api";

describe('ConfirmInscriptionComponent', () => {
  let component: ConfirmInscriptionComponent;
  let fixture: ComponentFixture<ConfirmInscriptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConfirmInscriptionComponent],
      imports: [ReactiveFormsModule,FormsModule,HttpClientModule,RouterModule.forRoot([]),MatDialogModule],
      providers:[MessageService]

    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmInscriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
