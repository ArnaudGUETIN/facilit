import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs';

const helper = new JwtHelperService();
const endpoint = environment.endpoint;

@Injectable({
  providedIn: 'root'
})
export class CollaborateurserviceService {
  public CURRENT_COLLABORATEUR = new BehaviorSubject<any>('');
  _COLLABORATEUR = this.CURRENT_COLLABORATEUR.asObservable();

  constructor(public http: HttpClient) {
  }

  static getCollaborateurId():number{
    let parse = JSON.parse(localStorage.getItem('collaborateur'));
    return  parse?parse.collaborateurId:null;
  }
  static getCollaborateur():any{
    let parse = JSON.parse(localStorage.getItem('collaborateur'));
    return   parse?parse:null;
  }
  getOneCollab(username: string): any {
    return this.http.post(environment.endpoint + 'getCollaborateur', username, {observe: 'response'});
  }

  getAllEntrepriseCollaborateurs(entrepriseId: number, currentTaskId: number, niveauHierarchique: number,): any {
    return this.http.get(environment.endpoint + 'getAllCollaborateurs/' + entrepriseId + '?currentTaskId=' + currentTaskId + '&niveauHierarchique=' + niveauHierarchique);
  }

  addCollaborateur(entrepriseId: number, collaborateur: any) {
    return this.http.post(endpoint + 'addCollaborateur?entrepriseId=' + entrepriseId, collaborateur);
  }

  updateCollaborateur(collaborateur: any) {
    return this.http.put(endpoint + 'updateCollaborateur', collaborateur);
  }

  deleteCollaborateur(collaborateurId: number) {
    return this.http.delete(endpoint + 'deleteCollaborateur/' + collaborateurId);
  }

  getAllTaskCollaborateurs(taskId: number): any {
    return this.http.get(environment.endpoint + 'collaborateurs/search/findByAffectationsTaskTaskId?taskId=' + taskId);
  }

  getAllResponsable(niveauHierarchique: number, entrepriseId: number): any {
    return this.http.get(environment.endpoint + 'collaborateurs/search/findByNiveauHierarchiqueIsGreaterThanAndEntrepriseEntrepriseId?niveauHierarchique=' + niveauHierarchique + '&entrepriseId=' + entrepriseId);
  }
}
