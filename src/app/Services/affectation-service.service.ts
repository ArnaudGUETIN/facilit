import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

const helper = new JwtHelperService();
const endpoint = environment.endpoint;

@Injectable({
  providedIn: 'root'
})
export class AffectationServiceService {

  constructor(public http: HttpClient) {
  }


  addAffectations(affectation: any) {
    return this.http.post(endpoint + 'affectations/add', affectation);
  }

  deleteAffectations(affectation: any) {
    return this.http.post(endpoint + 'affectations/delete', affectation);
  }


}
