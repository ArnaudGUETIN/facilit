import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

const helper = new JwtHelperService();
const endpoint = environment.endpoint;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(public http: HttpClient) {
  }

  getAllUsers(): any {
    return this.http.get(environment.endpoint + 'getAllUsers');
  }

  changeUserPassword(password: string, token: string) {
    return this.http.post(environment.endpoint + 'changeUserPassword', {password: password, token: token});
  }

  getAllFacilitUsers(): any {
    return this.http.get(environment.endpoint + 'appUsers/search/findAllByCollaborateurNullAndEntrepriseNull');
  }

  getAllRoleUsers(roleName: string): any {
    return this.http.get(environment.endpoint + 'appUsers/search/findAllByRolesRoleName?rolename=' + roleName);
  }
  userEmailAlreadyExist(email: string): any {
    return this.http.get(environment.endpoint + 'userEmailAlreadyExist?email=' + email);
  }

  getUser(userId: number): any {
    return this.http.get(environment.endpoint + 'appUsers/' + userId);
  }
  resetPassword(email: string): any {
    return this.http.get(environment.endpoint + 'resetPassword?email=' + email);
  }
  getAllRoles(): any {
    return this.http.get(environment.endpoint + 'appRoles');
  }

  addUser(user: any) {
    return this.http.post(environment.endpoint + 'appUsers/addUser', user);
  }

  updateUser(user: any) {
    return this.http.put(environment.endpoint + 'appUsers/updateUser', user);
  }

  deleteUser(userId: any) {
    return this.http.delete(environment.endpoint + 'appUsers/deleteUser/' + userId);
  }
}
