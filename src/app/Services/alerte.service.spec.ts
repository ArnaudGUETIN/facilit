import { TestBed } from '@angular/core/testing';

import { AlerteService } from './alerte.service';
import {HttpClientModule} from "@angular/common/http";

describe('AlerteService', () => {
  let service: AlerteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(AlerteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
