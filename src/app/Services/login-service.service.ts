import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {JwtHelperService} from '@auth0/angular-jwt';
import {BehaviorSubject} from 'rxjs';

const helper = new JwtHelperService();

@Injectable()
export class LoginServiceService {
  jwtToken: string;
  public IS_ADMIN = new BehaviorSubject<any>('');
  ADMIN = this.IS_ADMIN.asObservable();
  public IS_RESTAURANT = new BehaviorSubject<any>('');
  RESTAURANT = this.IS_RESTAURANT.asObservable();
  public IS_LIVREUR = new BehaviorSubject<any>('');
  LIVREUR = this.IS_LIVREUR.asObservable();
  public IS_FACEBOOK_USER = new BehaviorSubject<any>('');
  FACEBOOK = this.IS_FACEBOOK_USER.asObservable();

  constructor(private http: HttpClient) {
  }

  attempAuthentification(user) {
    return this.http.post('https://www.facilit-dev.com/platform-facilit-1.0/login', user, {observe: 'response'});
  }

  getUserDetails(username: string) {
    return this.http.post(environment.endpoint + 'userDetails', username, {observe: 'response'});
  }

  saveToken(jwt: string) {
    const decodedToken = helper.decodeToken(jwt);
    // const tokenPayload = decode(jwt);
// Other functions
    const expirationDate = helper.getTokenExpirationDate(jwt);
    const isExpired = helper.isTokenExpired(jwt);
    console.log(decodedToken);
    localStorage.setItem('username', decodedToken.sub);
    localStorage.setItem('token', jwt);
  }

  keepUserDetails(jwt: string) {
    const tokenPayload = helper.decodeToken(jwt);
    localStorage.setItem('username', tokenPayload.sub);
    localStorage.setItem('token', jwt);
  }


  getTokenExpirationDate(token: string): Date {
    const expirationDate = helper.getTokenExpirationDate(token);

    if (expirationDate === undefined) return null;


    return expirationDate;
  }

  public isTokenExpired(): boolean {
    let token = this.loadToken();
    if (!token) return true;

    const date = this.getTokenExpirationDate(token);
    if (date === undefined) return false;
    let isExpired = !(date.valueOf() > new Date().valueOf());
    if (isExpired) {
      this.emptyLocalStorage();

    }
    return isExpired;
  }

  loadToken() {
    return localStorage.getItem('token');
  }

  updateUser(username: string) {
    if (this.jwtToken == null) this.loadToken();
    return this.http.put(environment.endpoint + 'authentification/user/update', username,);
  }

  getLocaleUsername(): string {
    return localStorage.getItem('username');
  }

  private emptyLocalStorage() {
    localStorage.removeItem('client');
    localStorage.removeItem('clientId');
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('appRole');
    this.IS_ADMIN.next(false);
    localStorage.removeItem('restaurant');
    localStorage.removeItem('restaurantId');
    this.IS_RESTAURANT.next(false);
  }

}
