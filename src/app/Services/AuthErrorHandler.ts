import {ErrorHandler, Injectable, Injector} from '@angular/core';
import {Router} from '@angular/router';
import {MessageService} from "primeng/api";

@Injectable()
export class AuthErrorHandler implements ErrorHandler {

  constructor(private injector: Injector, private messageService: MessageService) {
  }

  handleError(error: any) {
    const router = this.injector.get(Router);

  console.log(error);
    //this.messageService.add({severity: 'error', summary: 'Error', detail: 'Une erreur s\'est produite lors de cette oprération'});
     //  router.navigate(['/404']);
  }
}
