import { Injectable } from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs';
import {HttpClient} from "@angular/common/http";

const helper = new JwtHelperService();
const endpoint = environment.endpoint;
@Injectable({
  providedIn: 'root'
})
export class AlerteService {

  constructor(public http: HttpClient) {
  }
  getAllUserAlertes(username:string): any {
    return this.http.get(environment.endpoint + 'getAllUserAlertes' + '?username=' + username);
  }
}
