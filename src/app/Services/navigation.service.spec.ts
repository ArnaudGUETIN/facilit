import {TestBed} from '@angular/core/testing';

import {NavigationService} from './navigation.service';
import {Router, RouterModule} from "@angular/router";

describe('NavigationService', () => {
  let service: NavigationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([])]
    });
    service = TestBed.inject(NavigationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
