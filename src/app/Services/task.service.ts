import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';
import {BehaviorSubject} from 'rxjs';
import {HttpClient} from '@angular/common/http';

const helper = new JwtHelperService();
const endpoint = environment.endpoint;

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  public CURRENT_TASK = new BehaviorSubject<any>('');
  _TASK = this.CURRENT_TASK.asObservable();

  constructor(public http: HttpClient) {
  }

  getOneTask(taskId: any): any {
    return this.http.get(environment.endpoint + 'getTask/' + taskId);
  }

  getAllEntrepriseTasks(entrepriseId: number): any {
    return this.http.get(environment.endpoint + 'tasks/search/findByEntrepriseEntrepriseId?entrepriseId=' + entrepriseId);
  }
  getAllCollaborateurTasks(collaborateurId: number): any {
    return this.http.get(environment.endpoint + 'getCollaborateursTasks/' + collaborateurId);
  }
  addTask(entrepriseId: number, task: any) {
    return this.http.post(endpoint + 'addTask?entrepriseId=' + entrepriseId, task);
  }

  updateTask(task: any, isReleve: boolean) {
    return this.http.put(endpoint + 'updateTask?isReleve=' + isReleve, task);
  }

  deleteTask(taskId: number) {
    return this.http.delete(endpoint + 'deleteTask/' + taskId);
  }

  getAllTaskEtape(taskId: number): any {
    return this.http.get(environment.endpoint + 'etapes/search/findByTaskTaskId?taskId=' + taskId);
  }

  getAllTaskEtapes(taskId: number): any {
    return this.http.get(environment.endpoint + 'getAllEtapes/' + taskId);
  }

  addEtape(taskId: number, etape: any) {
    return this.http.post(endpoint + 'addEtape?taskId=' + taskId, etape);
  }

  updateEtape(etape: any) {
    return this.http.put(endpoint + 'updateEtape', etape);
  }
  updateSteps(etapeValue: any) {
    return this.http.post(endpoint + 'updateSteps', etapeValue);
  }

  deleteEtape(etapeId: number) {
    return this.http.delete(endpoint + 'etapes/delete/' + etapeId);
  }

/*  getAllTaskReleves(taskId: number): any {
    return this.http.get(environment.endpoint + 'releves/search/findAllByTaskTaskId?taskId=' + taskId);
  }*/
  getAllTaskReleves(taskId: number): any {
    return this.http.get(environment.endpoint + 'getAllReleves/' + taskId);
  }

  addReleve(taskId: number, releve: any) {
    return this.http.post(endpoint + 'addReleve?taskId=' + taskId, releve);
  }

  updateReleve(releve: any) {
    return this.http.put(endpoint + 'updateReleve', releve);
  }

  deleteReleve(releveId: number) {
    return this.http.delete(endpoint + 'releves/delete/' + releveId);
  }
  getTypeReleveChartData(collaborateurId:number){
    return this.http.get(environment.endpoint + 'getTypeReleveChartData/' + collaborateurId);
  }
  getAnnualCompteurReport(taskId:number){
    return this.http.get(environment.endpoint + 'getAnnualCompteurReport/' + taskId);
  }
  addSaisie(saisie: any) {
    return this.http.post(endpoint + 'addSaisie' , saisie);
  }

  getByLocalDateAndTaskTaskId(taskId: number,date:Date) {
    return this.http.post(environment.endpoint + 'getByLocalDateAndTaskTaskId?taskId=' + taskId,date);
  }
}
export enum TaskStatus {
  ANNULEE = 0,
  PLANIFIEE = 1,
  EN_ATTENTE = 2,
  EN_COURS = 3,
  TERMINEE = 4,
}
