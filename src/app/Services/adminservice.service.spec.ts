import {TestBed} from '@angular/core/testing';

import {AdminserviceService} from './adminservice.service';
import {HttpClientModule} from "@angular/common/http";

describe('AdminserviceService', () => {
  let service: AdminserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule]
    });
    service = TestBed.inject(AdminserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
