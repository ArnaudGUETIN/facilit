import {HttpHeaders} from '@angular/common/http';


const AUTH_HEADER_KEY = 'Authorization';
const AUTH_PREFIX = 'Bearer';
const TOKEN_NAME = 'token';

export class AuthRequestOptions {

  constructor() {
    let header = new HttpHeaders();
    const token = localStorage.getItem(TOKEN_NAME);
    if (token) {
      header.append(AUTH_HEADER_KEY, `${token}`);
    }
  }

}
