import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {JwtHelperService} from "@auth0/angular-jwt";

const helper = new JwtHelperService();
const endpoint = environment.endpoint;
@Injectable({
  providedIn: 'root'
})
export class InstallationService {

  constructor(public http: HttpClient) { }

  getAllEntrepriseInstallations(entrepriseId:number): any {
    return this.http.get(environment.endpoint + 'getAllEntrepriseInstallations/' + entrepriseId);
  }
  getAllEntrepriseInstallationsAndType(entrepriseId:number,type:number): any {
    return this.http.get(environment.endpoint + 'getAllEntrepriseInstallationsAndType/' + entrepriseId+"?type="+type);
  }
  getAllEntrepriseAndTaskCompteurs(entrepriseId:number,taskId:number): any {
    return this.http.get(environment.endpoint + 'getAllEntrepriseAndTaskCompteurs?entrepriseId=' + entrepriseId+"&taskId="+taskId);
  }

  addInstallation(installation:any): any {
    return this.http.post(environment.endpoint + 'addInstallation' ,installation);
  }
  addCompteur(compteur:any): any {
    return this.http.post(environment.endpoint + 'addCompteur' ,compteur);
  }
  addCompteurToTask(compteurId:number,taskId:number): any {
    return this.http.get(environment.endpoint + 'addCompteurToTask?compteurId='+compteurId+"&taskId="+taskId);
  }
  getAllEntrepriseCompteur(entrepriseId:number): any {
    return this.http.get(environment.endpoint + 'getAllEntrepriseCompteur/' + entrepriseId);
  }
}
