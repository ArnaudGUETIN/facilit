import {TestBed} from '@angular/core/testing';

import {UploadFilesService} from './upload-files.service';
import {HttpClientModule} from "@angular/common/http";

describe('UploadFilesService', () => {
  let service: UploadFilesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule]
    });
    service = TestBed.inject(UploadFilesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
