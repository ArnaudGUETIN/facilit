import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {fromEvent, Observable, Subscription, throwError} from 'rxjs';
import {AuthGuard} from './AuthGuard';
import {MessageService} from 'primeng/api';
import {catchError} from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
// Observables & Subscriptions to check online/offline connectivity status
  onlineEvent: Observable<Event>;
  offlineEvent: Observable<Event>;
  subscriptions: Subscription[] = [];

  constructor(public auth: AuthGuard, private messageService: MessageService) {
    this.checkOnlineStatus();
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.auth.getToken() && !request.url.endsWith('login')) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${this.auth.getToken()}`
        }
      });
    }
    if (request.url.endsWith('send')) {
      request = request.clone({
        setHeaders: {
          Autorization: `key=AAAAdHglqjM:APA91bGfe8XvoszGvzrWy4HuiZYx4aTwWUmHAZs8jH94IJibfGL7JeZMYwus-FjFNJBpTfv9qcOUEs7JWpEhPa2WjyCYC9Qk3WR3Z9IiKrDGf4aoXAYX0yu2oBlGNK8cbyodMt36OhXU`
        }
      });
    }
    if (!window.navigator.onLine) {
      // if there is no internet, throw a HttpErrorResponse error
      // since an error is thrown, the function will terminate here
      const error = {
        status: 0,
        error: {
          description: 'Check Connectivity!'
        },
        statusText: 'Check Connectivity!'
      };
      return throwError(new HttpErrorResponse(error));
    }

    return next.handle(request)
      .pipe(catchError((error: HttpErrorResponse) => {
        let errorMsg = '';
        this.checkOnlineStatus();
        if (error.error instanceof ErrorEvent) {
          errorMsg = `Error: ${error.error.message}`;
        } else {
          // this.messageService.add({severity:'error', summary: 'Error', detail:errorMsg})
          errorMsg = `Error Code: ${error.status},  Message: ${error.message}`;
        }
        this.messageService.add({severity: 'error', summary: 'Error', sticky: true, detail: errorMsg});
        return throwError(errorMsg);
      }));
  }

  //Get the online/offline connectivity status from browser window
  checkOnlineStatus = () => {
    this.onlineEvent = fromEvent(window, 'online');
    this.offlineEvent = fromEvent(window, 'offline');

    this.subscriptions.push(this.onlineEvent.subscribe(() => this.messageService.add({
      severity: 'info',
      summary: 'Info',
      detail: 'Connected'
    })));
    this.subscriptions.push(this.offlineEvent.subscribe(() => this.messageService.add({
      severity: 'error',
      summary: 'Error',
      detail: 'Check Connectivity!'
    })));
  };
}
