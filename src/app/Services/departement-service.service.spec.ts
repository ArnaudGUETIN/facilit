import {TestBed} from '@angular/core/testing';

import {DepartementServiceService} from './departement-service.service';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";

describe('DepartementServiceService', () => {
  let service: DepartementServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule,ReactiveFormsModule,FormsModule],
    });
    service = TestBed.inject(DepartementServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
