import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';

const helper = new JwtHelperService();
const endpoint = environment.endpoint;

@Injectable({
  providedIn: 'root'
})
export class EntrepriseserviceService {

  public CURRENT_ENTREPRISE = new BehaviorSubject<any>('');
  _ENTREPRISE = this.CURRENT_ENTREPRISE.asObservable();

  constructor(public http: HttpClient) {
  }

  addEntreprise(entreprise: any) {
    return this.http.post(endpoint + 'addEntreprise', entreprise);
  }

  addCamping(camping: any) {
    return this.http.post(endpoint + 'addCamping', camping);
  }

  updateEntreprise(entreprise: any) {
    return this.http.put(endpoint + 'updateEntreprise', entreprise);
  }

  updateCamping(camping: any) {
    return this.http.put(endpoint + 'updateCamping', camping);
  }

  deleteEntreprise(entrepriseId: number) {
    return this.http.delete(endpoint + 'deleteEntreprise/' + entrepriseId);
  }

  getOneEntreprise(username: string): any {
    return this.http.post(environment.endpoint + 'getEntreprise', username, {observe: 'response'});
  }

  getOneEntrepriseById(entrepriseId: number): any {
    return this.http.get(environment.endpoint + 'getEntreprise/' + entrepriseId);
  }
  getEntrepriseRib(entrepriseId: number): any {
    return this.http.get(environment.endpoint + 'getEntrepriseRib/' + entrepriseId);
  }

  getOneCampingById(campingId: number): any {
    return this.http.get(environment.endpoint + 'getCamping/' + campingId);
  }

  getAllEntreprises(): any {
    return this.http.get(environment.endpoint + 'getAllEntreprises');
  }

  getManagerEntreprises(managerId: number): any {
    return this.http.get(environment.endpoint + 'entreprises/search/findByManagerUserId?managerId=' + managerId);
  }

  getManagerEntreprisesByUsername(managerUsername: any): any {
    return this.http.get(environment.endpoint + 'entreprises/search/findByManagerUserUsername?managerUsername=' + managerUsername);
  }

  addMannagerToEntreprise(entrepriseId: number, userId: number): any {
    return this.http.get(environment.endpoint + 'addMannagerToEntreprise?entrepriseId=' + entrepriseId + '&userId=' + userId);
  }

  detachMannagerToEntreprise(entrepriseId: number, userId: number): any {
    return this.http.get(environment.endpoint + 'detachMannagerToEntreprise?entrepriseId=' + entrepriseId + '&userId=' + userId);
  }

  getCampingEmpl(entrepriseId: number,type:number) {
    return this.http.get(environment.endpoint + 'getCampingEmplacement/' + entrepriseId+"?type="+type);
  }

  addCampingEmpl(emplacement:any) {
    return this.http.post(environment.endpoint + 'addEmplacement/',emplacement);
  }

  updateCampingEmpl(emplacement:any) {
    return this.http.put(environment.endpoint + 'updateEmplacement/',emplacement);
  }

  updateEntrepriseRib(rib:any) {
    return this.http.put(environment.endpoint + 'updateRib/',rib);
  }

   getChartData() {
    return this.http.get("assets/FakeData.json");
  }
}
