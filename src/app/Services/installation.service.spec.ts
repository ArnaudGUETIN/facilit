import { TestBed } from '@angular/core/testing';

import { InstallationService } from './installation.service';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {BrowserDynamicTestingModule} from "@angular/platform-browser-dynamic/testing";

describe('InstallationService', () => {
  let service: InstallationService;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule,ReactiveFormsModule,FormsModule,BrowserDynamicTestingModule],
    });
    service = TestBed.inject(InstallationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
