import {TestBed} from '@angular/core/testing';

import {AffectationServiceService} from './affectation-service.service';
import {HttpClientModule} from "@angular/common/http";

describe('AffectationServiceService', () => {
  let service: AffectationServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientModule]
    });
    service = TestBed.inject(AffectationServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
