import {Injectable} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router} from '@angular/router';
import {LoginServiceService} from './login-service.service';
import {JwtHelperService} from '@auth0/angular-jwt';
import {RoleEnum} from "../shared/enums/RoleEnum";
import {MessageService} from "primeng/api";

const helper = new JwtHelperService();


@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private loginService: LoginServiceService,
    private route: ActivatedRoute,
    private messageService: MessageService) {
  }

  public getToken(): string {
    return localStorage.getItem('token');
  }

  canActivate(route: ActivatedRouteSnapshot) {
    const expectedRole = route.firstChild.data.expectedRole;
    const token = this.getToken();
    const currentRoute = this.router.url;

    if (token != null) {

      const tokenPayload = helper.decodeToken(token);
      console.log(RoleEnum.roleAdmin);
      console.log(expectedRole);
      console.log(!tokenPayload.roles.some(r => r.authority === expectedRole));
      if(!tokenPayload.roles.some(r => (r.authority === expectedRole) || r.authority===RoleEnum.roleAdmin))
      {
        this.messageService.add({severity: 'info', summary: 'Attention', detail: 'Vous ne pouvez pas accédez à cette ressource'});
        this.router.navigate([currentRoute]);
      }
    }


    if (!this.loginService.isTokenExpired()) {
      return true;
    }

    this.router.navigate(['login']);
    return false;
  }

}
