import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {JwtHelperService} from '@auth0/angular-jwt';
import {HttpClient} from '@angular/common/http';

const helper = new JwtHelperService();
const endpoint = environment.endpoint;

@Injectable({
  providedIn: 'root'
})
export class AdminserviceService {

  constructor(public http: HttpClient) {
  }

  getAdmin(username: string): any {
    return this.http.post(environment.endpoint + 'getAdmin', username, {observe: 'response'});
  }
}
