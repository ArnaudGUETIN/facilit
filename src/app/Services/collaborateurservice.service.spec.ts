import {TestBed} from '@angular/core/testing';

import {CollaborateurserviceService} from './collaborateurservice.service';
import {UserEditModalComponent} from "../shared/dialogModal/user-edit-modal/user-edit-modal.component";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";

describe('CollaborateurserviceService', () => {
  let service: CollaborateurserviceService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports:[HttpClientModule],
    })
    service=TestBed.inject(CollaborateurserviceService)
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
