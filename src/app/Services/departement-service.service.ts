import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

const helper = new JwtHelperService();
const endpoint = environment.endpoint;

@Injectable({
  providedIn: 'root'
})
export class DepartementServiceService {

  constructor(public http: HttpClient) {
  }

  getOneService(serviceId: any): any {
    return this.http.get(environment.endpoint + 'services/' + serviceId);
  }

  getAllEntrepriseServices(entrepriseId: number): any {
    return this.http.get(environment.endpoint + 'services/search/findByEntrepriseEntrepriseId?entrepriseId=' + entrepriseId);
  }

  addService(entrepriseId: number, service: any) {
    return this.http.post(endpoint + 'services/add?entrepriseId=' + entrepriseId, service);
  }

  updateService(service: any) {
    return this.http.put(endpoint + 'services/update', service);
  }

  deleteService(service: any) {
    return this.http.post(endpoint + 'services/delete', service);
  }
}
