import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {environment} from '../../environments/environment';
import {HttpClient, HttpEvent, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

const helper = new JwtHelperService();
const endpoint = environment.endpoint;

@Injectable({
  providedIn: 'root'
})
export class UploadFilesService {

  constructor(public http: HttpClient) {
  }

  upload(file: File, categorie: string, id: number): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();

    formData.append('file', file);

    const req = new HttpRequest('POST', `${endpoint}upload/update/` + categorie + '/' + id, formData, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.http.request(req);
  }

  getFiles(categorie: string, id: number, htmlid: string): any {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', `${endpoint}upload/files/` + categorie + '/' + id, true);
    xhr.setRequestHeader('Authorization', localStorage.getItem('token'));
    xhr.responseType = 'arraybuffer';
    xhr.onload = function (oEvent) {
      var arrayBuffer = xhr.response; // Note: not oReq.responseText
      if (arrayBuffer) {
        var binary = '';
        var u8 = new Uint8Array(arrayBuffer);
        //var b64encoded = btoa(String.fromCharCode.apply(null, u8));
        var len = u8.byteLength;
        for (var i = 0; i < len; i++) {
          binary += String.fromCharCode( u8[ i ] );
        }
        if (len) {
          var mimetype = 'image/png'; // or whatever your image mime type is childNodes[1].getAttribute('src')
          (<HTMLInputElement>document.getElementById(htmlid)).src = 'data:' +  ';base64,' + btoa(binary);
        } else {
          (<HTMLInputElement>document.getElementById(htmlid)).src = '/assets/img/avatars/office-building.png';
        }

      }
    };

    xhr.send(null);
    //return this.http.get(`${endpoint}upload/files/`+categorie+'/'+id);
  }


}
