import {TestBed} from '@angular/core/testing';

import {EntrepriseserviceService} from './entrepriseservice.service';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {BrowserDynamicTestingModule} from "@angular/platform-browser-dynamic/testing";

describe('EntrepriseserviceService', () => {
  let service: EntrepriseserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[BrowserDynamicTestingModule,HttpClientModule,ReactiveFormsModule,FormsModule],
    });
    service = TestBed.inject(EntrepriseserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
