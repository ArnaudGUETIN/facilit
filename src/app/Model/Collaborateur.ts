export class Collaborateur {

  collaborateurId: number;
  nom: string;
  prenom: string;
  email: string;
  telephone: string;
  fonction: string;
  profil: string;
  dateNaissance:Date;
  niveauHierarchique: number;
  serviceIds: number[];

  constructor() {
    this.collaborateurId = 0;
    this.nom = '';
    this.prenom = '';
    this.email = '';
    this.telephone = '';
    this.fonction = '';
    this.profil = '';
    this.dateNaissance = new Date();
    this.niveauHierarchique = 0;
    this.serviceIds = [];
  }

  copy(collaborateur: Collaborateur): Collaborateur {
    this.collaborateurId = collaborateur.collaborateurId;
    this.nom = collaborateur.nom;
    this.prenom = collaborateur.prenom;
    this.email = collaborateur.email;
    this.fonction = collaborateur.fonction;
    this.profil = collaborateur.profil;
    this.telephone = collaborateur.telephone;
    this.niveauHierarchique = collaborateur.niveauHierarchique;
    this.dateNaissance = collaborateur.dateNaissance;
    this.serviceIds = collaborateur.serviceIds;
    return this;
  }

}
