export class Etape {
  etapeId: number;
  ordre: number;
  designation: string;
  dateDebut: Date;
  dateFin: Date;
  statut: number;
  description: string;
  mandatory: boolean;
  checked: boolean;

  constructor() {
    this.etapeId = 0;
    this.ordre = 0;
    this.designation = '';
    this.dateDebut = null;
    this.dateFin = null;
    this.statut = 0;
    this.description = '';
    this.mandatory = false;
    this.checked = false;
  }
}
