export class Releve {
  releveId: number;
  ordre: number;
  dateReleve: Date;
  indexValue: number;
  acteurId:number;
  taskId:number;
  acteurName:string;

  constructor() {
    this.releveId = 0;
    this.ordre = 0;
    this.dateReleve = null;
    this.indexValue = 0;
    this.acteurId = 0;
    this.taskId = 0;
    this.acteurName = '';
  }
}
