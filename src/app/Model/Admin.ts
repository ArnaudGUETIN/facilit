export class Admin {

  id: number;
  nom: string;
  prenom: string;
  email: string;

  constructor() {
    this.id = 0;
    this.nom = '';
    this.prenom = '';
    this.email = '';
  }
}
