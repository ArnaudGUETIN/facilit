export class User {
  public static ROLE_ADMIN: string = 'ADMIN';
  public static ROLE_COLLABORATEUR: string = 'COLLABORATEUR';
  public static ROLE_ENTREPRISE: string = 'ENTREPRISE';
  id: number;
  nom: string;
  prenom: string;
  username: string;
  email: string;
  dateCreation: string;
  currentRole: string;
  currentRoleId: number;
  rolesIds: any;

  constructor() {
    this.id = 0;
    this.email = '';
    this.username = '';
    this.dateCreation = '';
    this.currentRole = '';
    this.nom = '';
    this.prenom = '';
    this.currentRoleId = 0;
    this.rolesIds = [];
  }

  copy(user: User): User {
    this.id = user.id;
    this.nom = user.nom;
    this.prenom = user.prenom;
    this.email = user.email;
    this.username = user.username;
    this.currentRoleId = user.currentRoleId;
    this.rolesIds = user.rolesIds;
    return this;
  }
}
