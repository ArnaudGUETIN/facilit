export class Affectation {
  affectationId: number;
  designation: string;
  commentaire: string;
  dateAffectation: any;
  statut: number;
  collaborateurId: number;
  taskId: number;

  constructor() {
    this.affectationId = 0;
    this.designation = '';
    this.commentaire = '';
    this.dateAffectation = null;
    this.statut = 0;
    this.collaborateurId = 0;
    this.taskId = 0;
  }
}
