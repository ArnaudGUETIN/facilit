export class Entreprise {

  entrepriseId: number;
  emailContact: string;
  code: string;
  nomCommercial: string;
  siret: string;
  siren: string;
  nic: string;
  codeAPE: string;
  telephone: string;
  telephone2: string;
  type: string;
  block: string;
  rue: string;
  ville: string;
  pays: string;
  codePostal: string;
  block2: string;
  rue2: string;
  ville2: string;
  pays2: string;
  codePostal2: string;
  raisonSociale: string;
  website: string;
  numeroTVA: string;
  entrepriseType: string;
  ribId: number;

  constructor() {
    this.entrepriseId = 0;
    this.code = '';
    this.emailContact = '';
    this.nomCommercial = '';
    this.siret = '';
    this.type = '';
    this.telephone = '';
    this.ville = '';
    this.codePostal = '';
    this.telephone2 = '';
    this.ville2 = '';
    this.codePostal2 = '';
    this.entrepriseType = '';
    this.siren = '';
    this.nic = '';
    this.codeAPE = '';
    this.rue = '';
    this.rue2 = '';
    this.block = '';
    this.block2 = '';
    this.ribId = 0;
  }

  copy(entreprise: Entreprise): Entreprise {
    this.entrepriseId = entreprise.entrepriseId;
    this.code = entreprise.code;
    this.emailContact = entreprise.emailContact;
    this.nomCommercial = entreprise.nomCommercial;
    this.siret = entreprise.siret;
    this.siren = entreprise.siren;
    this.nic = entreprise.nic;
    this.codeAPE = entreprise.codeAPE;
    this.type = entreprise.type;
    this.telephone = entreprise.telephone;
    this.telephone2 = entreprise.telephone2;
    this.ville = entreprise.ville;
    this.codePostal2 = entreprise.codePostal2;
    this.telephone2 = entreprise.telephone2;
    this.ville2 = entreprise.ville2;
    this.codePostal = entreprise.codePostal;
    this.rue = entreprise.rue;
    this.rue2 = entreprise.rue2;
    this.block = entreprise.block;
    this.block2 = entreprise.block2;
    this.ribId = entreprise.ribId;
    return this;
  }
}
