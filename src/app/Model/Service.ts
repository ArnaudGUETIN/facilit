export class Service {

  serviceId: number;
  designation: string;
  code: string;
  responsableId: number;
  responsableName: string;
  entrepriseId: number;

  constructor() {
    this.serviceId = 0;
    this.designation = '';
    this.code = '';
    this.entrepriseId = 0;
    this.responsableName = '';
  }

  copy(service: Service): Service {
    this.serviceId = service.serviceId;
    this.designation = service.designation;
    this.code = service.code;
    this.responsableId = service.responsableId;
    this.entrepriseId = service.entrepriseId;
    this.responsableName = service.responsableName;
    return this;
  }
}
