import {Entreprise} from './Entreprise';

export class Camping extends Entreprise {

  classement: number;
  dateDernierClassement: Date;
  dateProchainClassement: Date;
  nombreEmplacement: number;
  dateCreation: Date;
  superficie: number;
  gouvernance: number;
  situationGeographique: string;
  proprieteFonciere: boolean;
  proprieteFondDeCommerce: boolean;
  delegationServicePublic: boolean;

  constructor() {
    super();
    this.classement = 0;
    this.dateDernierClassement = null;
    this.dateProchainClassement = null;
    this.nombreEmplacement = 0;
    this.dateCreation = null;
    this.superficie = 0;
    this.gouvernance = 0;
    this.situationGeographique = '';
    this.proprieteFonciere = false;
    this.proprieteFondDeCommerce = false;
    this.delegationServicePublic = false;
  }
}
