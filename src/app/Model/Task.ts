import {Compteur} from './Compteur';
import {Time} from "@angular/common";

export class Task {

  taskId: number;
  designation: string;
  commentaire: string;
  instruction: string;
  dateDebut: any;
  dateFin: any;
  numero: number;
  statut: number;
  rappelTime: any;
  nature: number;
  categorie: number;
  typeTache: number;
  periodicite: number;
  responsableId: number;
  serviceId: number;
  responsableName: string;
  serviceName: string;
  compteurMV: Compteur;


  constructor() {
    this.taskId = 0;
    this.designation = '';
    this.commentaire = '';
    this.instruction = '';
    this.dateDebut = new Date();
    this.dateFin = new Date();
    this.numero = 0;
    this.statut = 0;
    this.nature = 0;
    this.typeTache = 0;
    this.categorie = 0;
    this.periodicite = 0;
    this.responsableId = 0;
    this.serviceId = 0;
    this.responsableName = '';
    this.serviceName = '';
    this.compteurMV = null;
  }

  copy(task: Task): Task {
    this.taskId = task.taskId;
    this.designation = task.designation;
    this.commentaire = task.commentaire;
    this.instruction = task.instruction;
    this.dateDebut = task.dateDebut;
    this.dateFin = task.dateFin;
    this.numero = task.numero;
    this.statut = task.statut;
    this.rappelTime = task.rappelTime;
    this.nature = task.nature;
    this.categorie = task.categorie;
    this.typeTache = task.typeTache;
    this.periodicite = task.periodicite;
    this.responsableId = task.responsableId;
    this.responsableName = task.responsableName;
    this.serviceName = task.serviceName;
    this.serviceId = task.serviceId;
    return this;
  }
}
