export class Compteur {
  compteurId: number;
  numero: number;
  valeur: number;
  fournisseur: string;

  constructor() {
    this.compteurId = 0;
    this.numero = 0;
    this.valeur = 0;
    this.fournisseur = '';
  }

}
