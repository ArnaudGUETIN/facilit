import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

import {IconSetService} from '@coreui/icons-angular';
import {freeSet} from '@coreui/icons';
import {LoginServiceService} from './Services/login-service.service';
import {ConnectionService} from 'ng-connection-service';
import {fromEvent, merge, Observable, Observer} from 'rxjs';
import {map} from 'rxjs/operators';
import {MessageService} from 'primeng/api';

const roleAdmin = 'ADMIN';
const roleCollaborateur = 'COLLABORATEUR';
const roleEntreprise = 'ENTREPRISE';
const roleConsultant = 'CONSULTANT';
const rolePartenaire = 'PARTENAIRE';
const role = localStorage.getItem('appRole');

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: ' \n' +
    '<div *ngIf="noInternetConnection" style="text-align: center;">     \n' +
    '    <h1>INTERNET CONNECTION IS NO THERE</h1>    \n' +
    '    <img width="111px" src="assets/img/avatars/no-wifi.png" alt="Wifi Disconnected" />    \n' +
    '</div> <router-outlet *ngIf="!noInternetConnection"></router-outlet>',
  providers: [IconSetService],
})
export class AppComponent implements OnInit {
  isConnected = false;
  noInternetConnection: boolean;

  constructor(
    private router: Router,
    public iconSet: IconSetService,
    private loginService: LoginServiceService,
    private messageService: MessageService,
    private connectionService: ConnectionService
  ) {
    // iconSet singleton
    iconSet.icons = {...freeSet};
    //this.checkConnection();
  }

  createOnline$() {
    return merge<boolean>(
      fromEvent(window, 'onoffline').pipe(map(() => false)),
      fromEvent(window, 'ononline').pipe(map(() => true)),
      new Observable((sub: Observer<boolean>) => {
        sub.next(navigator.onLine);
        sub.complete();
      }));
  }

  checkConnection() {
    this.connectionService.monitor().toPromise().then(isConnected => {
      console.log(isConnected);
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.noInternetConnection = false;
        this.messageService.add({severity: 'info', summary: 'Info', detail: 'Connected'});
      } else {
        this.noInternetConnection = true;
        this.messageService.add({severity: 'error', summary: 'Error', detail: 'Check Connectivity!'});
      }
    });
    console.log(this.isConnected);
  }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });


    if (!this.loginService.isTokenExpired()) {


    } else if (this.loginService.isTokenExpired()) {

    }
  }


}
