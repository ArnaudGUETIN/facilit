import {BackButtonDirective} from './back-button.directive';
import {TestBed} from "@angular/core/testing";
import {EntrepriseprofilComponent} from "../views/entreprise/entrepriseprofil/entrepriseprofil.component";
import {NavigationService} from "../Services/navigation.service";
import {AdminserviceService} from "../Services/adminservice.service";
import {Router, RouterModule} from '@angular/router';

describe('BackButtonDirective', () => {
  let navigationService: NavigationService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([])],
      providers: [NavigationService]
    })
      .compileComponents();
  });
  beforeEach(() => {
    TestBed.configureTestingModule({});
    navigationService = TestBed.inject(NavigationService);
  });
  it('should create an instance', () => {
    const directive = new BackButtonDirective(navigationService);
    expect(directive).toBeTruthy();
  });
});
