import {ErrorHandler, NgModule} from '@angular/core';
import {CommonModule, HashLocationStrategy, LocationStrategy} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {PerfectScrollbarConfigInterface, PerfectScrollbarModule} from 'ngx-perfect-scrollbar';

import {IconModule, IconSetModule, IconSetService} from '@coreui/icons-angular';
import {AppComponent} from './app.component';
// Import containers
import {DefaultLayoutComponent} from './containers';

import {P404Component} from './views/error/404.component';
import {P500Component} from './views/error/500.component';
import {LoginComponent} from './views/login/login.component';
import {RegisterComponent} from './views/register/register.component';
import {AppAsideModule, AppBreadcrumbModule, AppFooterModule, AppHeaderModule, AppSidebarModule,} from '@coreui/angular';
// Import routing module
import {AppRoutingModule} from './app.routing';
// Import 3rd party components
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {ChartsModule} from 'ng2-charts';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {LoginServiceService} from './Services/login-service.service';
import {EntrepriseserviceService} from './Services/entrepriseservice.service';
import {CollaborateurserviceService} from './Services/collaborateurservice.service';
import {AdminserviceService} from './Services/adminservice.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthErrorHandler} from './Services/AuthErrorHandler';
import {TokenInterceptor} from './Services/HttpInterceptor';
import {AuthGuard} from './Services/AuthGuard';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MatNativeDateModule,
  MatOptionModule
} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {EntrepriseEditModalComponent} from './shared/dialogModal/entreprise-edit-modal/entreprise-edit-modal.component';
import {ConfirmModalComponent} from './shared/dialogModal/confirm-modal/confirm-modal.component';
import {CollaborateurEditModalComponent} from './shared/dialogModal/collaborateur-edit-modal/collaborateur-edit-modal.component';
import {TaskEditModalComponent} from './shared/dialogModal/task-edit-modal/task-edit-modal.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {AffectationCollaborateurTaskModalComponent} from './shared/dialogModal/affectation-collaborateur-task-modal/affectation-collaborateur-task-modal.component';
import {DialogService, DynamicDialogModule} from 'primeng/dynamicdialog';
import {TableModule} from 'primeng/table';
import {ButtonModule} from 'primeng/button';
import {ProgressBarModule} from 'primeng/progressbar';
import {ServiceEditModalComponent} from './shared/dialogModal/service-edit-modal/service-edit-modal.component';
import {StepsModule} from 'primeng/steps';
import {GoBackComponent} from './shared/go-back/go-back.component';
import {BackButtonDirective} from './Directives/back-button.directive';
import {NavigationService} from './Services/navigation.service';
import {TaskViewComponent} from './views/collaborateur/task-view/task-view.component';
import {TaskEditComponent} from './views/collaborateur/task-edit/task-edit.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {DragDropModule} from 'primeng/dragdrop';
import {UserEditModalComponent} from './shared/dialogModal/user-edit-modal/user-edit-modal.component';
import {MatChipsModule} from '@angular/material/chips';
import {DialogModule} from 'primeng/dialog';
import {RadioButtonModule} from 'primeng/radiobutton';
import {AccordionModule} from 'primeng/accordion';
import {ListboxModule} from 'primeng/listbox';
import {AffectationEntrepriseUserComponent} from './shared/dialogModal/affectation-entreprise-user/affectation-entreprise-user.component';
import {TimelineModule} from 'primeng/timeline';
import {CardModule} from 'primeng/card';
import {MatTooltipModule} from '@angular/material/tooltip';
import {EntrepriseModule} from './views/entreprise/entreprise.module';
import {ConfirmationService, MessageService} from 'primeng/api';
import {ConnectionServiceModule} from 'ng-connection-service';
import {ConfirmInscriptionComponent} from './views/confirm-inscription/confirm-inscription.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {ToastModule} from 'primeng/toast';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';
import {LoaderPopUpComponent} from './shared/dialogModal/loader-pop-up/loader-pop-up.component';
import {SharedModule} from './shared/shared/shared.module';
import {InputSwitchModule} from 'primeng/inputswitch';
import {DividerModule} from 'primeng/divider';
import {CheckboxModule} from 'primeng/checkbox';
import {InputTextModule} from 'primeng/inputtext';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {InputNumberModule} from "primeng/inputnumber";
import {ToolbarModule} from "primeng/toolbar";
import {FileUploadModule} from "primeng/fileupload";
import {RatingModule} from "primeng/rating";
import {DropdownModule} from "primeng/dropdown";
import {ConfirmDialogModule} from "primeng/confirmdialog";
import {CalendarModule} from "primeng/calendar";
import {DateFormat} from "./shared/shared/DateFormat";
import {
  NGX_MAT_DATE_FORMATS,
  NgxMatDateAdapter, NgxMatDateFormats,
  NgxMatDatetimePickerModule,
  NgxMatNativeDateModule,
  NgxMatTimepickerModule
} from "@angular-material-components/datetime-picker";
import {CustomDateAdapter} from "./shared/shared/CustomDateAdapter";
import {PanelModule} from "primeng/panel";
import {MenuModule} from "primeng/menu";
import {AlertModule} from "ngx-bootstrap/alert";
import {ChartModule} from "primeng/chart";
import {CompteurPanelComponent} from "./shared/component/compteur-panel/compteur-panel.component";
import { SaisiePanelComponent } from './shared/component/saisie-panel/saisie-panel.component';
import { EtapePanelComponent } from './shared/component/etape-panel/etape-panel.component';
import {IndicateurComponent} from "./shared/component/indicateur/indicateur.component";
import {MatCardModule} from "@angular/material/card";
import {CheckListPanelComponent} from "./shared/component/check-list-panel/check-list-panel.component";

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

const APP_CONTAINERS = [
  DefaultLayoutComponent
];
const CUSTOM_DATE_FORMATS: NgxMatDateFormats = {
  parse: {
    dateInput: 'DD/MM/YYYY'
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};
const MY_FORMATS = {
  parse: {
    dateInput: 'DD.MM.YYYY',
  },
  display: {
    dateInput: 'DD.MM.YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'DD.MM.YYYY',
    monthYearA11yLabel: 'MM YYYY',
  },
};
@NgModule({
  imports: [
    HttpClientModule,
    FormsModule,
    CommonModule,
    //BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    MatMomentDateModule,
    AppSidebarModule,
    MatDialogModule,
    AccordionModule,
    ListboxModule,
    RadioButtonModule,
    MatTableModule,
    ProgressSpinnerModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatOptionModule,
    MatInputModule,
    DragDropModule,
    MatButtonModule,
    TableModule,
    DynamicDialogModule,
    ButtonModule,
    ProgressBarModule,
    StepsModule,
    DialogModule,
    MatChipsModule,
    MatPaginatorModule,
    MatSortModule,
    TimelineModule,
    MatTooltipModule,
    CardModule,
    PerfectScrollbarModule,
    EntrepriseModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    IconModule,
    FlexLayoutModule,
    ConnectionServiceModule,
    ToastModule,
    ReactiveFormsModule,
    IconSetModule.forRoot(),
    MatCheckboxModule,
    MatExpansionModule,
    SharedModule,
    InputSwitchModule,
    DividerModule,
    CheckboxModule,
    InputTextModule,
    InputTextareaModule,
    InputNumberModule,
    ToolbarModule,
    FileUploadModule,
    RatingModule,
    DropdownModule,
    ConfirmDialogModule,
    MatNativeDateModule,
    CalendarModule,
    NgxMatDatetimePickerModule,
    NgxMatNativeDateModule,
    NgxMatTimepickerModule,
    PanelModule,
    MenuModule,
    AlertModule,
    ChartModule,
    MatTooltipModule,
    MatCardModule


  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    EntrepriseEditModalComponent,
    ConfirmModalComponent,
    CollaborateurEditModalComponent,
    TaskEditModalComponent,
    AffectationCollaborateurTaskModalComponent,
    ServiceEditModalComponent,
    GoBackComponent,
    TaskViewComponent,
    TaskEditComponent,
    BackButtonDirective,
    UserEditModalComponent,
    AffectationEntrepriseUserComponent,
    ConfirmInscriptionComponent,
    CompteurPanelComponent,
    SaisiePanelComponent,
    EtapePanelComponent,
    IndicateurComponent,
    CheckListPanelComponent

  ],
  exports: [],
  providers: [
    AuthGuard,
    { provide: NgxMatDateAdapter, useClass: CustomDateAdapter},
    { provide: NGX_MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS },
    { provide: DateAdapter, useClass: DateFormat },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    {
      provide: ErrorHandler,
      useClass: AuthErrorHandler
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    IconSetService,
    LoginServiceService,
    EntrepriseserviceService,
    CollaborateurserviceService,
    AdminserviceService,
    DialogService,
    MessageService,
    ConfirmationService,
    NavigationService
  ],
  entryComponents: [EntrepriseEditModalComponent, CollaborateurEditModalComponent, TaskEditModalComponent, AffectationCollaborateurTaskModalComponent, ServiceEditModalComponent, GoBackComponent, UserEditModalComponent, AffectationEntrepriseUserComponent, LoaderPopUpComponent, LoginComponent],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private dateAdapter: DateAdapter<Date>) {
    dateAdapter.setLocale("en-in"); // DD/MM/YYYY
  }
}
