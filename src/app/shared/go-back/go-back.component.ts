import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'go-back',
  templateUrl: './go-back.component.html',
  styleUrls: ['./go-back.component.scss']
})
export class GoBackComponent implements OnInit {

  @Output() gobackEvent = new EventEmitter<string>();

  constructor() {
  }

  ngOnInit(): void {
  }

  goBack() {
    this.gobackEvent.emit();
  }
}
