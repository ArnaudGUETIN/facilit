import {JwtHelperService} from "@auth0/angular-jwt";

export enum RoleEnum {
 roleAdmin = 'ADMIN',
 roleCollaborateur = 'COLLABORATEUR',
 roleEntreprise = 'ENTREPRISE',
 roleCollabFacilit = 'COLLAB_FACILIT',
 roleConsultant = 'CONSULTANT',
 rolePartenaire = 'PARTENAIRE'
}
