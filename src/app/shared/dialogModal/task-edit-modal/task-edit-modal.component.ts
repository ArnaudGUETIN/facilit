import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TaskService} from '../../../Services/task.service';
import {Task} from '../../../Model/Task';
import {CollaborateurserviceService} from '../../../Services/collaborateurservice.service';

@Component({
  selector: 'app-task-edit-modal',
  templateUrl: './task-edit-modal.component.html',
  styleUrls: ['./task-edit-modal.component.scss']
})
export class TaskEditModalComponent implements OnInit, AfterViewInit {
  dateDebut: Date;
  dateFin: Date;
  responsableList: any = [];
  changed: boolean;
  task: Task;
  taskForm: any;
  payLoad = '';
  panelOpenState = false;
  showAccordion: any = false;

  constructor(
    public dialogRef: MatDialogRef<TaskEditModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private taskService: TaskService, private collaborateurService: CollaborateurserviceService) {

  }

  ngAfterViewInit(): void {
    this.changed = false;
    console.log(this.changed);
  }

  ngOnInit(): void {
    this.task = new Task();
    if (this.data.task) {
      this.task = this.task.copy(this.data.task);
    }


    this.taskForm = this.fb.group({
      numero: [this.task.numero, Validators.required],
      designation: [this.task.designation, Validators.required],
      commentaire: [this.task.commentaire, Validators.required],
      instruction: [this.task.instruction, Validators.required],
      dateDebut: [this.dateDebut, Validators.required],
      dateFin: [this.dateFin, Validators.required],
      responsable: [this.task.responsableId, Validators.required],
      nature: [this.task.nature, Validators.required],
      categorie: [this.task.categorie, Validators.required],
      periodicite: ['', Validators.required],


    });
    this.getRespobsables();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onChangeTask() {
    this.changed = true;
  }

  onSubmit() {
    if (this.taskForm.invalid) {
      return;
    }
    this.payLoad = JSON.stringify(this.taskForm.getRawValue());
    console.log(this.payLoad);
    console.log(this.data.entrepriseId);
    if (!this.data.isModify) {
      let task1 = JSON.parse(this.payLoad);
      task1.dateDebut = new Date(JSON.parse(this.payLoad).dateDebut).getTime() / 1000;
      task1.dateFin = new Date(JSON.parse(this.payLoad).dateFin).getTime() / 1000;
      this.taskService.addTask(this.data.entrepriseId, this.task)
        .subscribe(data => {
          console.log(data);
          setTimeout(() => {
            this.dialogRef.close({task: JSON.stringify(data), isModifyPerformed: false});
          }, 1000);

        });
    } else {
      if (this.changed) {
        this.taskService.updateTask(this.task, false)
          .subscribe(data => {
            console.log(data);
            setTimeout(() => {
              this.dialogRef.close({task: JSON.stringify(data), isModifyPerformed: true});
            }, 1000);
          });
      }

    }


  }

  getRespobsables() {
    this.collaborateurService.getAllResponsable(0, this.data.entrepriseId)
      .subscribe(data => {
        console.log(data);
        this.responsableList = data._embedded.collaborateurs;
      });
  }
}
