import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Entreprise} from '../../../Model/Entreprise';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EntrepriseserviceService} from '../../../Services/entrepriseservice.service';

@Component({
  selector: 'app-entreprise-edit-modal',
  templateUrl: './entreprise-edit-modal.component.html',
  styleUrls: ['./entreprise-edit-modal.component.scss']
})
export class EntrepriseEditModalComponent implements OnInit, AfterViewInit {
  changed: boolean;
  entreprise: Entreprise;
  entrepriseForm: any;
  payLoad = '';

  constructor(
    public dialogRef: MatDialogRef<EntrepriseEditModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private entrepriseService: EntrepriseserviceService) {

  }

  ngAfterViewInit(): void {
    this.changed = false;
    console.log(this.changed);
  }

  ngOnInit(): void {
    this.entreprise = new Entreprise();
    if (this.data.entreprise && this.data.entreprise.entrepriseId) {
      this.entreprise = this.entreprise.copy(this.data.entreprise);
    }


    this.entrepriseForm = this.fb.group({
      emailContact: [this.entreprise.emailContact, Validators.required],
      code: [this.entreprise.code, Validators.required],
      nomCommercial: [this.entreprise.nomCommercial, Validators.required],
      siret: [this.entreprise.siret, Validators.required],
      type: [this.entreprise.type, Validators.required],
      telephone: [this.entreprise.telephone, Validators.required],
      ville: [this.entreprise.ville, Validators.required],
      codePostal: [this.entreprise.codePostal, Validators.required],

    });

    this.entrepriseForm.statusChanges
      .subscribe(status => {
        console.log(status);
        // this.changed = true;
        //  console.log('object changed : ' + this.changed);
      });

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onChangeEntreprise() {
    this.changed = true;
    console.log('object changed : ' + this.changed);
  }

  onSubmit() {
    if (this.entrepriseForm.invalid) {
      return;
    }
    this.payLoad = JSON.stringify(this.entrepriseForm.getRawValue());
    if (!this.data.isModify) {
      this.entrepriseService.addEntreprise(JSON.parse(this.payLoad))
        .subscribe(data => {
          console.log(data);
          setTimeout(() => {
            this.dialogRef.close({entreprise: JSON.stringify(data), isModifyPerformed: false});
          }, 1000);

        });
    } else {
      if (this.changed) {
        this.entrepriseService.updateEntreprise(this.entreprise)
          .subscribe(data => {
            console.log(data);
            setTimeout(() => {
              this.dialogRef.close({entreprise: JSON.stringify(data), isModifyPerformed: true});
            }, 1000);
          });
      }

    }


  }


}
