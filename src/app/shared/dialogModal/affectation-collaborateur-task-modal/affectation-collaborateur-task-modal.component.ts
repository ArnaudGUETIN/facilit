import {Component, OnInit} from '@angular/core';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {CollaborateurserviceService} from '../../../Services/collaborateurservice.service';
import {Affectation} from '../../../Model/Affectation';
import {AffectationServiceService} from '../../../Services/affectation-service.service';

@Component({
  selector: 'app-affectation-collaborateur-task-modal',
  templateUrl: './affectation-collaborateur-task-modal.component.html',
  styleUrls: ['./affectation-collaborateur-task-modal.component.scss']
})
export class AffectationCollaborateurTaskModalComponent implements OnInit {
  collaborateurList: any = [];
  products: Product[];
  entrepriseId = 0;
  taskId = 0;
  affectation: any = new Affectation();

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private collaborateurService: CollaborateurserviceService, private affectationService: AffectationServiceService) {
    this.entrepriseId = this.config.data?.entrepriseId;
    this.taskId = this.config.data?.taskId;
  }

  ngOnInit(): void {
    // this.products =
    //   [
    //     { name: '1', price: 'kiran',quantity:'kiran@gmail.com',inventoryStatus:'kiran@gmail.com',image:'kiran@gmail.com' },
    //     { name: '2', price: 'kiran',quantity:'kiran@gmail.com',inventoryStatus:'kiran@gmail.com',image:'kiran@gmail.com' },
    //     { name: '3', price: 'kiran',quantity:'kiran@gmail.com',inventoryStatus:'kiran@gmail.com',image:'kiran@gmail.com' },
    //     { name: '4', price: 'kiran',quantity:'kiran@gmail.com',inventoryStatus:'kiran@gmail.com',image:'kiran@gmail.com' },
    //
    //
    //   ];

    this.getAllCollaborateurs(this.entrepriseId);

  }

  getAllCollaborateurs(entrepriseId: number) {
    this.collaborateurService.getAllEntrepriseCollaborateurs(entrepriseId, this.taskId, -1)
      .subscribe(data => {
        console.log(data);
        this.collaborateurList = data;

      });
  }

  selectCollab(collabId: number) {
    this.affectation.collaborateurId = collabId;
    this.affectation.taskId = this.taskId;
    this.affectation.statut = 0;
    this.affectationService.addAffectations(this.affectation)
      .subscribe(data => {
        this.affectation = data;
        this.closeModal(true);
      });
  }

  deleteAffectation(collabId: number) {
    this.affectation.collaborateurId = collabId;
    this.affectation.taskId = this.taskId;
    this.affectationService.deleteAffectations(this.affectation)
      .subscribe(data => {
        this.closeModal(true);
      });
  }

  closeModal(isModify:boolean) {
    this.ref.close({affection:this.affectation,isModify:isModify});
  }
}

export interface Product {

  name;
  price;
  quantity;
  inventoryStatus;
  image;
}
