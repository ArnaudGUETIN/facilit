import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AffectationCollaborateurTaskModalComponent} from './affectation-collaborateur-task-modal.component';
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from "primeng/dynamicdialog";
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {DialogModule} from "primeng/dialog";

describe('AffectationCollaborateurTaskModalComponent', () => {
  let component: AffectationCollaborateurTaskModalComponent;
  let fixture: ComponentFixture<AffectationCollaborateurTaskModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AffectationCollaborateurTaskModalComponent],
      imports: [ReactiveFormsModule,FormsModule,HttpClientModule,RouterModule.forRoot([]),DialogModule],
      providers:[DialogService,{
        provide: DynamicDialogRef,
        useValue: {}
      },{
        provide: DynamicDialogConfig,
        useValue: {}
      }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectationCollaborateurTaskModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
