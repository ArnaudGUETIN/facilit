import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-loader-pop-up',
  templateUrl: './loader-pop-up.component.html',
  styleUrls: ['./loader-pop-up.component.scss']
})
export class LoaderPopUpComponent implements OnInit {
  @Input() showLoader: boolean;

  constructor() {
  }

  ngOnInit(): void {
  }

}
