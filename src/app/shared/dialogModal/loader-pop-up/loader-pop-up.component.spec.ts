import {ComponentFixture, TestBed} from '@angular/core/testing';

import {LoaderPopUpComponent} from './loader-pop-up.component';

describe('LoaderPopUpComponent', () => {
  let component: LoaderPopUpComponent;
  let fixture: ComponentFixture<LoaderPopUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LoaderPopUpComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoaderPopUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
