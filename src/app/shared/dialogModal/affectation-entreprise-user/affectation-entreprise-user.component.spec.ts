import {ComponentFixture, TestBed} from '@angular/core/testing';

import {AffectationEntrepriseUserComponent} from './affectation-entreprise-user.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {DialogService, DynamicDialogConfig, DynamicDialogRef} from "primeng/dynamicdialog";
import {DialogModule} from "primeng/dialog";

describe('AffectationEntrepriseUserComponent', () => {
  let component: AffectationEntrepriseUserComponent;
  let fixture: ComponentFixture<AffectationEntrepriseUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AffectationEntrepriseUserComponent],
      imports: [ReactiveFormsModule,FormsModule,HttpClientModule,RouterModule.forRoot([]),MatDialogModule,DialogModule],
      providers:[DialogService,{
        provide: DynamicDialogRef,
        useValue: {}
      },{
        provide: DynamicDialogConfig,
        useValue: {}
      }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectationEntrepriseUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
