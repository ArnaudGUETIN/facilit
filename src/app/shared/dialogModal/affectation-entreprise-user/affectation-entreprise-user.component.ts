import {Component, OnInit} from '@angular/core';
import {DynamicDialogConfig, DynamicDialogRef} from 'primeng/dynamicdialog';
import {EntrepriseserviceService} from '../../../Services/entrepriseservice.service';

@Component({
  selector: 'app-affectation-entreprise-user',
  templateUrl: './affectation-entreprise-user.component.html',
  styleUrls: ['./affectation-entreprise-user.component.scss']
})
export class AffectationEntrepriseUserComponent implements OnInit {

  entrepriseList: any = [];
  entrepriseId = 0;
  userId = 0;
  ids=[];

  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig, private entrepriseService: EntrepriseserviceService) {
    this.userId = this.config.data?.userId;
    this.ids = this.config.data?.ids;
  }

  ngOnInit(): void {
    this.getEntreprises();
  }

  getEntreprises() {
    this.entrepriseService.getAllEntreprises()
      .subscribe(data => {
        console.log(data);
        this.entrepriseList = data;
        for(let ent of this.entrepriseList){

          if(this.ids.includes(ent.entrepriseId)){
            ent.affected = true;
          }else{
            ent.affected = false;
          }
        }
      });
  }

  selectCollab(entId: number) {
    this.entrepriseService.addMannagerToEntreprise(entId, this.userId)
      .subscribe(data => {
        console.log(data);
        this.ref.close(data);
      });
  }
}
