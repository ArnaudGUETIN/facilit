import {Component, Inject, OnInit} from '@angular/core';
import {User} from '../../../Model/User';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, Validators} from '@angular/forms';
import {UserService} from '../../../Services/user.service';

@Component({
  selector: 'app-user-edit-modal',
  templateUrl: './user-edit-modal.component.html',
  styleUrls: ['./user-edit-modal.component.scss']
})
export class UserEditModalComponent implements OnInit {
  roleList: any = [];
  changed: boolean;
  user: User;
  userForm: any;
  payLoad = '';

  constructor(public dialogRef: MatDialogRef<UserEditModalComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private userService: UserService) {
  }

  ngOnInit(): void {
    this.user = new User();
    if (this.data.user) {
      this.user = this.user.copy(this.data.user);
    }


    this.userForm = this.fb.group({
      nom: [this.user.nom, Validators.required],
      prenom: [this.user.prenom, Validators.required],
      email: [this.user.email, Validators.required],
      username: [this.user.username, Validators.required],
      role: [this.user.currentRoleId, Validators.required],

    });
    this.getRoles();
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getRoles() {
    this.userService.getAllRoles()
      .subscribe(data => {
        console.log(data);
        this.roleList = data._embedded.appRoles;
      });
  }

  onSubmit() {
    if (this.userForm.invalid) {
      return;
    }
    this.payLoad = JSON.stringify(this.userForm.getRawValue());
    console.log(this.payLoad);
    if (!this.data.isModify) {
      let service1 = JSON.parse(this.payLoad);
      this.userService.addUser(this.user)
        .subscribe(data => {
          console.log(data);
          setTimeout(() => {
            this.dialogRef.close({user: JSON.stringify(data), isModifyPerformed: false});
          }, 1000);

        });
    } else {

      this.userService.updateUser(this.user)
        .subscribe(data => {
          console.log(data);
          setTimeout(() => {
            this.dialogRef.close({user: JSON.stringify(data), isModifyPerformed: true});
          }, 1000);
        });


    }


  }
}
