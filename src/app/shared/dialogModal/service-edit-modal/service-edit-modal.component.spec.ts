import {ComponentFixture, TestBed} from '@angular/core/testing';

import {ServiceEditModalComponent} from './service-edit-modal.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {DialogService} from "primeng/dynamicdialog";

describe('ServiceEditModalComponent', () => {
  let component: ServiceEditModalComponent;
  let fixture: ComponentFixture<ServiceEditModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ServiceEditModalComponent],
      imports: [ReactiveFormsModule,FormsModule,HttpClientModule,RouterModule.forRoot([]),MatDialogModule],
      providers:[DialogService,{
        provide: MatDialogRef,
        useValue: {}
      },{
        provide: MAT_DIALOG_DATA,
        useValue: {}
      }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
