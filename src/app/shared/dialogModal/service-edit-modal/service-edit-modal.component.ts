import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {DepartementServiceService} from '../../../Services/departement-service.service';
import {Service} from '../../../Model/Service';
import {CollaborateurserviceService} from '../../../Services/collaborateurservice.service';

@Component({
  selector: 'app-service-edit-modal',
  templateUrl: './service-edit-modal.component.html',
  styleUrls: ['./service-edit-modal.component.scss']
})
export class ServiceEditModalComponent implements OnInit, AfterViewInit {
  dateDebut: Date;
  dateFin: Date;
  responsableList: any = [];
  changed: boolean;
  service: Service;
  serviceForm: any;
  payLoad = '';

  constructor(
    public dialogRef: MatDialogRef<ServiceEditModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private departementService: DepartementServiceService, private collaborateurService: CollaborateurserviceService) {

  }

  ngAfterViewInit(): void {
    this.changed = false;
    console.log(this.changed);
  }

  ngOnInit(): void {
    this.service = new Service();
    if (this.data.service) {
      this.service = this.service.copy(this.data.service);
    }


    this.serviceForm = this.fb.group({
      code: [this.service.code, Validators.required],
      designation: [this.service.designation, Validators.required],
      responsable: [this.service.responsableId, Validators.required],

    });
    this.getRespobsables();

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onChangeService() {
    this.changed = true;
    console.log('object changed : ' + this.changed);
  }

  getRespobsables() {
    this.collaborateurService.getAllResponsable(0, this.data.entrepriseId)
      .subscribe(data => {
        console.log(data);
        this.responsableList = data._embedded.collaborateurs;
      });
  }

  onSubmit() {
    if (this.serviceForm.invalid) {
      return;
    }
    this.payLoad = JSON.stringify(this.serviceForm.getRawValue());
    console.log(this.payLoad);
    console.log(this.data.entrepriseId);
    if (!this.data.isModify) {
      let service1 = JSON.parse(this.payLoad);
      this.departementService.addService(this.data.entrepriseId, this.service)
        .subscribe(data => {
          console.log(data);
          setTimeout(() => {
            this.dialogRef.close({service: JSON.stringify(data), isModifyPerformed: false});
          }, 1000);

        });
    } else {
      if (this.changed) {
        this.service.entrepriseId = this.data.entrepriseId;
        this.departementService.updateService(this.service)
          .subscribe(data => {
            console.log(data);
            setTimeout(() => {
              this.dialogRef.close({service: JSON.stringify(data), isModifyPerformed: true});
            }, 1000);
          });
      }

    }


  }

}
