import {AfterViewInit, Component, Inject, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Collaborateur} from '../../../Model/Collaborateur';
import {CollaborateurserviceService} from '../../../Services/collaborateurservice.service';
import {UserService} from "../../../Services/user.service";
import {MatTableDataSource} from "@angular/material/table";
import {DepartementServiceService} from "../../../Services/departement-service.service";
import {Service} from "../../../Model/Service";

@Component({
  selector: 'app-collaborateur-edit-modal',
  templateUrl: './collaborateur-edit-modal.component.html',
  styleUrls: ['./collaborateur-edit-modal.component.scss']
})
export class CollaborateurEditModalComponent implements OnInit, AfterViewInit {

  fonctionList: any = [
    {label: 'EMPLOYE', code: 0},
    {label: 'CHEF DE SERVICE', code: 1},
    {label: 'DIRIGEANT', code: 2},
  ];
  changed: boolean;
  collaborateur: Collaborateur;
  entrepriseId: number;
  collaborateurForm: any;
  payLoad = '';
  showLoader: any;
  alreadyExist: any = false;
  serviceList: Service[]= [];

  constructor(
    public dialogRef: MatDialogRef<CollaborateurEditModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private  departmentService:DepartementServiceService
    , private collaborateurService: CollaborateurserviceService,private userService:UserService) {

  }

  ngAfterViewInit(): void {
    this.changed = false;
    console.log(this.changed);
  }

  ngOnInit(): void {
    this.collaborateur = new Collaborateur();
    if (this.data.collaborateur && this.data.collaborateur.collaborateurId) {
      this.collaborateur = this.collaborateur.copy(this.data.collaborateur);
    }
    this.entrepriseId = this.data.entrepriseId;
    this.getDepartements(this.entrepriseId);

    this.collaborateurForm = this.fb.group({
      email: [this.collaborateur.email, [Validators.email,emailExistValidator(this.alreadyExist),Validators.required]],
      nom: [this.collaborateur.nom, Validators.required],
      prenom: [this.collaborateur.prenom, Validators.required,],
      niveauHierarchique: [this.collaborateur.niveauHierarchique, Validators.required],
      profil: [this.collaborateur.profil, Validators.required],
      telephone: [this.collaborateur.telephone, Validators.required],
      serviceIds: [this.collaborateur.serviceIds],

    },{updateOn: 'submit'});

/*    const emailControl = <FormControl>this.collaborateurForm.get('email');
    emailControl.valueChanges.subscribe(value=>{
      this.userService.userEmailAlreadyExist(value)
        .subscribe(data=>{
          this.alreadyExist = data;
          console.log(this.alreadyExist)
          emailControl.clearValidators();
          emailControl.setValidators([Validators.email,emailExistValidator(this.alreadyExist),Validators.required]);
        })
    })*/

  }

  onNoClick(): void {
    console.log(this.collaborateurForm);
    this.dialogRef.close();
  }

  onChangeEntreprise() {
    this.changed = true;
    console.log('object changed : ' + this.collaborateur.niveauHierarchique);
  }

  onSubmit() {
    this.userService.userEmailAlreadyExist(this.collaborateurForm.get('email').value)
      .subscribe(data=>{
        this.alreadyExist = data;
        if(this.alreadyExist){
          this.collaborateurForm.get('email').setErrors({alreadyExist:true})
        }
      })
    if (this.collaborateurForm.invalid) {
      return;
    }
    this.showLoader = true;
    this.payLoad = JSON.stringify(this.collaborateurForm.getRawValue());
    if (!this.data.isModify) {
      this.collaborateurService.addCollaborateur(this.entrepriseId, JSON.parse(this.payLoad))
        .subscribe(data => {
          this.showLoader = false;
          setTimeout(() => {
            this.dialogRef.close({collaborateur: JSON.stringify(data), isModifyPerformed: false});
          }, 1000);

        });
    } else {
      if (this.changed) {
        this.collaborateurService.updateCollaborateur(this.collaborateur)
          .subscribe(data => {
            this.showLoader = false;
            setTimeout(() => {
              this.dialogRef.close({collaborateur: JSON.stringify(data), isModifyPerformed: true});
            }, 1000);
          });
      }

    }


  }

   getDepartements(entrepriseId:number){
     this.departmentService.getAllEntrepriseServices(entrepriseId)
       .subscribe(data => {
         console.log(data);
         if (data) {
           this.serviceList = data._embedded.services;
         }

       });
   }

}
export function emailExistValidator(alreadyExist:boolean): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {

      return alreadyExist  ? {alreadyExist: {alreadyExist: control.value}} : null;

  };
}
