import {ComponentFixture, TestBed} from '@angular/core/testing';

import {CollaborateurEditModalComponent} from './collaborateur-edit-modal.component';
import {MAT_DIALOG_DATA, MatDialogModule, MatDialogRef} from "@angular/material/dialog";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";
import {DialogService} from "primeng/dynamicdialog";

describe('CollaborateurEditModalComponent', () => {
  let component: CollaborateurEditModalComponent;
  let fixture: ComponentFixture<CollaborateurEditModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CollaborateurEditModalComponent],
      imports: [ReactiveFormsModule,FormsModule,HttpClientModule,RouterModule.forRoot([]),MatDialogModule],
      providers:[DialogService,{
        provide: MatDialogRef,
        useValue: {}
      },{
        provide: MAT_DIALOG_DATA,
        useValue: {}
      }]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CollaborateurEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
