import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-etape-panel',
  templateUrl: './etape-panel.component.html',
  styleUrls: ['./etape-panel.component.scss']
})
export class EtapePanelComponent implements OnInit {
  @Input()task: any;
  @Input()etapeListReady: any;
  @Input()steps: any;
  @Input()activeIndex: any;
  @Input()etape: any;
  @Output()removeLastStepEmiter:EventEmitter<any> = new EventEmitter;
  @Output()addStepEmiter:EventEmitter<any> = new EventEmitter;
  @Output()updateStepEmiter:EventEmitter<any> = new EventEmitter;
  constructor() { }

  ngOnInit(): void {
  }

  removeLastStep() {
    this.removeLastStepEmiter.emit();
  }

  addStep() {
    this.addStepEmiter.emit();
  }

  updateStep(etape: any) {
    this.updateStepEmiter.emit(etape);
  }
}
