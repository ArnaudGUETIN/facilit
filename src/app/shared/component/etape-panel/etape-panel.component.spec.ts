import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EtapePanelComponent } from './etape-panel.component';

describe('EtapePanelComponent', () => {
  let component: EtapePanelComponent;
  let fixture: ComponentFixture<EtapePanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EtapePanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EtapePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
