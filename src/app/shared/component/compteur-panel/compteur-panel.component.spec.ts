import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompteurPanelComponent } from './compteur-panel.component';

describe('CompteurPanelComponent', () => {
  let component: CompteurPanelComponent;
  let fixture: ComponentFixture<CompteurPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompteurPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompteurPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
