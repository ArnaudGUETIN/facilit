import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-compteur-panel',
  templateUrl: './compteur-panel.component.html',
  styleUrls: ['./compteur-panel.component.scss']
})
export class CompteurPanelComponent implements OnInit {
  @Input()task: any;
  @Input()itemsCompteur: any;
  @Input()taskCompteurList: any;
  @Input()compteurForm: any;
  @Input()relevesList: any;
  @Input()annuelData: any;
  @Input()stackedData: any;
  @Output() showRelevesEventEmiter: EventEmitter<any> = new EventEmitter; //
  @Output() onSubmitEmiter: EventEmitter<any> = new EventEmitter; //
  constructor() { }

  ngOnInit(): void {
  }

  showReleves() {
    this.showRelevesEventEmiter.emit();
  }

  onSubmit() {
    this.onSubmitEmiter.emit();
  }
}
