import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicateurComponent } from './indicateur.component';
import {HttpClientModule} from "@angular/common/http";

describe('IndicateurComponent', () => {
  let component: IndicateurComponent;
  let fixture: ComponentFixture<IndicateurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IndicateurComponent ],
      imports: [ HttpClientModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
