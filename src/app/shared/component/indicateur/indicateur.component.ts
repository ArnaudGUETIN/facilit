import { Component, OnInit } from '@angular/core';
import {environment} from "../../../../environments/environment";
import {EntrepriseserviceService} from "../../../Services/entrepriseservice.service";
import {ChartDataModel} from "../../../views/entreprise/entreprisedashboard/entreprisedashboard.component";
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
@Component({
  selector: 'app-indicateur',
  templateUrl: './indicateur.component.html',
  styleUrls: ['./indicateur.component.scss']
})
export class IndicateurComponent implements OnInit {
  public barChart2Data: Array<any> = [
    {
      data: [7, 18, 9],
      label: '2019',
      barPercentage: 0.6,
      backgroundColor: 'rgba(200, 120, 14, 1)'
    },
    {
      data: [ 19, 2, 7],
      label: '2020',
      barPercentage: 0.6,
      backgroundColor: 'rgba(255, 99, 132, 1)'
    }
  ];
  public barChart2Labels: Array<any> = ['Location', 'Emplacement', 'Inc.'];
  public barChart2Options: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips
    },
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        display: true,
      }],
      yAxes: [{
        display: true,
        ticks: {
          beginAtZero: true,
        }
      }]
    },
    legend: {
      display: true
    }
  };
  public barChart2Colours: Array<any> = [
    {
      backgroundColor: 'rgba(200, 120, 14, 1)',
      borderWidth: 0
    }
  ];
  public barChart2Legend = true;
  public barChart2Type = 'bar';
  // Pie
  pieChartLabels: string[] = ['Terminé', 'En cours', 'Affectées'];
  pieChartData: number[] = [300, 500, 100];
  pieChartType = 'pie';
  isChartsReady = false;
  lineChartCA : ChartDataModel = {
    libelle:'',
    rate:'25%',
    diff:'+0',
    value:0,
    data:[],
    labels:[],
    options:environment.lineChartDefaultOption,
    colors:[{
      backgroundColor: 'transparent',
      borderColor: 'rgba(255,255,255,.55)',
      borderWidth: 3
    }],
    legend:true,
    type:'line'
  };
  lineChartNbBooking : ChartDataModel = {
    libelle:'',
    rate:'15%',
    diff:'+41',
    value:0,
    data:[],
    labels:[],
    options:environment.lineChartDefaultOption,
    colors:[{
      backgroundColor: 'transparent',
      borderColor: 'rgba(255,255,255,.55)',
      borderWidth: 3
    }],
    legend:true,
    type:'line'
  };
  constructor(private entrepriseService: EntrepriseserviceService) { }

  ngOnInit(): void {
    this.isChartsReady = true;
    this.fillChart();
  }


  fillChart(){
    this.entrepriseService.getChartData().subscribe(data=>{
      let payloadCAN = [];
      let payloadCAN1 = [];
      let payloadCAN2 = [];
      let payloadBookingN = [];
      let payloadBookingN1 = [];
      for(let i=0; i<12; i++){
        payloadCAN.push(data[0].ProductList[0].BookingWindowMonthData[i].CaN);
        payloadCAN1.push(data[0].ProductList[0].BookingWindowMonthData[i].CaN1);
        payloadCAN2.push(data[0].ProductList[0].BookingWindowMonthData[i].CaN2);
        payloadBookingN.push(data[0].ProductList[0].BookingWindowMonthData[i].NbBookingN);
        payloadBookingN1.push(data[0].ProductList[0].BookingWindowMonthData[i].NbBookingN1);
      }
      this.lineChartCA.libelle = "CHIFFRE D'AFFAIRES TTC";
      this.lineChartCA.value = "1.890,65 €";
      this.lineChartCA.data =[{data:payloadCAN,label:new Date().getFullYear()},
        {data:payloadCAN1,label:new Date().getFullYear()-1,borderColor: 'rgba(255, 99, 132, 1)',backgroundColor: 'transparent'},
        {data:payloadCAN2,label:new Date().getFullYear()-2,borderColor: 'rgba(25, 45, 99, 1)',backgroundColor: 'transparent'}
      ];
      this.lineChartCA.labels =environment.dateShortLabels;
      this.lineChartCA.options['legend']= { display: true   };

      this.lineChartNbBooking.libelle = "NB de réservation";
      this.lineChartNbBooking.value = "1.474 ";
      this.lineChartNbBooking.data = [{data:payloadBookingN,label:new Date().getFullYear()},{data:payloadBookingN1,label:new Date().getFullYear()-1,borderColor: 'rgba(25, 99, 132, 1)',backgroundColor: 'transparent'}];
      this.lineChartNbBooking.labels =environment.dateShortLabels;
      this.lineChartNbBooking.options['legend'] = { display: true };
      setTimeout(() =>{
        this.isChartsReady = false;
        console.log(this.lineChartCA.options['legend'])
      },1000)

    })
  }
}
