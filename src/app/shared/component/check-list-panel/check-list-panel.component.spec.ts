import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckListPanelComponent } from './check-list-panel.component';

describe('CheckListPanelComponent', () => {
  let component: CheckListPanelComponent;
  let fixture: ComponentFixture<CheckListPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckListPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckListPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
