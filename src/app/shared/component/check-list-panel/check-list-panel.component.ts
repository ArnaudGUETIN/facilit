import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Etape} from "../../../Model/Etape";

@Component({
  selector: 'app-check-list-panel',
  templateUrl: './check-list-panel.component.html',
  styleUrls: ['./check-list-panel.component.scss']
})
export class CheckListPanelComponent implements OnInit {
  @Input()task: any;
  @Input()etapeList: any;
  @Output()addCheckEmiter:EventEmitter<any> = new EventEmitter();
  @Output()removeLastCheckEmiter:EventEmitter<any> = new EventEmitter();
  @Output()updateCheckEmiter:EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  addCheck(isUpdate: boolean, etape?: Etape) {
    // @ts-ignore
    this.addCheckEmiter.emit({isUpdate:isUpdate,step:etape});
  }

  removeLastCheck() {
    this.removeLastCheckEmiter.emit();
  }

  updateCheck(step: any, b: boolean) {
    this.updateCheckEmiter.emit({step:step,b:b});
  }
}
