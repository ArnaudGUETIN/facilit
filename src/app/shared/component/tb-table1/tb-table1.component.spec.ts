import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TbTable1Component } from './tb-table1.component';

describe('TbTable1Component', () => {
  let component: TbTable1Component;
  let fixture: ComponentFixture<TbTable1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TbTable1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TbTable1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
