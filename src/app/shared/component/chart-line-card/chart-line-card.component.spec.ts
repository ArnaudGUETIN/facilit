import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartLineCardComponent } from './chart-line-card.component';

describe('ChartLineCardComponent', () => {
  let component: ChartLineCardComponent;
  let fixture: ComponentFixture<ChartLineCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChartLineCardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartLineCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
