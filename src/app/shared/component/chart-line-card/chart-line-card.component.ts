import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-chart-line-card',
  templateUrl: './chart-line-card.component.html',
  styleUrls: ['./chart-line-card.component.scss']
})
export class ChartLineCardComponent implements OnInit {
  @Input()lineChart: any;
  @Input()bgclass: string;
  @Input()height: any = '150px';

  constructor() { }

  ngOnInit(): void {
  }

}
