import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-saisie-panel',
  templateUrl: './saisie-panel.component.html',
  styleUrls: ['./saisie-panel.component.scss']
})
export class SaisiePanelComponent implements OnInit {
  @Input()task: any;
  @Input()items: any;
  @Input()saisieCurrentDate: any;
  @Input()saisieList: any;
  @Input()collaborateurList: any;
  @Output()getAllSaisieEmiter: EventEmitter<any> = new EventEmitter;

  constructor() { }

  ngOnInit(): void {
  }

  getAllSaisie() {
    this.getAllSaisieEmiter.emit();
  }
}
