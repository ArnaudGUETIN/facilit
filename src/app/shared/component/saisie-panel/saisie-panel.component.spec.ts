import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisiePanelComponent } from './saisie-panel.component';

describe('SaisiePanelComponent', () => {
  let component: SaisiePanelComponent;
  let fixture: ComponentFixture<SaisiePanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaisiePanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisiePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
