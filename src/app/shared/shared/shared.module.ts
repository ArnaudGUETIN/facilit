import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoaderPopUpComponent} from '../dialogModal/loader-pop-up/loader-pop-up.component';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {DialogModule} from 'primeng/dialog';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {ParametrageComponent} from '../component/parametrage/parametrage.component';
import {ChartLineCardComponent} from "../component/chart-line-card/chart-line-card.component";
import {ChartsModule} from "ng2-charts";
import {IndicateurComponent} from "../component/indicateur/indicateur.component";


@NgModule({
  declarations: [LoaderPopUpComponent, ParametrageComponent,
    ChartLineCardComponent],
  imports: [
    CommonModule,
    DynamicDialogModule,
    DialogModule,
    ChartsModule,
    ProgressSpinnerModule,
  ],
  exports: [LoaderPopUpComponent, ParametrageComponent,ChartLineCardComponent]
})
export class SharedModule {
}
