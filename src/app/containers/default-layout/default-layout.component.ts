import {Component, ElementRef, HostListener, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {CollaborateurserviceService} from '../../Services/collaborateurservice.service';
import {Collaborateur} from '../../Model/Collaborateur';
import {NavigationService} from '../../Services/navigation.service';
import {EntrepriseserviceService} from '../../Services/entrepriseservice.service';
import {Entreprise} from '../../Model/Entreprise';
import {AlerteService} from "../../Services/alerte.service";
import {ViewportScroller} from "@angular/common";

const roleAdmin = 'ADMIN';
const roleCollaborateur = 'COLLABORATEUR';
const roleEntreprise = 'ENTREPRISE';
const roleConsultant = 'CONSULTANT';
const rolePartenaire = 'PARTENAIRE';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html',
  styles: ['.breadcrumb {\n' +
  '    display: flex;\n' +
  '    flex-wrap: wrap;\n' +
  '    padding: 0.75rem 1rem;\n' +
  '    margin-bottom: 1.5rem;\n' +
  '    list-style: none;\n' +
  '    background-color: #fff;\n' +
  '    border-radius: 0;\n' +
  '    justify-content: space-between;\n' +
  '}'+
  '.fab:focus {\n' +
  '    outline: 1px dotted !important;\n' +

  '}']
})
export class DefaultLayoutComponent implements OnInit {
  role = localStorage.getItem('appRole');
  pictureUrl = JSON.parse(localStorage.getItem('pictureUrl'));
  public navItems = [];//navItems;
  private collaborateur: Collaborateur;
  private entreprise: Entreprise;
  alerteLength = 0;
  pageYoffset = 0;
  @ViewChild('body') body: ElementRef;
  @HostListener('window:scroll', ['$event']) onScroll(event){
    this.pageYoffset = window.pageYOffset;
  }
  ngOnInit(): void {
    this.entrepriseService.CURRENT_ENTREPRISE.subscribe(FILTER => {
      this.entreprise = FILTER;
    });
    if(!this.pictureUrl){
      this.pictureUrl="assets/img/brand/logo.png";
    }
    this.initObservable();
  }
  public sidebarMinimized = false;

  constructor(private route: Router, private collaborateurService: CollaborateurserviceService, private navigation: NavigationService, private router: Router, private entrepriseService: EntrepriseserviceService,
              private alerteService:AlerteService,private scroll: ViewportScroller) {


  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  logOut() {

    localStorage.removeItem('collaborateur');
    localStorage.removeItem('collaborateurId');
    localStorage.removeItem('entreprise');
    localStorage.removeItem('entrepriseId');
    localStorage.removeItem('token');
    localStorage.removeItem('username');
    localStorage.removeItem('appRole');
    // this.loginService.IS_ADMIN.next(false);

    // this.loginService.IS_RESTAURANT.next(false);
    this.goToLogin();
  }

  goToLogin() {
    this.route.navigate(['login']);
  }

  goBackNav(): void {
    this.navigation.back();
  }

  initObservable() {
     this.initAdminNav();
      //this.navItems = [...this.navItems];
      //this.router.navigate(['admin']);
    // } else if (this.role == roleCollaborateur) {
      this.collaborateurService.CURRENT_COLLABORATEUR.subscribe(FILTER => {
        this.collaborateur = FILTER;
        this.navItems = [];
        this.initAdminNav();
        this.initEntrepriseNav();
        this.initCollabNav();
      });


      this.entrepriseService.CURRENT_ENTREPRISE.subscribe(FILTER => {
        this.entreprise = FILTER;
        this.navItems = [];
        this.initAdminNav();
        this.initEntrepriseNav();
      });

    this.alerteService.getAllUserAlertes(localStorage.getItem('username'))
      .subscribe(data=>{
        this.alerteLength = data.length
      })


  }

  private initEntrepriseNav() {
    if (!this.entreprise) {
      this.entreprise = JSON.parse(localStorage.getItem('entreprise'));
    }
    if (this.entreprise) {
      this.navItems.push({
        name: this.entreprise.nomCommercial,
        url: '/entreprise/' + this.entreprise.entrepriseId,
        icon: 'cil-home',
      });
      this.navItems.push({
        name: 'Profile',
        url: 'entreprise/' + this.entreprise.entrepriseId + '/editEntreprise',
        icon: 'cil-description',
        linkProps: {
          queryParams: {
            'entrepriseId': this.entreprise.entrepriseId,
            'entrepriseType': this.entreprise.entrepriseType
          }
        }
      });
      this.navItems.push({
        name: 'Collaborateurs',
        url: 'entreprise/' + this.entreprise.entrepriseId,
        icon: 'cil-people',
        linkProps: {queryParams: {'selectedIndex': 0}}
      });
      this.navItems.push({
        name: 'Tâches',
        url: 'entreprise/' + this.entreprise.entrepriseId,
        icon: 'cil-list',
        linkProps: {queryParams: {'selectedIndex': 1}}
      });
      this.navItems.push({
        name: 'Services',
        url: 'entreprise/' + this.entreprise.entrepriseId,
        icon: 'cil-layers',
        linkProps: {queryParams: {'selectedIndex': 2}}
      });
      this.navItems.push({
        name: 'Parametrage',
        url: '/entreprise/' + this.entreprise.entrepriseId + '/parametrage',
        icon: 'cil-settings',
      });
    }
  }

  private initCollabNav() {
    if (!this.collaborateur) {
      this.collaborateur = JSON.parse(localStorage.getItem('collaborateur'));
    }
    if (this.collaborateur) {
      this.navItems.push({
        name: this.collaborateur?.nom + ' ' + this.collaborateur?.prenom,
        url: '/collaborateur',
        icon: 'cil-home',
      });
      this.navItems.push({
        name: 'Mes tâches',
        url: 'collaborateur/tasks',
        icon: 'cil-list',
        linkProps: {queryParams: {}}
      });
      this.navItems.push({
        name: 'Mes indicateurs',
        url: 'entreprise/' + this.entreprise.entrepriseId,
        icon: 'cil-graph',
        linkProps: {queryParams: {'selectedIndex': 1}}
      });

    }
  }

  private initAdminNav() {
    if (this.role == roleAdmin) {
      this.navItems.push({
        name: 'ADMINISTRATEUR',
        url: '/admin',
        icon: 'cil-home',
      })
      this.navItems.push({
        name: 'Utilisateurs',
        url: '/admin/users',
        icon: 'cil-group',
      });
      this.navItems.push({
        name: 'Parametrage',
        url: '/admin/parametrage',
        icon: 'cil-settings',
      });
    }
  }

  goToHome(): string {
    if (this.role == roleAdmin) {
      return '/admin'; //this.router.navigate(['admin']);
    } else if (this.role == roleCollaborateur) {
      return '/collaborateur';//this.router.navigate(['collaborateur']);
    } else if (this.role == roleEntreprise) {
      return '/entreprise/'+this.entreprise.entrepriseId;// this.router.navigate(['entreprise']);
    }

  }

  goToProfile() {
    if (this.role == roleCollaborateur) {
      return '/collaborateur';//this.router.navigate(['collaborateur']);
    } else if (this.role == roleAdmin || this.role == roleEntreprise) {
      this.router.navigate(['entreprise/' + this.entreprise.entrepriseId + '/editEntreprise', {
        entrepriseId: this.entreprise.entrepriseId,
        entrepriseType: this.entreprise.entrepriseType
      }]);
    }

  }

  goToAlertes():string {
    return '/collaborateur/alertes';
  }
  scrollToTop(){
    this.body.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
    //this.scroll.scrollToPosition([0,0]);
  }

  goToIndicators() {
    this.router.navigate(['entreprise/' + this.entreprise.entrepriseId + '/indicateurs', {
      entrepriseId: this.entreprise.entrepriseId,
      entrepriseType: this.entreprise.entrepriseType
    }]);
  }

  goToParameters() {
    this.router.navigate(['entreprise/' + this.entreprise.entrepriseId + '/parametrage', {
      entrepriseId: this.entreprise.entrepriseId,
      entrepriseType: this.entreprise.entrepriseType
    }])
  }
}
